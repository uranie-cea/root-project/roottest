if(DEFINED ENV{CI_PROJECT_DIR})
    set(CTEST_SOURCE_DIRECTORY $ENV{CI_PROJECT_DIR})
    set(CTEST_BINARY_DIRECTORY $ENV{CI_PROJECT_DIR}/build)
endif()

set(CTEST_CMAKE_COMMAND "cmake")
if(UNIX)
    set(CTEST_CMAKE_GENERATOR "Unix Makefiles")
elseif(WIN32)
    set(CTEST_CMAKE_GENERATOR "Visual Studio 17 2022")
endif()

cmake_host_system_information(RESULT CTEST_SITE QUERY HOSTNAME)
find_program(LSB_RELEASE_EXEC lsb_release)
find_program(POWERSHELL_EXEC powershell)
if (LSB_RELEASE_EXEC)
    execute_process(COMMAND ${LSB_RELEASE_EXEC} -is
        OUTPUT_VARIABLE DISTRIBUTION_NAME
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )
    execute_process(COMMAND ${LSB_RELEASE_EXEC} -rs
        OUTPUT_VARIABLE DISTRIBUTION_VERSION
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )
    set(CTEST_SITE "${CTEST_SITE} (${DISTRIBUTION_NAME} ${DISTRIBUTION_VERSION})")
elseif(POWERSHELL_EXEC)
    execute_process(COMMAND ${POWERSHELL_EXEC} -Command
        "(Get-ComputerInfo).WindowsProductName"
        OUTPUT_VARIABLE WINDOWS_VERSION
        OUTPUT_STRIP_TRAILING_WHITESPACE)
    set(CTEST_SITE "${CTEST_SITE} (${WINDOWS_VERSION})")
else()
    set(CTEST_SITE "${CTEST_SITE} (${CMAKE_SYSTEM_NAME})")
endif()

if(DEFINED ENV{CI_COMMIT_REF_NAME})
    set(CTEST_BUILD_NAME "$ENV{CI_COMMIT_REF_NAME}")
endif()
if(DEFINED ENV{CI_COMMIT_SHORT_SHA})
    set(CTEST_BUILD_NAME "${CTEST_BUILD_NAME} / $ENV{CI_COMMIT_SHORT_SHA}")
endif()

# Parameters
include("${CTEST_SCRIPT_DIRECTORY}/ctest_custom.cmake")

ctest_start(Experimental)
ctest_configure()
ctest_submit()

ctest_build(PARALLEL_LEVEL ${NUM_PROCS_USED})
ctest_submit()

ctest_test(PARALLEL_LEVEL ${NUM_PROCS_USED} OUTPUT_JUNIT report.xml)
ctest_submit()
