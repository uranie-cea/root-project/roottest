import argparse
import re

parser = argparse.ArgumentParser(description='Print Regex')
parser.add_argument('regex')
parser.add_argument('string')
args = parser.parse_args()

regex = re.compile(args.regex)
print('regex:', regex)
print('string:', args.string)

match = re.search(regex, args.string)
print('match:', match)
for i_group in range(regex.groups):
    print(f'group_{i_group}:', match.group(i_group + 1))
