import argparse
import json
import random

parser = argparse.ArgumentParser(description='Values Writer in JSON')
parser.add_argument('inputs', help='JSON filename')
args = parser.parse_args()

with open(args.inputs) as json_file:
    inputs = json.load(json_file)
values = [random.random() for i_number in range(inputs['number of values'])]
with open(inputs['output filename'] , 'w') as json_file:
    json.dump({'values': values}, json_file)
