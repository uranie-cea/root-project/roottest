using namespace URANIE::DataServer;
using namespace URANIE::Relauncher;
using namespace URANIE::MpiRelauncher;
using namespace URANIE::Reoptimizer;

/* user defined */
#define TOLERANCE 0.001
#define NBmaxEVAL 100000
#define TAILLEPOP 400 

/* solution
 *        1 *         1 *      0 *
 */

void testTVizir2MonoMpiRosenbrock(void){
    // Define the DataServer

    // Add the study attributes ( min, max and nominal values)
    TAttribute x("x", -1.5, 1.5);
    TAttribute y("y", -1.5, 1.5);
    TAttribute fval("fval");

    // Set the reference input file and the key for each input attributes
    TKeyScript fin("input_rosenbrock_with_keys.dat");
    fin.setInputs(2, &x, "x", &y, "y");

    // The output file of the code
    TKeyResult fout("_output_rosenbrock_with_keys_.dat");
    // The attribute in the output file
    fout.setOutputs(1, &fval, "fval");

    // Instanciation de mon code
    TCodeEval mycode("rosenbrock -k");

    mycode.addInputFile(&fin);
    mycode.addOutputFile(&fout);

    TMpiRun run(&mycode);
    run.startSlave();
    if (run.onMaster()){
        TDataServer tds("tdsFlowrate",
                "OATMinMax Design of experiments for Flowrate");
        tds.keepFinalTuple(false);
        tds.addAttribute(&x);
        tds.addAttribute(&y);

        TVizirGenetic solv;
        solv.setSize(TAILLEPOP, NBmaxEVAL);
        TVizir2 vzr(&tds, &run, &solv);
        vzr.addObjective(&fval);
        vzr.setTolerance(TOLERANCE);

        /* resolution */
        vzr.solverLoop();

        /* solution */
        tds.exportData("_mpirosenb_.dat");
	
        run.stopSlave();
        tds.computeStatistic("x:y:fval");

        cout << "x Mean[" << tds.getAttribute("x")->getMean() << "]" << endl;
        cout << "x Sigma[" << tds.getAttribute("x")->getStd() << "]" << endl;
        cout << "y Mean[" << tds.getAttribute("y")->getMean() << "]" << endl;
        cout << "y Sigma[" << tds.getAttribute("y")->getStd() << "]" << endl;
        cout << "fval Mean[" << tds.getAttribute("fval")->getMean() << "]" << endl;
        cout << "fval Sigma[" << tds.getAttribute("fval")->getStd() << "]" << endl;
    }
}
