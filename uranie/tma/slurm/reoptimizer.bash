#!/bin/bash
#SBATCH -J reoptimizer
#SBATCH -t 180
#SBATCH -o myjob.%j.o
#SBATCH -e myjob.%j.e

START=`date`

mpirun root -l -b -q testTVizir2MonoMpiRosenbrock.C

echo $START
echo `date`
