#!/bin/bash
#SBATCH -J relauncher
#SBATCH -t 60
#SBATCH -o myjob.%j.o
#SBATCH -e myjob.%j.e

START=`date`

mpirun root -l -b -q testTLauncherFlowrateMPI.C

echo $START
echo `date`
