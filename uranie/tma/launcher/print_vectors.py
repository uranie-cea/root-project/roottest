import argparse

parser = argparse.ArgumentParser(description='Simple Vector Outputs')
parser.add_argument('name_1', help='variable')
parser.add_argument('name_2', help='variable')
parser.add_argument('input', help='filename')
args = parser.parse_args()

with open(args.input) as input_file:
    input = float(input_file.read().strip())

print(args.name_1 + '\t' + args.name_2)
print(f'{input}\t{2 * input}')
print(f'{-input}\t{-2 * input}')
