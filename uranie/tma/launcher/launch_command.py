from URANIE import DataServer, Sampler, Launcher
import argparse, sys

parser = argparse.ArgumentParser('Command Launcher')
parser.add_argument('script', help='Python script')
args = parser.parse_args()

ds = DataServer.TDataServer()
ds.addAttribute(DataServer.TUniformDistribution('x'))

ds.addAttribute('x_2', '2*x')
ds.getAttribute('x_2').setFileKey('input', '', '', DataServer.TAttributeFileKey.kNewKey)

sampler = Sampler.TSampling(ds, 'lhs', 10)
sampler.generateSample()

output = Launcher.TOutputFileKey('output')
output.addAttribute(DataServer.TAttribute('y'))

code = Launcher.TCode(ds, sys.executable + ' ' + args.script + ' input x_2 > output')
code.addOutputFile(output)

launcher = Launcher.TLauncher(ds, code)
launcher.setSave()
launcher.run()

ds.scan()
