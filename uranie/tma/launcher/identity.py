import argparse

parser = argparse.ArgumentParser('Identity from file')
parser.add_argument('filename', help='input filename')
parser.add_argument('key', help='input key')
args = parser.parse_args()

data = {}
with open(args.filename) as inputfile:
    for line in inputfile:
        key, value = line.strip().replace(';', '').partition('=')[::2]
        data[key.strip()] = float(value)

print('y =', data[args.key])
