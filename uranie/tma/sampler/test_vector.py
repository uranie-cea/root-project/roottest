from URANIE import DataServer, Sampler
import ROOT
import argparse

parser = argparse.ArgumentParser('Sampling Vector Components')
parser.add_argument('filename', help='image filename')
args = parser.parse_args()

ds = DataServer.TDataServer()
ds.addAttribute(DataServer.TNormalDistribution('x_1'))
ds.addAttribute(DataServer.TNormalDistribution('x_2'))
ds.getListOfAttributes().ls()

sampling = Sampler.TSampling(ds)
sampling.generateSample()

canvas = ROOT.TCanvas()
ds.draw('x_2:x_1')
canvas.SaveAs(args.filename)
