from URANIE import DataServer, Sampler
import argparse

parser = argparse.ArgumentParser('Unique Generator Test')
parser.add_argument('--num_samples', type=int, help='sample size', default=3)
args = parser.parse_args()

ds_1 = DataServer.TDataServer()
ds_1.addAttribute(DataServer.TUniformDistribution('x'))
ds_2 = DataServer.TDataServer()
ds_2.addAttribute(DataServer.TUniformDistribution('x'))

sampling = Sampler.TSampling(ds_1, 'srs', args.num_samples)
sampling.generateSample()
ds_1.scan()

print('ds_1:', id(ds_1))
print('_tds:', id(sampling._tds))

sampling._tds = ds_2
sampling.generateSample()
ds_2.scan()

print('ds_2:', id(ds_2))
print('_tds:', id(sampling._tds))
