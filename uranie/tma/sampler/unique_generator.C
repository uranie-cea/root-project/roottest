URANIE::DataServer::TDataServer ds_1, ds_2;
URANIE::DataServer::TUniformDistribution distribution("x");

void unique_generator(size_t num_samples=3) {
    ds_1.addAttribute(&distribution);
    URANIE::Sampler::TSampling sampling(&ds_1, "srs", num_samples);
    sampling.generateSample();
    ds_1.scan();

    cout << "ds_1: " << &ds_1 << endl;
    cout << "_tds: " << sampling._tds << endl;

    ds_2.addAttribute(&distribution);
    sampling._tds = &ds_2;
    sampling.generateSample();
    ds_2.scan();

    cout << "ds_2: " << &ds_2 << endl;
    cout << "_tds: " << sampling._tds << endl;
}
