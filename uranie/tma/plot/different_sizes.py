import plotly.express, plotly.graph_objects
import pandas
import argparse

parser = argparse.ArgumentParser('Different Sizes Plot')
parser.add_argument('filename', help='JSON filename')
parser.add_argument('time', help='vector time name')
parser.add_argument('output', help='vector output name')
parser.add_argument('--image', help='PNG image filename')
args = parser.parse_args()

df = pandas.read_json(args.filename)
print('df:')
print(df)

df_0 = pandas.DataFrame({args.time: df[args.time][0], args.output: df[args.output][0], 'observation': 0})
df_1 = pandas.DataFrame({args.time: df[args.time][1], args.output: df[args.output][1], 'observation': 1})

fig_0 = plotly.express.scatter(df_0, x=args.time, y=args.output, color='observation')
fig_1 = plotly.express.scatter(df_1, x=args.time, y=args.output, color='observation')
fig = plotly.graph_objects.Figure(data = fig_0.data + fig_1.data)
if args.image:
    fig.write_image(args.image)
else:
    fig.show()
