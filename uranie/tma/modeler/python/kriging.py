from URANIE import DataServer, Sampler, Modeler
import argparse

parser = argparse.ArgumentParser('Gaussian Process (Kriging) Test')
parser.add_argument('trend', nargs='?', default='')
args = parser.parse_args()

ds = DataServer.TDataServer()
ds.addAttribute(DataServer.TUniformDistribution('x'))
ds.addAttribute(DataServer.TUniformDistribution('y'))

sampling = Sampler.TSampling(ds, 'srs', 10)
sampling.generateSample()

builder = Modeler.TGPBuilder(ds, 'x', 'y', 'gauss', args.trend)
