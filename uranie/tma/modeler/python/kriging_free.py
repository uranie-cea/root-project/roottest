from URANIE import DataServer, Sampler, Modeler

ds = DataServer.TDataServer()
ds.addAttribute(DataServer.TUniformDistribution('x1'))
ds.addAttribute(DataServer.TUniformDistribution('x2'))
ds.addAttribute(DataServer.TUniformDistribution('y'))

sampling = Sampler.TSampling(ds, 'srs', 10)
sampling.generateSample()

builder = Modeler.TGPBuilder(ds, 'x1:x2', 'y', 'gauss', 'x1+x2')
