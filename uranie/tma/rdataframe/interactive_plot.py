import plotly.express, pandas
import re, argparse

parser = argparse.ArgumentParser('Interactive Plot Demo')
parser.add_argument('filename', help='JSON filename')
parser.add_argument('time', help='constant vector time name')
parser.add_argument('output', help='vector output name')
parser.add_argument('--image', help='PNG image filename')
parser.add_argument('--input_prefix', help='input name prefix', default='in_')
parser.add_argument('--input_digits', help='input round digits', type=int, default=2)
args = parser.parse_args()

df = pandas.read_json(args.filename)
inputs = []
for column in df.columns:
    match = re.search(args.input_prefix, str(column))
    if match:
        inputs.append(str(column))

df_1 = df[[args.time, *inputs, args.output]]
df_2 = pandas.DataFrame({args.time: df_1[args.time][0]})

def stripped_input(name, value):
    stripped = name.lstrip(args.input_prefix)
    rounded = str(round(value, args.input_digits))
    return stripped + '=' + rounded

outputs = []
for i_obs, output in enumerate(df_1[args.output]):
    values = [df_1[input_i][i_obs] for input_i in inputs]
    input_values = ','.join(list(map(stripped_input, inputs, values)))
    outputs.append(args.output + '_' + str(i_obs) + ':' + input_values)
    df_2[outputs[-1]] = output

fig = plotly.express.scatter(df_2, x=args.time, y=outputs, labels={'value': args.output})
if args.image:
    fig.write_image(args.image)
else:
    fig.show()
