import ROOT
import argparse

parser = argparse.ArgumentParser('ROOT RDataFrame to Numpy Demo')
parser.add_argument('filename', help='ROOT binary filename')
parser.add_argument('treename', help='ROOT binary treename')
args = parser.parse_args()

df = ROOT.RDataFrame(args.treename, args.filename)
df.Display().Print()
df_1 = df.AsNumpy([df.GetColumnNames()[-1]])
print(df_1)
