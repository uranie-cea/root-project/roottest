from URANIE import DataServer
import ROOT
import json, argparse

parser = argparse.ArgumentParser('Data Post-treatment')
parser.add_argument('configuration', help='JSON configuration')
parser.add_argument('macro', help='C++ macro')
parser.add_argument('output', help='Salomé table output')
parser.add_argument('input', help='Salomé table input')
args = parser.parse_args()

tds_1 = DataServer.TDataServer()
tds_1.keepFinalTuple(False)
tds_1.fileDataRead(args.input)

ROOT.gInterpreter.ExecuteMacro(args.macro)

with open(args.configuration) as json_file:
    configuration = json.load(json_file)

rdf = ROOT.RDataFrame(tds_1.getTuple())
outputs = []
for name, operations in configuration['outputs'].items():
    if operations['last']:
        outputs.append(name + '_last')
        rdf = rdf.Define(outputs[-1], 'Functions::get_last(' + name + ')')
    if operations['mean']:
        outputs.append(name + '_mean')
        rdf = rdf.Define(outputs[-1], 'Functions::get_mean(temps, '+ name +')')

selected_outputs = []
for column in rdf.GetColumnNames():
    if 'Vec' in rdf.GetColumnType(str(column)) or 'size' in str(column):
        continue
    selected_outputs.append(column)
rdf.Snapshot('tree', 'tree.root', selected_outputs)

tds_2 = DataServer.TDataServer()
tds_2.keepFinalTuple(False)
tds_2.ntupleDataRead('tree.root', 'tree')
tds_2.exportData(args.output)
