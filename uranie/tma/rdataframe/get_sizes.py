import pandas
import ROOT
import argparse

parser = argparse.ArgumentParser('Print vector variables size')
parser.add_argument('filename', help='ROOT binary filename')
parser.add_argument('treename', help='ROOT binary treename')
parser.add_argument('time', help='time vector name')
parser.add_argument('output', help='output vector name')
args = parser.parse_args()

rdf = ROOT.RDataFrame(args.treename, args.filename).AsNumpy([args.time, args.output])
pdf = pandas.DataFrame(rdf)
time_sizes = [len(time) for time in pdf[args.time]]
output_sizes = [len(output) for output in pdf[args.output]]
print('same size:', time_sizes == output_sizes)
