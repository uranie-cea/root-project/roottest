import ROOT, pandas
import argparse

parser = argparse.ArgumentParser('Convert to JSON')
parser.add_argument('output', help='JSON output filename')
parser.add_argument('filename', help='ROOT binary filename')
parser.add_argument('treename', help='ROOT binary treename')
args = parser.parse_args()

rdf = ROOT.RDataFrame(args.treename, args.filename).AsNumpy()
df = pandas.DataFrame(rdf)
df.to_json(args.output, default_handler=lambda vector: list(vector))
