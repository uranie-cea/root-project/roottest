void call_function() {
    auto df_empty = ROOT::RDataFrame(10);
    auto df_filled = df_empty
        .Define("x", [](){return gRandom->Rndm();})
        .Define("y", Functions::identity, {"x"});
    df_filled.Display()->Print();
}
