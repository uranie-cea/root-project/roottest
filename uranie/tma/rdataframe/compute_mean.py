from URANIE import DataServer
import ROOT
import numpy
import json, argparse

# \frac{1}{t_n - t_1} \sum_{k=1}^{n-1} \frac{p_k + p_{k+1}}{2} (t_{k+1}-t_k)
def mean(time, power):
    time, power = numpy.array(time), numpy.array(power)
    return 1./(time[-1] - time[0]) * numpy.dot(power[:-1] + power[1:], time[1:] - time[:-1])/2.

parser = argparse.ArgumentParser('Data Post-treatment')
parser.add_argument('configuration', help='JSON configuration')
parser.add_argument('input', help='Salomé table input')
args = parser.parse_args()

tds_1 = DataServer.TDataServer()
tds_1.keepFinalTuple(False)
tds_1.fileDataRead(args.input)

with open(args.configuration) as json_file:
    configuration = json.load(json_file)

df = ROOT.RDataFrame(tds_1.getTuple()).AsNumpy()
for name, operations in configuration['outputs'].items():
    if operations['mean']:
        print('name:', name)
        print('mean[0]:', mean(df['temps'][0], df[name][0]))
