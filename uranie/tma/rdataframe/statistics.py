from URANIE import DataServer
import ROOT
import argparse

parser = argparse.ArgumentParser(description='ROOT Statistics Tester')
parser.add_argument('filename', help='input data filename')
args = parser.parse_args()

ds = DataServer.TDataServer()
ds.keepFinalTuple(False)
ds.fileDataRead(args.filename)
ds.scan('', ds.getIteratorName() + '<=10')

df = ROOT.RDataFrame(ds.getTuple())
df.Display().Print()

print('Mean:', df.Mean('x1').GetValue())
print('StdDev:', df.StdDev('x1').GetValue())
print('Mean with Filter:', df.Filter('x1 > 3.5').Mean('x1').GetValue())
