import ROOT
import argparse

parser = argparse.ArgumentParser('C++ Function Call')
parser.add_argument('macro', help='C++ macro')
args = parser.parse_args()

ROOT.gInterpreter.ExecuteMacro(args.macro)

df_empty = ROOT.RDataFrame(10)
df_filled = df_empty.Define('x', 'gRandom->Rndm()').Define('y', 'Functions::identity(x)')
df_filled.Display().Print()
