from URANIE import DataServer
import ROOT, numpy, pandas
import argparse, ctypes

parser = argparse.ArgumentParser('Quantile Computation')
parser.add_argument('filename', help='image filename')
args = parser.parse_args()

sample_size = 200
num_tests = 100
probability = 0.95
confidence = 0.95

results = {"P=" + str(probability) + method: [] for method in['', ' ' + 'CL=' + str(confidence)]}

for i_test in range(num_tests):
    df = ROOT.RDataFrame(sample_size)
    df_2 = df.Define('x', "gRandom->Gaus()")
    histo = df_2.Histo1D('x')

    quantiles = numpy.zeros(1)
    histo.GetQuantiles(quantiles.size, quantiles, numpy.array([probability]))
    results["P=" + str(probability)].append(quantiles[0])

for i_test in range(num_tests):
    df = ROOT.RDataFrame(sample_size)
    df_2 = df.Define('x', "gRandom->Gaus()")

    df_2.Snapshot('tree', 'tree.root')
    ds = DataServer.TDataServer()
    ds.keepFinalTuple(False)
    ds.ntupleDataRead('tree.root', 'tree')

    quantile = ctypes.c_double(0)
    ds.estimateQuantile('x', probability, quantile, confidence)
    results["P=" + str(probability) + ' ' + 'CL=' + str(confidence)].append(quantile.value)

df = pandas.DataFrame(results)

axes = df.plot.hist(alpha=0.5)
axes.axvline(ROOT.Math.gaussian_quantile(probability, 1))
axes.set_xlabel('Quantile')
axes.figure.savefig(args.filename)
