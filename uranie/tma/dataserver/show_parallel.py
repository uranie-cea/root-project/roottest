from URANIE import DataServer
import ROOT
import argparse, json

parser = argparse.ArgumentParser('Parallel Coordinates Plotter')
parser.add_argument('configuration', help='JSON configuration')
parser.add_argument('figure', help='PNG figure')
parser.add_argument('database', help='Salomé table database')
parser.add_argument('--size', help='figure width height', nargs=2, type=int)
args = parser.parse_args()

ds = DataServer.TDataServer()
ds.fileDataRead(args.database)

with open(args.configuration) as json_file:
    configuration = json.load(json_file)

if args.size:
    canvas = ROOT.TCanvas("canvas", "parallel", *args.size)
else:
    canvas = ROOT.TCanvas()
ds.drawCobWeb(ds.getStringListOfAttributes())
para = [primitive for primitive in ROOT.gPad.GetListOfPrimitives()
        if isinstance(primitive, ROOT.TParallelCoord)][0]
i_color = configuration["initial color"]
for variable, bounds in configuration["variables"].items():
    axis = para.GetVarList().FindObject(variable)
    para.GetCurrentSelection().SetLineColor(i_color)
    i_color += 1
    range = ROOT.TParallelCoordRange(axis, bounds["min"], bounds["max"])
    axis.AddRange(range)
    para.AddSelection(variable)
    range.Draw()

canvas.SaveAs(args.figure)
