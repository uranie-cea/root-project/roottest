from URANIE import DataServer
import ROOT
import numpy

x = numpy.array([1, 2, 3], dtype=numpy.float64)
print(x)

df = ROOT.RDF.FromNumpy({'x': x})
df.Display().Print()
df.Snapshot('tree', 'tree.root')

ds = DataServer.TDataServer()
ds.keepFinalTuple(False)
ds.ntupleDataRead('tree.root', 'tree')
ds.scan()
