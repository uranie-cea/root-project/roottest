vector<size_t> delimiters(const string& splittable, const string& delimiter) {
    vector<size_t> positions;
    size_t last = 0, next = 0;
    while((next = splittable.find(delimiter, last)) != string::npos) {
        positions.push_back(next);
        last = next + 1;
    }
    return positions;
}

pair<string, string> split(const string& splittable, const string& delimiter){
    const auto positions = delimiters(splittable, delimiter);
    const size_t middle = positions[positions.size()/2];
    pair<string, string> name = {splittable.substr(0, middle), splittable.substr(middle+1)};
    return name;
}

void name_splitter(){};
