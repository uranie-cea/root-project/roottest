from URANIE import DataServer, Sampler
import ROOT

ds = DataServer.TDataServer()
ds.keepFinalTuple(False)
attribute = DataServer.TAttribute('x')
attribute.setDefaultValue(0.)
ds.addAttribute(attribute)

design = Sampler.TOATDesign(ds)
design.setRange('x', 1.)
design.generateSample()
ds.scan()

df = ROOT.RDataFrame(ds.getTuple())
df.Display().Print()

print(df.AsNumpy())
