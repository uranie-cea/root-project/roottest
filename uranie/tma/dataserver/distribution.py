from URANIE import DataServer
import ROOT
import argparse

parser = argparse.ArgumentParser('Distribution demo')
parser.add_argument('filename', help='image filename')
args = parser.parse_args()

normal = DataServer.TNormalDistribution('normal', 0., 1.)
x_min = normal.getTheoreticalMean() - 4.*normal.getTheoreticalStdDev()
x_max = normal.getTheoreticalMean() + 4.*normal.getTheoreticalStdDev()

canvas = ROOT.TCanvas()
canvas.Divide(2, 2)

canvas.cd(1)
pdf_lambda = lambda x, _: normal.getPDF(x[0])
pdf = ROOT.TF1('PDF', pdf_lambda, x_min, x_max)
pdf.Draw()

canvas.cd(2)
cdf_lambda = lambda x, _: normal.getCDF(x[0])
cdf = ROOT.TF1('CDF', cdf_lambda, x_min, x_max)
cdf.Draw()

canvas.cd(3)
quantile_lambda = lambda p, _: normal.getInvCDF(p[0])
p_min = normal.getCDF(x_min)
p_max = normal.getCDF(x_max)
quantile = ROOT.TF1('Quantile', quantile_lambda, p_min, p_max)
quantile.Draw()

canvas.SaveAs(args.filename)
