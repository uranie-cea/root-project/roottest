param(
    [Parameter(Mandatory=$true)]
    [ValidateNotNullOrEmpty()]
    [String]
    $VsLauncher,
    [Parameter(Mandatory=$true)]
    [ValidateNotNullOrEmpty()]
    [String]
    $VsArchitecture
)

Write-Host "VsLauncher: $VsLauncher"
Write-Host "VsArchitecture: $VsArchitecture"

Push-Location
& $VsLauncher -Arch $VsArchitecture
Pop-Location

Get-Command cl,root-config
$CFLAGS=(root-config --cflags) -replace 'FI(\w+\.h)', 'FI"$1"'
Invoke-Expression "cl /Fetest_cjit /Tp test_cjit.C ${CFLAGS} $(root-config --libs) libUranieDataServer.lib"
if (-Not $?) {
    Exit 1
}
.\test_cjit
if (-Not $?) {
    Exit 1
}
