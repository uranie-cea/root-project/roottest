from URANIE import DataServer, Sampler, Relauncher, MpiRelauncher
import ROOT
import argparse, sys

parser = argparse.ArgumentParser('ReLauncher Command')
parser.add_argument('script', help='Python script')
parser.add_argument('runner', help='ReLauncher Runner', choices=['seq', 'mpi'])
args = parser.parse_args()

input = DataServer.TUniformDistribution('x')
input_file = Relauncher.TKeyScript('input')
input_file.addInput(input, 'x')

output = DataServer.TAttribute('y')
output_file = Relauncher.TKeyResult('output')
output_file.addOutput(output, 'y')

code = Relauncher.TCodeEval(sys.executable + ' ' + args.script + ' input x > output')
code.addInputFile(input_file)
code.addOutputFile(output_file)

if args.runner == 'seq':
    run = Relauncher.TSequentialRun(code)
elif args.runner == 'mpi':
    run = MpiRelauncher.TMpiRun(code)

run.startSlave()
if run.onMaster():
    ds = DataServer.TDataServer()
    ds.addAttribute(input)

    sampler = Sampler.TSampling(ds, 'lhs', 10)
    sampler.generateSample()
    ds.scan()

    launcher = Relauncher.TLauncher2(ds, run)
    launcher.solverLoop()
    ROOT.SetOwnership(run, True)
    run.stopSlave()
    ds.scan()
