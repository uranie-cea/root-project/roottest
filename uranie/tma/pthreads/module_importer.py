import argparse, importlib

def conditional_import(description):
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('--module', help='Uranie module name')
    args = parser.parse_args()

    if args.module:
        importlib.import_module('URANIE.' + args.module)
