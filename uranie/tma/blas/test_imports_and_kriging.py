import importlib, itertools, argparse

modules = {'numpy': '', 'URANIE.DataServer': '', 'URANIE.Modeler': ''}
permutations = list(itertools.permutations(modules.keys()))

parser = argparse.ArgumentParser(description='Test Kriging for and imports permutation')
parser.add_argument('filename', help='ROOT file name')
parser.add_argument('treename', help='ROOT tree name')
parser.add_argument('i_permutation', type=int, help='permutation number',
    choices=range(len(permutations)))
args = parser.parse_args()

permutation = permutations[args.i_permutation]
print('permutation:', permutation)
for module in permutation:
    modules[module] = importlib.import_module(module)
    print(modules[module])

ds = modules['URANIE.DataServer'].TDataServer()
ds.keepFinalTuple(False)
ds.ntupleDataRead(args.filename, args.treename)
ds.scan()

builder = modules['URANIE.Modeler'].TGPBuilder(ds, "x", "y", "Gauss")
builder.findOptimalParameters("LOO", 10, "Subplexe", 100)
