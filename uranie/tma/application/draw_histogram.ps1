param(
    [Parameter(Mandatory=$true)]
    [ValidateNotNullOrEmpty()]
    [String]
    $VSLAUNCHER,
    [Parameter(Mandatory=$true)]
    [ValidateNotNullOrEmpty()]
    [String]
    $VSARCHITECTURE
)

Push-Location
& $VSLAUNCHER -Arch $VSARCHITECTURE
Pop-Location

Get-Command cl,root-config
$CFLAGS=(root-config --cflags) -replace 'FI(\w+\.h)', 'FI"$1"'
Invoke-Expression "cl /Feapplication /Tp draw_histogram.cpp ${CFLAGS} $(root-config --libs) libUranieDataServer.lib libUranieSampler.lib"

if (-Not $?) {
    Exit 1
}

$code = {
    "$(Get-Location)\application"
}
$job = Start-Job -ScriptBlock $code
if (Wait-Job $job -Timeout 3) { Receive-Job $job }
Remove-Job -force $job
