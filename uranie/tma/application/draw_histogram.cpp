#include "TDataServer.h"
#include "TSampling.h"
#include "TNormalDistribution.h"
#include "TApplication.h"
#include "TCanvas.h"
#include "TRootCanvas.h"

using namespace URANIE;

int main (int argc, char* argv[]){
    TApplication app("app", &argc, argv);
    DataServer::TDataServer tds("tdssampler", "Sampler Uranie demo");
    DataServer::TNormalDistribution distribution("u", -2., 3.);
    tds.addAttribute(&distribution);
    Sampler::TSampling fsamp(&tds, "lhs", 300);
    fsamp.generateSample(); // Create a representative sample

    TCanvas canvas;
    tds.Draw("u");
    canvas.Modified();
    canvas.Update();

    TRootCanvas *root_canvas = dynamic_cast<TRootCanvas*>(canvas.GetCanvasImp());
    root_canvas->Connect("CloseWindow()", "TApplication", gApplication, "Terminate()");
    app.Run();
    return 0;
}
