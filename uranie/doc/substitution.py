import argparse, re

parser = argparse.ArgumentParser("Substitute old string/regex by new string in text file")
parser.add_argument("filename", help="Text filename")
parser.add_argument("choice", help="Type of the old pattern : regex or string", choices=["string", "regex"])
parser.add_argument("old", help="Old string to subtitute")
parser.add_argument("new", help="New string to paste")
args = parser.parse_args()

with open(args.filename, 'r') as inputF:
    inputF_data = inputF.read()

if args.choice == "string":
    outputF_data = inputF_data.replace(args.old, args.new)
elif args.choice == "regex":
    outputF_data = re.sub(args.old, args.new, inputF_data)

with open(args.filename, "w") as outputF:
    outputF.write(outputF_data)
