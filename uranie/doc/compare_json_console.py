import pytest
import argparse
import json
from jsoncomparison import Compare, NO_DIFF

parser = argparse.ArgumentParser('Comparison of two json files')
parser.add_argument('reference', help='reference json file')
parser.add_argument('input', help='input json file')
args = parser.parse_args()

config = {
    "output": {
        "console": True,
        },
    "types": {
        "float": {
            "allow_round": 6,
        },
        "list": {
            "check_length": True,
        }
    }
}

with open(args.reference, 'r') as ref:
    data_ref = json.load(ref)

with open(args.input, 'r') as file:
    data_file = json.load(file)

assert len(data_ref) == len(data_file), "erreur: les dictionnaires doivent avoir le même nombre de clés"

diff = Compare(config).check(data_ref, data_file)
assert diff == NO_DIFF
