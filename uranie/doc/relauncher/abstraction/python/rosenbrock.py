#!/usr/bin/env python3

import ROOT

### user evaluation function
def rosenbrock(x, y) :
    d1 = (1-x)
    d2 = (y-x*x)
    return [d1*d1 + d2*d2,]

### study section

# problem variables 
itemvar = [
    ROOT.URANIE.DataServer.TAttribute("x", -3.0, 3.0),
    ROOT.URANIE.DataServer.TAttribute("y", -4., 6.),
]
ros = ROOT.URANIE.DataServer.TAttribute("rose")

# user evaluation function
eval = ROOT.URANIE.Relauncher.TPythonEval(rosenbrock)
for a in itemvar :
    eval.addInput(a)
eval.addOutput(ros)

# resources
run = ROOT.URANIE.Relauncher.TSequentialRun(eval)
#run = ROOT.URANIE.MpiRelauncher.TMpiRun(eval)
run.startSlave()
if run.onMaster() :
    # data server
    tds = ROOT.URANIE.DataServer.TDataServer("rosopt", "Rosenbrock Optimisation")
    for a in itemvar :
        tds.addAttribute(a)

    # optimisation
    algo = ROOT.URANIE.Reoptimizer.TVizirGenetic()
    study = ROOT.URANIE.Reoptimizer.TVizir2(tds, run, algo)
    study.addObjective(ros)
    study.solverLoop()

    # save results
    tds.exportData("pyrosenbrock.dat")

    run.stopSlave()

