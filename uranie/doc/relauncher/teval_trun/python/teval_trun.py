from URANIE import DataServer, Relauncher

def rosenbrock(x, y) :
    d1 = (1-x)
    d2 = (y-x*x)
    return [d1*d1 + d2*d2,]

def param_declaration(x,y,ros):
    # user evaluation function
    eval = Relauncher.TPythonEval(rosenbrock)
    eval.addInput(x)  # Adding attribute in the correct order
    eval.addInput(y)
    eval.addOutput(ros)

def input_file(x,y):
    # Input File Flat format case
    finp1 = Relauncher.TFlatScript("input_rosenbrock_with_values_rows.dat")
    finp1.addInput(x)  # Adding attributes in the correct order, one-by-one
    finp1.addInput(y)

    # Or Input File Key format case
    kinp1 = Relauncher.TKeyScript("input_rosenbrock_with_keys.dat")
    kinp1.addInput(x, "x")  # Adding attributes in the correct order, one-by-one
    kinp1.addInput(y, "y")

    # Add to the TCodeEval
    code = Relauncher.TCodeEval("rosenbrock -r")  # put "rosenbrock -k" instead for key
    code.addInputFile(finp1)  # put kinp1 instead for key

def output_file(ros):
    code = Relauncher.TCodeEval("rosenbrock -r")

    # Input File Flat format case
    fout = Relauncher.TFlatResult("_output_rosenbrock_with_values_rows.dat")
    fout.addOutput(ros)

    # Or Input File Key format case
    kout = Relauncher.TKeyResult("_output_rosenbrock_with_keys.dat")
    kout.addOutput(ros, "ros")

    # Add output file to the TCodeEval
    code.addOutputFile(fout)  # put kout instead for key

def tsequential():
    output_file = Relauncher.TKeyResult("output.dat")
    code = Relauncher.TCodeEval("rosenbrock -r")
    code.addOutputFile(output_file)

    # Creating the sequential runner
    srun = Relauncher.TSequentialRun(code)

def tthreaded():
    import ROOT
    ROOT.EnableThreadSafety()

    output_file = Relauncher.TKeyResult("output.dat")
    code = Relauncher.TCodeEval("rosenbrock -r")
    code.addOutputFile(output_file)

    # Creating the threaded runner
    trun = Relauncher.TThreadedRun(code,4)

def teval_trun():
    # problem variables
    x = DataServer.TAttribute("x", -3.0, 3.0)
    y = DataServer.TAttribute("y", -4., 6.)
    ros = DataServer.TAttribute("rose")

    param_declaration(x,y,ros)
    input_file(x,y)
    output_file(ros)
    tsequential()
    tthreaded()

teval_trun()
