from URANIE import DataServer
import ROOT
import argparse

parser = argparse.ArgumentParser("Draw flowrate data")
parser.add_argument("input", help="Text input filename")
parser.add_argument("figure", help="image filename")
parser.add_argument("style", help="macro filename")
args = parser.parse_args()

## Get the outputs into the TDS
tds = DataServer.TDataServer("foo","pouet")
tds.fileDataRead(args.input)

## Draw the result
can = ROOT.TCanvas("pouet","foo",1)
tds.Draw("yhat:rw","","colZ")

ROOT.gROOT.LoadMacro(args.style)

can.SaveAs(args.figure)
