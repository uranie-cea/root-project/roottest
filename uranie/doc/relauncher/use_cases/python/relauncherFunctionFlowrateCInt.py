"""
Example of function launching in sequential mode
"""
from URANIE import DataServer, Relauncher
import ROOT
import argparse

parser = argparse.ArgumentParser("function launching in sequential mode")
parser.add_argument("--figure", help="image filename", default="figure.png")
parser.add_argument("--style", help="macro filename")
args = parser.parse_args()

# Create the DataServer.TDataServer
tds = DataServer.TDataServer("foo", "test")
tds.fileDataRead("flowrateUniformDesign.dat")

# Get the attributes
rw = tds.getAttribute("rw")
r = tds.getAttribute("r")
tu = tds.getAttribute("tu")
tl = tds.getAttribute("tl")
hu = tds.getAttribute("hu")
hl = tds.getAttribute("hl")
lvar = tds.getAttribute("l")
kw = tds.getAttribute("kw")

# Create the output attribute
yhat = DataServer.TAttribute("yhat")

# Constructing the code
ROOT.gROOT.LoadMacro("UserFunctions.C")
mycode = Relauncher.TCIntEval("flowrateModel")
mycode.addInput(rw)
mycode.addInput(r)
mycode.addInput(tu)
mycode.addInput(tl)
mycode.addInput(hu)
mycode.addInput(hl)
mycode.addInput(lvar)
mycode.addInput(kw) # Adding the input attributes
mycode.addOutput(yhat)  # Adding the output attributes

# Create the sequential runner
run = Relauncher.TSequentialRun(mycode)
run.startSlave()  # Start the master (necessary even for a sequential)
if run.onMaster():

    lanceur = Relauncher.TLauncher2(tds, run)

    # resolution
    lanceur.solverLoop()
    run.stopSlave()  # Stop the slaves (necessary even for a sequential)

# Draw the result
can = ROOT.TCanvas("pouet", "foo", 1)
tds.Draw("yhat:rw", "", "colZ")

if args.style:
    ROOT.gROOT.LoadMacro(args.style)

can.SaveAs(args.figure)
