ROOTTEST_ADD_TESTDIRS()

include(UranieModules)
include(CMakePrintHelpers)
include("${CMAKE_CURRENT_SOURCE_DIR}/../regex_table.cmake")

set(PYROOT_EXTRAFLAGS_SAVED ${PYROOT_EXTRAFLAGS})
set(PYROOT_EXTRAFLAGS)
cmake_print_variables(CMAKE_CURRENT_LIST_FILE PYROOT_EXTRAFLAGS)

################## CREATION SOUS-DOSSIERS POUR TESTS PYTHON ##################

file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/cint)
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/python)
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/sequential)
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/sequential_const)
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/threaded)
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/mpi)
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/sequential_fail)
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/key)
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/key_vector)
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/key_vector_fail)
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/multi_type)
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/compose_multi_type)
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/sequential_temporary)

################## COPIES FICHIERS POUR TESTS PYTHON ##################

configure_file(${CMAKE_SOURCE_DIR}/uranie/references/flowrateUniformDesign.dat
    ${CMAKE_CURRENT_BINARY_DIR}/cint COPYONLY)
configure_file(${CMAKE_SOURCE_DIR}/uranie/doc/UserFunctions.C
    ${CMAKE_CURRENT_BINARY_DIR}/cint COPYONLY)

configure_file(${CMAKE_SOURCE_DIR}/uranie/references/flowrateUniformDesign.dat
    ${CMAKE_CURRENT_BINARY_DIR}/python COPYONLY)

configure_file(${CMAKE_SOURCE_DIR}/uranie/references/flowrate_input_with_values_rows.in
    ${CMAKE_CURRENT_BINARY_DIR}/sequential COPYONLY)
configure_file(${CMAKE_SOURCE_DIR}/uranie/references/flowrateUniformDesign.dat
    ${CMAKE_CURRENT_BINARY_DIR}/sequential COPYONLY)

configure_file(${CMAKE_SOURCE_DIR}/uranie/references/flowrate_input_with_keys.in
    ${CMAKE_CURRENT_BINARY_DIR}/sequential_const COPYONLY)

configure_file(${CMAKE_SOURCE_DIR}/uranie/references/flowrate_input_with_values_rows.in
    ${CMAKE_CURRENT_BINARY_DIR}/threaded COPYONLY)
configure_file(${CMAKE_SOURCE_DIR}/uranie/references/flowrateUniformDesign.dat
    ${CMAKE_CURRENT_BINARY_DIR}/threaded COPYONLY)

configure_file(${CMAKE_SOURCE_DIR}/uranie/references/flowrate_input_with_values_rows.in
    ${CMAKE_CURRENT_BINARY_DIR}/mpi COPYONLY)
configure_file(${CMAKE_SOURCE_DIR}/uranie/references/flowrateUniformDesign.dat
    ${CMAKE_CURRENT_BINARY_DIR}/mpi COPYONLY)

configure_file(${CMAKE_SOURCE_DIR}/uranie/references/flowrate_input_with_values_rows.in
    ${CMAKE_CURRENT_BINARY_DIR}/sequential_fail COPYONLY)
configure_file(${CMAKE_SOURCE_DIR}/uranie/references/flowrateUniformDesign.dat
    ${CMAKE_CURRENT_BINARY_DIR}/sequential_fail COPYONLY)

configure_file(${CMAKE_SOURCE_DIR}/uranie/references/readmultitype_sampling.dat
    ${CMAKE_CURRENT_BINARY_DIR}/multi_type COPYONLY)

configure_file(${CMAKE_SOURCE_DIR}/uranie/references/multitype_sampling.dat
    ${CMAKE_CURRENT_BINARY_DIR}/compose_multi_type COPYONLY)

configure_file(${CMAKE_SOURCE_DIR}/uranie/references/flowrate_input_with_keys.in
    ${CMAKE_CURRENT_BINARY_DIR}/sequential_temporary COPYONLY)

################## TESTS GENERATION IMAGE PYTHON ##################

ROOTTEST_ADD_TEST(relauncherFunctionFlowrateCInt MACRO relauncherFunctionFlowrateCInt.py
    OPTS "--figure" relauncherFunctionFlowrateCInt.png
        "--style" ${CMAKE_SOURCE_DIR}/uranie/doc/test_style_OLD.C
    WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/cint)

ROOTTEST_ADD_TEST(relauncherFunctionFlowratePython MACRO relauncherFunctionFlowratePython.py
    OPTS "--figure" relauncherFunctionFlowratePython.png
        "--style" ${CMAKE_SOURCE_DIR}/uranie/doc/test_style_OLD.C
    WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/python)

ROOTTEST_ADD_TEST(relauncherCodeFlowrateSequential MACRO relauncherCodeFlowrateSequential.py
    OPTS "--figure" relauncherCodeFlowrateSequential.png
        "--style" ${CMAKE_SOURCE_DIR}/uranie/doc/test_style_OLD.C
    WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/sequential)

ROOTTEST_ADD_TEST(relauncherCodeFlowrateThreaded MACRO relauncherCodeFlowrateThreaded.py
    OPTS "--figure" relauncherCodeFlowrateThreaded.png
        "--style" ${CMAKE_SOURCE_DIR}/uranie/doc/test_style_OLD.C
    WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/threaded)

find_package(MPI)
if(MPIEXEC_EXECUTABLE AND MPIEXEC_NUMPROC_FLAG)
    ROOTTEST_ADD_TEST(relauncherCodeFlowrateMPI COMMAND ${MPIEXEC_EXECUTABLE} ${MPIEXEC_NUMPROC_FLAG} 3
        ${Python3_EXECUTABLE} ${CMAKE_CURRENT_SOURCE_DIR}/relauncherCodeFlowrateMPI.py
        POSTCMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/relauncher/use_cases/draw_flowrate.py
            "_output_testFlowrateMPI_py_.dat" "relauncherCodeFlowrateMPI.png"
            "${CMAKE_SOURCE_DIR}/uranie/doc/test_style_OLD.C"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/mpi)
endif()

ROOTTEST_ADD_TEST(relauncherCodeMultiTypeKey MACRO relauncherCodeMultiTypeKey.py
    OPTS "--figure" relauncherCodeMultiTypeKey.png "--style" ${CMAKE_SOURCE_DIR}/uranie/doc/test_style_OLD.C
        "--seed" 12345
    WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/key)

ROOTTEST_ADD_TEST(relauncherCodeMultiTypeKeyEmptyVectors
    MACRO relauncherCodeMultiTypeKeyEmptyVectors.py
    OPTS "--figure" relauncherCodeMultiTypeKeyEmptyVectors.png
        "--style" ${CMAKE_SOURCE_DIR}/uranie/doc/test_style_OLD.C "--seed" 12345
    WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/key_vector)

################## TESTS SORTIE CONSOLE + GENERATION IMAGE PYTHON ##################

ROOTTEST_ADD_TEST(relauncherCodeFlowrateSequentialFailure
    MACRO relauncherCodeFlowrateSequentialFailure.py
    OPTS "--figure" relauncherCodeFlowrateSequentialFailure.png
        "--style" ${CMAKE_SOURCE_DIR}/uranie/doc/test_style_OLD.C
    POSTCMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/delete_ansi.py
        ${CMAKE_CURRENT_BINARY_DIR}/relauncherCodeFlowrateSequentialFailure_clean.log
        ${CMAKE_CURRENT_BINARY_DIR}/relauncherCodeFlowrateSequentialFailure.log
    WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/sequential_fail
    PASSREGEX ${REGEX_CONSOLE_SEQUENTIAL_FAILURE})

ROOTTEST_ADD_TEST(relauncherCodeMultiTypeKeyEmptyVectorsAsFailure
    MACRO relauncherCodeMultiTypeKeyEmptyVectorsAsFailure.py
    OPTS "--figure" relauncherCodeMultiTypeKeyEmptyVectorsAsFailure.png
        "--style" ${CMAKE_SOURCE_DIR}/uranie/doc/test_style_OLD.C "--seed" 12345
    POSTCMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/delete_ansi.py
        ${CMAKE_CURRENT_BINARY_DIR}/relauncherCodeMultiTypeKeyEmptyVectorsAsFailure_clean.log
        ${CMAKE_CURRENT_BINARY_DIR}/relauncherCodeMultiTypeKeyEmptyVectorsAsFailure.log
    WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/key_vector_fail
    PASSREGEX ${REGEX_CONSOLE_VECTORS_AS_FAILURE})

################## TESTS SORTIE CONSOLE PYTHON ##################

ROOTTEST_ADD_TEST(relauncherCodeFlowrateSequential_ConstantVar
    MACRO relauncherCodeFlowrateSequential_ConstantVar.py
    OPTS "--seed" 132456
    WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/sequential_const
    PASSREGEX ${REGEX_CONSOLE_CONSTANT_VAR})

ROOTTEST_ADD_TEST(relauncherCodeReadMultiType MACRO relauncherCodeReadMultiType.py
    WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/multi_type
    PASSREGEX ${REGEX_CONSOLE_READ_MULTI_TYPE})

ROOTTEST_ADD_TEST(relauncherComposeMultitypeAndReadMultiType
    MACRO relauncherComposeMultitypeAndReadMultiType.py
    WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/compose_multi_type
    PASSREGEX ${REGEX_CONSOLE_AND_READ_MULTI_TYPE})

ROOTTEST_ADD_TEST(relauncherCodeFlowrateSequential_TemporaryVar
    MACRO relauncherCodeFlowrateSequential_TemporaryVar.py
    OPTS "--seed" 132456
    WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/sequential_temporary
    PASSREGEX ${REGEX_CONSOLE_TEMPORARY_VAR})

############################# COMPARAISONS ##############################

include("${CMAKE_CURRENT_SOURCE_DIR}/../comparisons.cmake")

################## COMPARAISON IMAGES ##################

if(PY_SKIMAGE_FOUND)
    ROOTTEST_ADD_TEST(relauncherFunctionFlowratePython_figure
        MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_images.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/relauncher/use_cases/relauncherFunctionFlowratePython.png"
            "relauncherFunctionFlowratePython.png" "relauncherFunctionFlowratePython_diff.png"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/python
        DEPENDS relauncherFunctionFlowratePython)
endif()

set(PYROOT_EXTRAFLAGS ${PYROOT_EXTRAFLAGS_SAVED})
