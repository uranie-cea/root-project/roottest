"""
Example of MPi usage for code launching
"""
from URANIE import DataServer, Relauncher, MpiRelauncher
import ROOT

# Create input attributes
rw = DataServer.TAttribute("rw")
r = DataServer.TAttribute("r")
tu = DataServer.TAttribute("tu")
tl = DataServer.TAttribute("tl")
hu = DataServer.TAttribute("hu")
hl = DataServer.TAttribute("hl")
lvar = DataServer.TAttribute("l")
kw = DataServer.TAttribute("kw")

# Create the output attribute
yhat = DataServer.TAttribute("yhat")
d = DataServer.TAttribute("d")

# Set the reference input file and the key for each input attributes
fin = Relauncher.TFlatScript("flowrate_input_with_values_rows.in")
fin.addInput(rw)
fin.addInput(r)
fin.addInput(tu)
fin.addInput(tl)
fin.addInput(hu)
fin.addInput(hl)
fin.addInput(lvar)
fin.addInput(kw)

# The output file of the code
fout = Relauncher.TFlatResult("_output_flowrate_withRow_.dat")
fout.addOutput(yhat)
fout.addOutput(d)  # Passing the attributes to the output file

# Instanciation de mon code
mycode = Relauncher.TCodeEval("flowrate -s -r")
# mycode.setOldTmpDir()
mycode.addInputFile(fin)
mycode.addOutputFile(fout)

# Create the MPI runner
run = MpiRelauncher.TMpiRun(mycode)
run.startSlave()
if run.onMaster():

    # Define the DataServer
    tds = DataServer.TDataServer("tdsflowrate",
                                 "Design of Experiments for Flowrate")
    mycode.addAllInputs(tds)
    tds.fileDataRead("flowrateUniformDesign.dat", False, True)

    lanceur = Relauncher.TLauncher2(tds, run)

    # resolution
    lanceur.solverLoop()

    tds.exportData("_output_testFlowrateMPI_py_.dat")

    ROOT.SetOwnership(run, True)
    run.stopSlave()
