"""
Example of code launching in threaded mode
"""
from URANIE import DataServer, Relauncher
import ROOT
import argparse

parser = argparse.ArgumentParser("Launch code in threaded mode")
parser.add_argument("--figure", help="image filename", default="figure.png")
parser.add_argument("--style", help="macro filename")
args = parser.parse_args()

# Create input attributes
rw = DataServer.TAttribute("rw")
r = DataServer.TAttribute("r")
tu = DataServer.TAttribute("tu")
tl = DataServer.TAttribute("tl")
hu = DataServer.TAttribute("hu")
hl = DataServer.TAttribute("hl")
lvar = DataServer.TAttribute("l")
kw = DataServer.TAttribute("kw")

# Create the output attribute
yhat = DataServer.TAttribute("yhat")
d = DataServer.TAttribute("d")

# Set the reference input file and the key for each input attributes
fin = Relauncher.TFlatScript("flowrate_input_with_values_rows.in")
fin.addInput(rw)
fin.addInput(r)
fin.addInput(tu)
fin.addInput(tl)
fin.addInput(hu)
fin.addInput(hl)
fin.addInput(lvar)
fin.addInput(kw)

# The output file of the code
fout = Relauncher.TFlatResult("_output_flowrate_withRow_.dat")
fout.addOutput(yhat)
fout.addOutput(d)  # Passing the attributes to the output file

# Constructing the code
mycode = Relauncher.TCodeEval("flowrate -s -r")
mycode.setOldTmpDir()
mycode.addInputFile(fin)  # Adding the input file
mycode.addOutputFile(fout)  # Adding the output file

# Fix the number of threads
nthread = 3
# Create the Threaded runner
run = Relauncher.TThreadedRun(mycode, nthread)
run.startSlave()  # Start the master
if run.onMaster():

    # Define the DataServer
    tds = DataServer.TDataServer("tdsflowrate", "Design of Experiments")
    mycode.addAllInputs(tds)
    tds.fileDataRead("flowrateUniformDesign.dat", False, True)

    lanceur = Relauncher.TLauncher2(tds, run)

    # resolution
    lanceur.solverLoop()
    run.stopSlave()  # Stop the slaves (necessary even for a sequential)

    # Draw the result
    can = ROOT.TCanvas("pouet", "foo", 1)
    tds.Draw("yhat:rw", "", "colZ")

    if args.style:
        ROOT.gROOT.LoadMacro(args.style)

    can.SaveAs(args.figure)
