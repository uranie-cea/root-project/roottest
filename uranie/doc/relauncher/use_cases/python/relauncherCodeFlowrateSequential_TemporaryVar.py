"""
Example of code launching in sequential mode with temporary variable
"""
from URANIE import DataServer, Relauncher, Sampler
import argparse

parser = argparse.ArgumentParser("Code launching in sequential mode with temporary variable")
parser.add_argument("--seed", help="seed number", default=0, type=int)
args = parser.parse_args()


def increase_d(local_x):
    """Dummy function that increase by one the input."""
    local_y = local_x + 1
    return [local_y, ]


# Create the TDataServer
tds = DataServer.TDataServer("foo", "test")

# Define the attribute that should be considered as constant
r = DataServer.TAttribute("r")

# Add the study attributes (min, max and nominal values)
tds.addAttribute(DataServer.TUniformDistribution("rw", 0.05, 0.15))
tds.addAttribute(DataServer.TUniformDistribution("tu", 63070.0, 115600.0))
tds.addAttribute(DataServer.TUniformDistribution("tl", 63.1, 116.0))
tds.addAttribute(DataServer.TUniformDistribution("hu", 990.0, 1110.0))
tds.addAttribute(DataServer.TUniformDistribution("hl", 700.0, 820.0))
tds.addAttribute(DataServer.TUniformDistribution("l", 1120.0, 1680.0))
tds.addAttribute(DataServer.TUniformDistribution("kw", 9855.0, 12045.0))

# The reference input file
sIn = "flowrate_input_with_keys.in"

nS = 15
# Generate the Design of Experiments
sampling = Sampler.TSampling(tds, "lhs", nS)
if args.seed > 0:
    sampling.setSeed(args.seed)
sampling.generateSample()

# Create the input files
inputFile = Relauncher.TKeyScript(sIn)
inputFile.addInput(tds.getAttribute("rw"), "Rw")
inputFile.addInput(r, "R")
inputFile.addInput(tds.getAttribute("tu"), "Tu")
inputFile.addInput(tds.getAttribute("tl"), "Tl")
inputFile.addInput(tds.getAttribute("hu"), "Hu")
inputFile.addInput(tds.getAttribute("hl"), "Hl")
inputFile.addInput(tds.getAttribute("l"), "L")
inputFile.addInput(tds.getAttribute("kw"), "Kw")

# Create the output attributes
yhat = DataServer.TAttribute("yhat")
d = DataServer.TAttribute("d")

# Create the output files
outputFile = Relauncher.TKeyResult("_output_flowrate_withKey_.dat")
outputFile.addOutput(yhat, "yhat")
outputFile.addOutput(d, "d")

# Create the user's evaluation function
eval1 = Relauncher.TCodeEval("flowrate -s -k")
eval1.addInputFile(inputFile)
eval1.addOutputFile(outputFile)

# Create a second evaluation function that uses d to change it slightly
incd = DataServer.TAttribute("incd")
eval2 = Relauncher.TPythonEval(increase_d)
eval2.addInput(d)
eval2.addOutput(incd)

# Create the composition
evalC = Relauncher.TComposeEval()
# Add the code one-by-one, in the right order
evalC.addEval(eval1)
evalC.addEval(eval2)

# Create the sequential runner
run = Relauncher.TSequentialRun(evalC)
run.startSlave()  # Start the master (necessary even for a sequential)
if run.onMaster():

    lanceur = Relauncher.TLauncher2(tds, run)
    # State to the master: d is an output attribute and I'm interested in its value
    # but I don't want to keep it in the end. It might be useful for another evaluator
    lanceur.addTemporary(d)
    lanceur.addConstantValue(r, 108)

    # resolution
    lanceur.solverLoop()
    run.stopSlave()  # Stop the slaves (necessary even for a sequential)

tds.scan("*", "", "colsize=6")

tds.saveTuple("update")
