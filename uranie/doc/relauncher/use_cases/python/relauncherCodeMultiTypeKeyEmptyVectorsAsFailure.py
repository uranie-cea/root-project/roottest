"""
Example of code launching with empy vectors as failure
"""
from URANIE import DataServer, Relauncher, Sampler
import ROOT
import argparse

parser = argparse.ArgumentParser("Launch code with empy vectors as failure")
parser.add_argument("--figure", help="image filename", default="figure.png")
parser.add_argument("--style", help="macro filename")
parser.add_argument("--seed", help="seed number", default=0, type=int)
args = parser.parse_args()

# Create the DataServer.TDataServer and create the seed attribute
tds = DataServer.TDataServer("foo", "multitype usecase")
tds.addAttribute(DataServer.TUniformDistribution("seed", 0, 100000))

# Create DOE
tsam = Sampler.TSampling(tds, "lhs", 100)
if args.seed > 0:
    tsam.setSeed(args.seed)
tsam.generateSample()

# Create output attribute pointers
w1 = DataServer.TAttribute("w1", DataServer.TAttribute.kString)
w2 = DataServer.TAttribute("w2", DataServer.TAttribute.kString)
v1 = DataServer.TAttribute("v1", DataServer.TAttribute.kVector)
v2 = DataServer.TAttribute("v2", DataServer.TAttribute.kVector)
f1 = DataServer.TAttribute("f1")

# Create the input files
inputFile = Relauncher.TFlatScript("multitype_input.dat")
inputFile.addInput(tds.getAttribute("seed"))

# Create the output files
outputFile = Relauncher.TKeyResult("_output_multitype_mt_Key_.dat")
outputFile.addOutput(w1, "w1")
outputFile.addOutput(v1, "v1")
outputFile.addOutput(v2, "v2")
outputFile.addOutput(f1, "f1")
outputFile.addOutput(w2, "w2")

# Create the user's evaluation function
myeval = Relauncher.TCodeEval("multitype -mtKey -empty")
myeval.addInputFile(inputFile)  # Add the input file
myeval.addOutputFile(outputFile)  # Add the output file

# Create the runner
runner = Relauncher.TSequentialRun(myeval)

# Start the slaves
runner.startSlave()
if runner.onMaster():

    # Create the launcher
    lanceur = Relauncher.TLauncher2(tds, runner)

    # Store the wrong calculation
    error = DataServer.TDataServer("WrongComputations", "pouet")
    lanceur.setSaveError(error)

    lanceur.solverLoop()

    # dump all wrong configurations
    error.getTuple().SetScanField(-1)
    error.scan("*")

    error.saveTuple("update")

    # Stop the slave processes
    runner.stopSlave()

# Produce control plot
Can = ROOT.TCanvas("Can", "Can", 10, 10, 1000, 800)
pad = ROOT.TPad("pad", "pad", 0, 0.03, 1, 1)
pad.Draw()
if args.style:
    ROOT.gROOT.LoadMacro(args.style)
pad.cd()
tds.drawPairs("w1:v1:v2:f1:w2")

Can.SaveAs(args.figure)
