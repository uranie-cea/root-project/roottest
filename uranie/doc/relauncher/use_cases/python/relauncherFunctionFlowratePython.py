"""
Example of python function launching
"""
import math
from URANIE import DataServer, Relauncher
import ROOT
import argparse

parser = argparse.ArgumentParser("Python function launching")
parser.add_argument("--figure", help="image filename", default="figure.png")
parser.add_argument("--style", help="macro filename")
args = parser.parse_args()

def flowrate_model(drw, drvar, dtu, dtl, dhu, dhl, dlvar, dkw):
    """Simple flowrate model python function."""
    dnum = 2.0 * math.pi * dtu * (dhu - dhl)
    dlnronrw = math.log(drvar / drw)
    dden = dlnronrw * (1.0 + (2.0 * dlvar * dtu) / (dlnronrw * drw * drw * dkw)
                       + dtu / dtl)

    return [dnum / dden, ]


# Create the TDataServer
tds = DataServer.TDataServer("foo", "test")
tds.fileDataRead("flowrateUniformDesign.dat")

# Get the attributes
rw = tds.getAttribute("rw")
r = tds.getAttribute("r")
tu = tds.getAttribute("tu")
tl = tds.getAttribute("tl")
hu = tds.getAttribute("hu")
hl = tds.getAttribute("hl")
lvar = tds.getAttribute("l")
kw = tds.getAttribute("kw")

# Create the output attribute
yhat = DataServer.TAttribute("yhat")

# Constructing the code
mycode = Relauncher.TPythonEval(flowrate_model)
# Adding the input file
mycode.addInput(rw)
mycode.addInput(r)
mycode.addInput(tu)
mycode.addInput(tl)
mycode.addInput(hu)
mycode.addInput(hl)
mycode.addInput(lvar)
mycode.addInput(kw)
# Adding the output file
mycode.addOutput(yhat)

# Create the sequential runner
run = Relauncher.TSequentialRun(mycode)
run.startSlave()  # Start the master (necessary even for a sequential)
if run.onMaster():

    launch = Relauncher.TLauncher2(tds, run)

    # resolution
    launch.solverLoop()
    run.stopSlave()  # Stop the slaves (necessary even for a sequential)

# Draw the result
can = ROOT.TCanvas("pouet", "foo", 1)
tds.Draw("yhat:rw", "", "colZ")

if args.style:
    ROOT.gROOT.LoadMacro(args.style)

can.SaveAs(args.figure)
