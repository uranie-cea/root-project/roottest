param(
    [Parameter(Mandatory=$true)]
    [ValidateNotNullOrEmpty()]
    [String]
    $VSLAUNCHER,
    [Parameter(Mandatory=$true)]
    [ValidateNotNullOrEmpty()]
    [String]
    $VSARCHITECTURE
)

Push-Location
& $VSLAUNCHER -Arch $VSARCHITECTURE
Pop-Location

Get-Command cl,root-config
$CFLAGS=(root-config --cflags) -replace 'FI(\w+\.h)', 'FI"$1"'
Invoke-Expression "cl /FeCJitTest /Tp relauncherFunctionFlowrateCJit.C ${CFLAGS} $(root-config --libs) libUranieDataServer.lib libUranieRelauncher.lib"

if (-Not $?) {
    Exit 1
}
.\CJitTest
if (-Not $?) {
    Exit 1
}
