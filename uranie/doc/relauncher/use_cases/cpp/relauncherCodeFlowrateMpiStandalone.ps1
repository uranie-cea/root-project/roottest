param(
    [Parameter(Mandatory=$true)]
    [ValidateNotNullOrEmpty()]
    [String]
    $VSLAUNCHER,
    [Parameter(Mandatory=$true)]
    [ValidateNotNullOrEmpty()]
    [String]
    $VSARCHITECTURE
)

Push-Location
& $VSLAUNCHER -Arch $VSARCHITECTURE
Pop-Location

Get-Command cl,root-config
$CFLAGS=(root-config --cflags) -replace 'FI(\w+\.h)', 'FI"$1"'
Invoke-Expression "cl /FerelaunStand /Tp relauncherCodeFlowrateMpiStandalone.C ${CFLAGS} $(root-config --libs) libUranieDataServer.lib libUranieRelauncher.lib"

if(-Not $?) {
    Exit 1
}

Get-Command mpiexec

if(-Not $?) {
    Exit 1
}

mpiexec -np 3 .\relaunStand initType 0
mpiexec -np 3 .\relaunStand initType 1

if(-Not $?) {
    Exit 1
}
