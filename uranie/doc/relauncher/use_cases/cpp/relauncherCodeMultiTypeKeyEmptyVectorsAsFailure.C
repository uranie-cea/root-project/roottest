    using namespace URANIE::DataServer;
    using namespace URANIE::Relauncher;
    using namespace URANIE::Sampler;

    TDataServer *tds = new TDataServer("foo", "multitype usecase");

    TCanvas *Can = new TCanvas("Can","Can",10,10,1000,800);
    TPad *pad = new TPad("pad","pad",0, 0.03, 1, 1); 

void relauncherCodeMultiTypeKeyEmptyVectorsAsFailure(const string& figure="figure.png", const string& style="", const int seed=0)
{
    // Create the TDataServer and create the seed attribute
    tds->addAttribute( new TUniformDistribution("seed",0,100000));

    //Create DOE
    TSampling *tsam = new TSampling(tds,"lhs",100);
    if (seed > 0)
        tsam->setSeed(seed);
    tsam->generateSample();

    // Create output attribute pointers
    TAttribute *w1 = new TAttribute("w1", TAttribute::kString);
    TAttribute *w2 = new TAttribute("w2", TAttribute::kString);
    TAttribute *v1 = new TAttribute("v1", TAttribute::kVector);
    TAttribute *v2 = new TAttribute("v2", TAttribute::kVector);
    TAttribute *f1 = new TAttribute("f1");

    // Create the input files
    TFlatScript inputFile("multitype_input.dat");
    inputFile.setInputs(1, tds->getAttribute("seed"), "seed");    

    // Create the output files
    TKeyResult outputFile("_output_multitype_mt_Key_.dat");
    outputFile.addOutput(w1, "w1");
    outputFile.addOutput(v1, "v1");
    outputFile.addOutput(v2, "v2");
    outputFile.addOutput(f1, "f1");
    outputFile.addOutput(w2, "w2");

    // Create the user's evaluation function
    TCodeEval eval("multitype -mtKey -empty");
    eval.addInputFile(&inputFile); // Add the input file
    eval.addOutputFile(&outputFile); // Add the output file

    //Create the runner
    TSequentialRun runner(&eval);

    // Start the slaves
    runner.startSlave();                                           
    if (runner.onMaster())
    {

        // Create the launcher
        TLauncher2 lanceur(tds, &runner);
        
        // Store the wrong calculation
        TDataServer error("WrongComputations","pouet");
        lanceur.setSaveError(&error);

        lanceur.solverLoop();

        // dump all wrong configurations
        cout<<"\nFailed configurations: "<<endl;
        error.getTuple()->SetScanField(-1);
        error.scan("*");

        error.saveTuple("update");
          
        // Stop the slave processes
        runner.stopSlave();
        
    }          
    //Produce control plot
    pad->Draw(); 
    if (!style.empty())
        gROOT->LoadMacro(style.data());
    pad->cd();
    tds->drawPairs("w1:v1:v2:f1:w2");

    Can->SaveAs(figure.data());
}
