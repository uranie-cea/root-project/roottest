ROOTTEST_ADD_TESTDIRS()

include(UranieModules)
include(CMakePrintHelpers)
include("${CMAKE_CURRENT_SOURCE_DIR}/../regex_table.cmake")

################## CREATION SOUS-DOSSIERS POUR TESTS C++ ##################

file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/cint)
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/cjit)
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/thread_test)
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/sequential)
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/sequential_const)
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/threaded)
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/mpi)
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/standalone)
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/sequential_fail)
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/key)
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/key_vector)
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/key_vector_fail)
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/multi_type)
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/compose_multi_type)
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/sequential_temporary)

################## COPIES FICHIERS POUR TESTS C++ ##################

configure_file(${CMAKE_SOURCE_DIR}/uranie/references/flowrateUniformDesign.dat
    ${CMAKE_CURRENT_BINARY_DIR}/cint COPYONLY)

configure_file(${CMAKE_SOURCE_DIR}/uranie/references/flowrateUniformDesign.dat
    ${CMAKE_CURRENT_BINARY_DIR}/cjit COPYONLY)
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/relauncherFunctionFlowrateCJit.C
    ${CMAKE_CURRENT_BINARY_DIR}/cjit COPYONLY)

configure_file(${CMAKE_SOURCE_DIR}/uranie/references/flowrateUniformDesign.dat
    ${CMAKE_CURRENT_BINARY_DIR}/thread_test COPYONLY)

configure_file(${CMAKE_SOURCE_DIR}/uranie/references/flowrate_input_with_values_rows.in
    ${CMAKE_CURRENT_BINARY_DIR}/sequential COPYONLY)
configure_file(${CMAKE_SOURCE_DIR}/uranie/references/flowrateUniformDesign.dat
    ${CMAKE_CURRENT_BINARY_DIR}/sequential COPYONLY)

configure_file(${CMAKE_SOURCE_DIR}/uranie/references/flowrate_input_with_keys.in
    ${CMAKE_CURRENT_BINARY_DIR}/sequential_const COPYONLY)

configure_file(${CMAKE_SOURCE_DIR}/uranie/references/flowrate_input_with_values_rows.in
    ${CMAKE_CURRENT_BINARY_DIR}/threaded COPYONLY)
configure_file(${CMAKE_SOURCE_DIR}/uranie/references/flowrateUniformDesign.dat
    ${CMAKE_CURRENT_BINARY_DIR}/threaded COPYONLY)

configure_file(${CMAKE_SOURCE_DIR}/uranie/references/flowrate_input_with_values_rows.in
    ${CMAKE_CURRENT_BINARY_DIR}/mpi COPYONLY)
configure_file(${CMAKE_SOURCE_DIR}/uranie/references/flowrateUniformDesign.dat
    ${CMAKE_CURRENT_BINARY_DIR}/mpi COPYONLY)

configure_file(${CMAKE_SOURCE_DIR}/uranie/references/flowrateUniformDesign.dat
    ${CMAKE_CURRENT_BINARY_DIR}/standalone COPYONLY)
configure_file(${CMAKE_SOURCE_DIR}/uranie/references/flowrate_input_with_values_rows.in
    ${CMAKE_CURRENT_BINARY_DIR}/standalone COPYONLY)
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/relauncherCodeFlowrateMpiStandalone.C
    ${CMAKE_CURRENT_BINARY_DIR}/standalone COPYONLY)

configure_file(${CMAKE_SOURCE_DIR}/uranie/references/flowrate_input_with_values_rows.in
    ${CMAKE_CURRENT_BINARY_DIR}/sequential_fail COPYONLY)
configure_file(${CMAKE_SOURCE_DIR}/uranie/references/flowrateUniformDesign.dat
    ${CMAKE_CURRENT_BINARY_DIR}/sequential_fail COPYONLY)

configure_file(${CMAKE_SOURCE_DIR}/uranie/references/readmultitype_sampling.dat
    ${CMAKE_CURRENT_BINARY_DIR}/multi_type COPYONLY)

configure_file(${CMAKE_SOURCE_DIR}/uranie/references/multitype_sampling.dat
    ${CMAKE_CURRENT_BINARY_DIR}/compose_multi_type COPYONLY)

configure_file(${CMAKE_SOURCE_DIR}/uranie/references/flowrate_input_with_keys.in
    ${CMAKE_CURRENT_BINARY_DIR}/sequential_temporary COPYONLY)

################## TESTS GENERATION IMAGE C++ ##################

ROOTTEST_ADD_TEST(relauncherFunctionFlowrateCInt MACRO relauncherFunctionFlowrateCInt.C
    MACROARG "\"relauncherFunctionFlowrateCInt.png\", \"${CMAKE_SOURCE_DIR}/uranie/doc/test_style_OLD.C\""
    WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/cint)

if(UNIX)
    ROOTTEST_ADD_TEST(relauncherFunctionFlowrateCJit COMMAND
        bash ${CMAKE_CURRENT_SOURCE_DIR}/relauncherFunctionFlowrateCJit.sh
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/cjit)

    ROOTTEST_ADD_TEST(relauncherCodeFlowrateMpiStandalone COMMAND
        bash ${CMAKE_CURRENT_SOURCE_DIR}/relauncherCodeFlowrateMpiStandalone.sh
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/standalone)
elseif(WIN32 AND DEFINED ENV{VISUAL_STUDIO_LAUNCHER} AND DEFINED ENV{VISUAL_STUDIO_ARCHITECTURE})
    ROOTTEST_ADD_TEST(relauncherFunctionFlowrateCJit COMMAND powershell -file
        ${CMAKE_CURRENT_SOURCE_DIR}/relauncherFunctionFlowrateCJit.ps1
        $ENV{VISUAL_STUDIO_LAUNCHER} $ENV{VISUAL_STUDIO_ARCHITECTURE}
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/cjit)

    ROOTTEST_ADD_TEST(relauncherCodeFlowrateMpiStandalone COMMAND powershell -file
        ${CMAKE_CURRENT_SOURCE_DIR}/relauncherCodeFlowrateMpiStandalone.ps1
        $ENV{VISUAL_STUDIO_LAUNCHER} $ENV{VISUAL_STUDIO_ARCHITECTURE}
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/standalone
        WILLFAIL)
endif()

set(PYROOT_EXTRAFLAGS_SAVED ${PYROOT_EXTRAFLAGS})
set(PYROOT_EXTRAFLAGS)
cmake_print_variables(CMAKE_CURRENT_LIST_FILE PYROOT_EXTRAFLAGS)

ROOTTEST_ADD_TEST(relauncherFunctionFlowrateCJit_draw MACRO
    ${CMAKE_SOURCE_DIR}/uranie/doc/relauncher/use_cases/draw_flowrate.py
    OPTS "_outputFile_functionflowrate_cjit_.dat" "relauncherFunctionFlowrateCJit.png"
        "${CMAKE_SOURCE_DIR}/uranie/doc/test_style_OLD.C"
    WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/cjit
    DEPENDS relauncherFunctionFlowrateCJit)

ROOTTEST_ADD_TEST(relauncherCodeFlowrateSequential MACRO relauncherCodeFlowrateSequential.C
    MACROARG "\"relauncherCodeFlowrateSequential.png\", \"${CMAKE_SOURCE_DIR}/uranie/doc/test_style_OLD.C\""
    WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/sequential)

ROOTTEST_ADD_TEST(relauncherCodeFlowrateThreaded MACRO relauncherCodeFlowrateThreaded.C
    MACROARG "\"relauncherCodeFlowrateThreaded.png\", \"${CMAKE_SOURCE_DIR}/uranie/doc/test_style_OLD.C\""
    WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/threaded)

find_package(MPI)
if(MPIEXEC_EXECUTABLE AND MPIEXEC_NUMPROC_FLAG)
    ROOTTEST_ADD_TEST(relauncherCodeFlowrateMPI COMMAND ${MPIEXEC_EXECUTABLE} ${MPIEXEC_NUMPROC_FLAG} 3
        ${ROOT_root_CMD} -b -q -l ${CMAKE_CURRENT_SOURCE_DIR}/relauncherCodeFlowrateMPI.C
        POSTCMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/relauncher/use_cases/draw_flowrate.py
            "_output_testFlowrateMPI_.dat" "relauncherCodeFlowrateMPI.png"
            "${CMAKE_SOURCE_DIR}/uranie/doc/test_style_OLD.C"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/mpi)
endif()

ROOTTEST_ADD_TEST(relauncherCodeFlowrateMpiStandalone_draw MACRO relauncherCodeFlowrateMpiStandalone_draw.C
    MACROARG "\"relauncherCodeFlowrateMpiStandalone.png\", \"${CMAKE_SOURCE_DIR}/uranie/doc/test_style_OLD.C\""
    WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/standalone
    DEPENDS relauncherCodeFlowrateMpiStandalone
    ${WILLFAIL_ON_WIN32})

ROOTTEST_ADD_TEST(relauncherCodeMultiTypeKey MACRO relauncherCodeMultiTypeKey.C
    MACROARG "\"relauncherCodeMultiTypeKey.png\", \"${CMAKE_SOURCE_DIR}/uranie/doc/test_style_OLD.C\", 12345"
    WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/key)

ROOTTEST_ADD_TEST(relauncherCodeMultiTypeKeyEmptyVectors MACRO relauncherCodeMultiTypeKeyEmptyVectors.C
    MACROARG "\"relauncherCodeMultiTypeKeyEmptyVectors.png\",
        \"${CMAKE_SOURCE_DIR}/uranie/doc/test_style_OLD.C\", 12345"
    WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/key_vector)

################## TESTS SORTIE CONSOLE + GENERATION IMAGE C++ ##################

ROOTTEST_ADD_TEST(relauncherCodeFlowrateSequentialFailure MACRO relauncherCodeFlowrateSequentialFailure.C
    MACROARG "\"relauncherCodeFlowrateSequentialFailure.png\",
        \"${CMAKE_SOURCE_DIR}/uranie/doc/test_style_OLD.C\""
    POSTCMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/delete_ansi.py
        ${CMAKE_CURRENT_BINARY_DIR}/relauncherCodeFlowrateSequentialFailure_clean.log
        ${CMAKE_CURRENT_BINARY_DIR}/relauncherCodeFlowrateSequentialFailure.log
    WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/sequential_fail
    PASSREGEX ${REGEX_CONSOLE_SEQUENTIAL_FAILURE})

ROOTTEST_ADD_TEST(relauncherCodeMultiTypeKeyEmptyVectorsAsFailure MACRO
    relauncherCodeMultiTypeKeyEmptyVectorsAsFailure.C
    MACROARG "\"relauncherCodeMultiTypeKeyEmptyVectorsAsFailure.png\",
        \"${CMAKE_SOURCE_DIR}/uranie/doc/test_style_OLD.C\", 12345"
    POSTCMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/delete_ansi.py
        ${CMAKE_CURRENT_BINARY_DIR}/relauncherCodeMultiTypeKeyEmptyVectorsAsFailure_clean.log
        ${CMAKE_CURRENT_BINARY_DIR}/relauncherCodeMultiTypeKeyEmptyVectorsAsFailure.log
    WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/key_vector_fail
    PASSREGEX ${REGEX_CONSOLE_VECTORS_AS_FAILURE})

################## TESTS SORTIE CONSOLE C++ ##################

ROOTTEST_ADD_TEST(relauncherCJitFunctionThreadTest MACRO relauncherCJitFunctionThreadTest.C
    MACROARG "123456"
    POSTCMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/delete_ansi.py
        ${CMAKE_CURRENT_BINARY_DIR}/relauncherCJitFunctionThreadTest_clean.log
        ${CMAKE_CURRENT_BINARY_DIR}/relauncherCJitFunctionThreadTest.log
    WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/thread_test
    PASSREGEX ${REGEX_CONSOLE_CJIT_FUNCTION_THREAD_TEST})

ROOTTEST_ADD_TEST(relauncherCodeFlowrateSequential_ConstantVar MACRO
    relauncherCodeFlowrateSequential_ConstantVar.C
    MACROARG "132456"
    WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/sequential_const
    PASSREGEX ${REGEX_CONSOLE_CONSTANT_VAR})

ROOTTEST_ADD_TEST(relauncherCodeReadMultiType MACRO relauncherCodeReadMultiType.C
    WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/multi_type
    PASSREGEX ${REGEX_CONSOLE_READ_MULTI_TYPE})

ROOTTEST_ADD_TEST(relauncherComposeMultitypeAndReadMultiType MACRO
    relauncherComposeMultitypeAndReadMultiType.C
    WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/compose_multi_type
    PASSREGEX ${REGEX_CONSOLE_AND_READ_MULTI_TYPE})

ROOTTEST_ADD_TEST(relauncherCodeFlowrateSequential_TemporaryVar MACRO
    relauncherCodeFlowrateSequential_TemporaryVar.C
    MACROARG "132456"
    WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/sequential_temporary
    PASSREGEX ${REGEX_CONSOLE_TEMPORARY_VAR})

######################### COMPARAISONS ##################################

include("${CMAKE_CURRENT_SOURCE_DIR}/../comparisons.cmake")

################## COMPARAISON TABLEAUX SORTIE CONSOLE ##################

if(PY_PANDAS_FOUND)
    ROOTTEST_ADD_TEST(relauncherCJitFunctionThreadTest_array
        MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_arrays.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/relauncher/use_cases/relauncherCJitFunctionThreadTest.json"
        "relauncherCJitFunctionThreadTest.json"
        PRECMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/convert_tree_json.py
        "relauncherCJitFunctionThreadTest.json" "_dataserver_.root" "pouet__data__tree__"
        "--sort" pouet__n__iter__
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/thread_test
        DEPENDS relauncherCJitFunctionThreadTest)
endif()

################## COMPARAISON IMAGES ##################

if(PY_SKIMAGE_FOUND)
    ROOTTEST_ADD_TEST(relauncherFunctionFlowrateCJit_figure
        MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_images.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/relauncher/use_cases/relauncherFunctionFlowrateCJit.png"
            "relauncherFunctionFlowrateCJit.png" "relauncherFunctionFlowrateCJit_diff.png"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/cjit
        DEPENDS relauncherFunctionFlowrateCJit_draw)

    ROOTTEST_ADD_TEST(relauncherCodeFlowrateMpiStandalone_figure
        MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_images.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/relauncher/use_cases/relauncherCodeFlowrateMpiStandalone.png"
            "relauncherCodeFlowrateMpiStandalone.png" "relauncherCodeFlowrateMpiStandalone_diff.png"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/standalone
        DEPENDS relauncherCodeFlowrateMpiStandalone_draw
        ${WILLFAIL_ON_WIN32})
endif()

set(PYROOT_EXTRAFLAGS ${PYROOT_EXTRAFLAGS_SAVED})
