#!/bin/bash

mpicc -o relaunStand relauncherCodeFlowrateMpiStandalone.C `echo $URANIECPPFLAG $URANIELDFLAG` -lstdc++

mpirun -np 3 ./relaunStand initType 0
mpirun -np 3 ./relaunStand initType 1
