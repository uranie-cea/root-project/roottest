set(STRING_CONSOLE_SEQUENTIAL_FAILURE "flowrate -s -rf has returned non-zero exit code (255).")
if(WIN32)
    set(STRING_CONSOLE_SEQUENTIAL_FAILURE "Cannot open")
endif()

################## COMPARAISON REGEX MESSAGES ###########################

ROOTTEST_ADD_TEST(relauncherCodeFlowrateSequentialFailure_console
    MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/substitution.py
    OPTS "relauncherCodeFlowrateSequentialFailure_clean.log" "string"
        "${CMAKE_CURRENT_BINARY_DIR}/sequential_fail" "\$\{RUNNINGDIR\}"
    POSTCMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/test_pattern.py
        "${CMAKE_CURRENT_BINARY_DIR}/relauncherCodeFlowrateSequentialFailure_clean.log" "string"
        "${STRING_CONSOLE_SEQUENTIAL_FAILURE}"
    DEPENDS relauncherCodeFlowrateSequentialFailure)

ROOTTEST_ADD_TEST(relauncherCodeMultiTypeKeyEmptyVectorsAsFailure_console
    MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/test_pattern.py
    OPTS "${CMAKE_CURRENT_BINARY_DIR}/relauncherCodeMultiTypeKeyEmptyVectorsAsFailure_clean.log" "regex"
        "TKeyResult\\(_output_multitype_mt_Key_\\.dat\\): v(1|2) Not found"
    DEPENDS relauncherCodeMultiTypeKeyEmptyVectorsAsFailure)

################## COMPARAISON TABLEAUX SORTIE CONSOLE ##################

find_python_module(pandas OPTIONAL)
if(PY_PANDAS_FOUND)
    ROOTTEST_ADD_TEST(relauncherCodeFlowrateSequential_ConstantVar_array
        MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_arrays.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/relauncher/use_cases/relauncherCodeFlowrateSequential_ConstantVar.json"
        "relauncherCodeFlowrateSequential_ConstantVar.json"
        PRECMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/convert_tree_json.py
        "relauncherCodeFlowrateSequential_ConstantVar.json" "_dataserver_.root" "foo__data__tree__"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/sequential_const
        DEPENDS relauncherCodeFlowrateSequential_ConstantVar)

    ROOTTEST_ADD_TEST(relauncherCodeFlowrateSequentialFailure_array
        MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_arrays.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/relauncher/use_cases/relauncherCodeFlowrateSequentialFailure.json"
        "relauncherCodeFlowrateSequentialFailure.json"
        PRECMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/convert_tree_json.py
        "relauncherCodeFlowrateSequentialFailure.json" "_dataserver_.root"
        "WrongComputations__data__tree__"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/sequential_fail
        DEPENDS relauncherCodeFlowrateSequentialFailure)

    ROOTTEST_ADD_TEST(relauncherCodeMultiTypeKeyEmptyVectorsAsFailure_array
        MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_arrays.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/relauncher/use_cases/relauncherCodeMultiTypeKeyEmptyVectorsAsFailure.json"
        "relauncherCodeMultiTypeKeyEmptyVectorsAsFailure.json"
        PRECMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/convert_tree_json.py
        "relauncherCodeMultiTypeKeyEmptyVectorsAsFailure.json" "_dataserver_.root"
        "WrongComputations__data__tree__"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/key_vector_fail
        DEPENDS relauncherCodeMultiTypeKeyEmptyVectorsAsFailure)

    ROOTTEST_ADD_TEST(relauncherCodeReadMultiType_array
        MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_arrays.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/relauncher/use_cases/relauncherCodeReadMultiType.json"
        "relauncherCodeReadMultiType.json"
        PRECMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/convert_tree_json.py
        "relauncherCodeReadMultiType.json" "readmultitype_sampling.root" "foo__data__tree__"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/multi_type
        DEPENDS relauncherCodeReadMultiType)

    ROOTTEST_ADD_TEST(relauncherComposeMultitypeAndReadMultiType_array
        MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_arrays.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/relauncher/use_cases/relauncherComposeMultitypeAndReadMultiType.json"
        "relauncherComposeMultitypeAndReadMultiType.json"
        PRECMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/convert_tree_json.py
        "relauncherComposeMultitypeAndReadMultiType.json" "multitype_sampling.root" "foo__data__tree__"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/compose_multi_type
        DEPENDS relauncherComposeMultitypeAndReadMultiType)

    ROOTTEST_ADD_TEST(relauncherCodeFlowrateSequential_TemporaryVar_array
        MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_arrays.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/relauncher/use_cases/relauncherCodeFlowrateSequential_TemporaryVar.json"
        "relauncherCodeFlowrateSequential_TemporaryVar.json"
        PRECMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/convert_tree_json.py
        "relauncherCodeFlowrateSequential_TemporaryVar.json" "_dataserver_.root" "foo__data__tree__"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/sequential_temporary
        DEPENDS relauncherCodeFlowrateSequential_TemporaryVar)
endif()

################## COMPARAISON IMAGES ##################

find_python_module(skimage OPTIONAL)
if(PY_SKIMAGE_FOUND)
    ROOTTEST_ADD_TEST(relauncherFunctionFlowrateCInt_figure
        MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_images.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/relauncher/use_cases/relauncherFunctionFlowrateCInt.png"
            "relauncherFunctionFlowrateCInt.png"
            "relauncherFunctionFlowrateCInt_diff.png"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/cint
        DEPENDS relauncherFunctionFlowrateCInt)

    ROOTTEST_ADD_TEST(relauncherCodeFlowrateSequential_figure
        MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_images.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/relauncher/use_cases/relauncherCodeFlowrateSequential.png"
            "relauncherCodeFlowrateSequential.png"
            "relauncherCodeFlowrateSequential_diff.png"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/sequential
        DEPENDS relauncherCodeFlowrateSequential)

    ROOTTEST_ADD_TEST(relauncherCodeFlowrateThreaded_figure
        MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_images.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/relauncher/use_cases/relauncherCodeFlowrateThreaded.png"
            "relauncherCodeFlowrateThreaded.png"
            "relauncherCodeFlowrateThreaded_diff.png"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/threaded
        DEPENDS relauncherCodeFlowrateThreaded)

    ROOTTEST_ADD_TEST(relauncherCodeFlowrateMPI_figure
        MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_images.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/relauncher/use_cases/relauncherCodeFlowrateMPI.png"
            "relauncherCodeFlowrateMPI.png"
            "relauncherCodeFlowrateMPI_diff.png"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/mpi
        DEPENDS relauncherCodeFlowrateMPI)

    ROOTTEST_ADD_TEST(relauncherCodeFlowrateSequentialFailure_figure
        MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_images.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/relauncher/use_cases/relauncherCodeFlowrateSequentialFailure.png"
            "relauncherCodeFlowrateSequentialFailure.png"
            "relauncherCodeFlowrateSequentialFailure_diff.png"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/sequential_fail
        DEPENDS relauncherCodeFlowrateSequentialFailure)

    ROOTTEST_ADD_TEST(relauncherCodeMultiTypeKey_figure
        MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_images.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/relauncher/use_cases/relauncherCodeMultiTypeKey.png"
            "relauncherCodeMultiTypeKey.png"
            "relauncherCodeMultiTypeKey_diff.png"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/key
        DEPENDS relauncherCodeMultiTypeKey)

    ROOTTEST_ADD_TEST(relauncherCodeMultiTypeKeyEmptyVectors_figure
        MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_images.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/relauncher/use_cases/relauncherCodeMultiTypeKeyEmptyVectors.png"
            "relauncherCodeMultiTypeKeyEmptyVectors.png"
            "relauncherCodeMultiTypeKeyEmptyVectors_diff.png"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/key_vector
        DEPENDS relauncherCodeMultiTypeKeyEmptyVectors)

    ROOTTEST_ADD_TEST(relauncherCodeMultiTypeKeyEmptyVectorsAsFailure_figure
        MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_images.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/relauncher/use_cases/relauncherCodeMultiTypeKeyEmptyVectorsAsFailure.png"
            "relauncherCodeMultiTypeKeyEmptyVectorsAsFailure.png"
            "relauncherCodeMultiTypeKeyEmptyVectorsAsFailure_diff.png"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/key_vector_fail
        DEPENDS relauncherCodeMultiTypeKeyEmptyVectorsAsFailure)
endif()
