set(REGEX_CONSOLE_SEQUENTIAL_FAILURE "Row[ ]+\\*[ ]+WrongComp[ ]+\\*[ ]+rw\\.rw[ ]+\\*[ ]+r\\.r[ ]+\\*[ ]+tu\\.tu[ ]+\\*[ ]+tl\\.tl[ ]+\\*[ ]+hu\\.hu[ ]+\\*[ ]+hl\\.hl[ ]+\\*[ ]+l\\.l[ ]+\\*[ ]+kw\\.kw[ ]+\\*[ ]+ystar\\.yst")
set(REGEX_CONSOLE_VECTORS_AS_FAILURE "Row[ ]+\\*[ ]+WrongComp[ ]+\\*[ ]+seed\\.seed")
set(REGEX_CONSOLE_CONSTANT_VAR "Row[ ]+\\*[ ]+foo__n[ ]+\\*[ ]+rw\\.rw[ ]+\\*[ ]+tu\\.tu[ ]+\\*[ ]+tl\\.tl[ ]+\\*[ ]+hu\\.hu[ ]+\\*[ ]+hl\\.hl[ ]+\\*[ ]+l\\.l[ ]+\\*[ ]+kw\\.kw[ ]+\\*[ ]+yhat\\.y[ ]+\\*[ ]+d\\.d[ ]+\\*[ ]+r\\.r")
set(REGEX_CONSOLE_TEMPORARY_VAR "Row[ ]+\\*[ ]+foo__n[ ]+\\*[ ]+rw\\.rw[ ]+\\*[ ]+tu\\.tu[ ]+\\*[ ]+tl\\.tl[ ]+\\*[ ]+hu\\.hu[ ]+\\*[ ]+hl\\.hl[ ]+\\*[ ]+l\\.l[ ]+\\*[ ]+kw\\.kw[ ]+\\*[ ]+yhat\\.y[ ]+\\*[ ]+incd\\.i")
set(REGEX_CONSOLE_READ_MULTI_TYPE "Row[ ]+\\*[ ]+thev1[ ]+\\*[ ]+thev2")
set(REGEX_CONSOLE_AND_READ_MULTI_TYPE "Row[ ]+\\*[ ]+thev1[ ]+\\*[ ]+thev2")
set(REGEX_CONSOLE_CJIT_FUNCTION_THREAD_TEST "Row[ ]+\\*[ ]+pouet__n_[ ]+\\*[ ]+multiplie[ ]+\\*[ ]+MultMean\\.")
