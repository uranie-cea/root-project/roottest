using namespace URANIE::DataServer;
using namespace URANIE::Sensitivity;

void sensitivityFASTFunctionFlowrate(const string& figure="figure.png", const string& style="", 
    const string& filename="", const string& treename="", int seed=0)
{
    if (!style.empty())
        gROOT->LoadMacro(style.data());

    gROOT->LoadMacro("UserFunctions.C");
        
    // Define the DataServer
    TDataServer *tds = new TDataServer("tdsflowreate", "DataBase flowreate");
    tds->addAttribute( new TUniformDistribution("rw", 0.05, 0.15));
    tds->addAttribute( new TUniformDistribution("r", 100.0, 50000.0));
    tds->addAttribute( new TUniformDistribution("tu", 63070.0, 115600.0));
    tds->addAttribute( new TUniformDistribution("tl", 63.1, 116.0));
    tds->addAttribute( new TUniformDistribution("hu", 990.0, 1110.0));
    tds->addAttribute( new TUniformDistribution("hl", 700.0, 820.0));
    tds->addAttribute( new TUniformDistribution("l", 1120.0, 1680.0));
    tds->addAttribute( new TUniformDistribution("kw", 9855.0, 12045.0));
      
    // \param Size of a sampling.
    Int_t nS = 4000;
    // Graph
    TFast * tfast = new TFast(tds, "flowrateModel", nS);
    if (seed > 0)
        tfast->setSeed(seed);
    tfast->setDrawProgressBar(kFALSE);
    tfast->computeIndexes("graph");

    tfast->getResultTuple()->Scan("Out:Inp:Order:Method:Value","Algo==\"--first--\"");

    TCanvas  *Canvas = (TCanvas*)gROOT->FindObject("__sensitivitycan__0");

    if (!style.empty())
        gROOT->ProcessLine("set_date()");

    Canvas->SaveAs(figure.data());

    if (!filename.empty() && !treename.empty())
    {
        ROOT::RDataFrame tfast_rdt(*tfast->getResultTuple());
        tfast_rdt.Snapshot(treename, filename, {"Out","Inp","Order","Method","Value"});
    }
}
