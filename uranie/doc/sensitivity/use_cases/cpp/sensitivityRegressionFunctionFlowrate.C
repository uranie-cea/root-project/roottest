using namespace URANIE::DataServer;
using namespace URANIE::Launcher;
using namespace URANIE::Sampler;
using namespace URANIE::Sensitivity;

void sensitivityRegressionFunctionFlowrate(const string& figure="figure.png", const string& style="", 
    const string& filename="", const string& treename="", int seed=0)
{
    if (!style.empty())
        gROOT->LoadMacro(style.data());
   
    gROOT->LoadMacro("UserFunctions.C");

    // Define the DataServer
    TDataServer *tds = new TDataServer("tdsflowreate", "DataBase flowreate");
    tds->addAttribute( new TUniformDistribution("rw", 0.05, 0.15));
    tds->addAttribute( new TUniformDistribution("r", 100.0, 50000.0));
    tds->addAttribute( new TUniformDistribution("tu", 63070.0, 115600.0));
    tds->addAttribute( new TUniformDistribution("tl", 63.1, 116.0));
    tds->addAttribute( new TUniformDistribution("hu", 990.0, 1110.0));
    tds->addAttribute( new TUniformDistribution("hl", 700.0, 820.0));
    tds->addAttribute( new TUniformDistribution("l", 1120.0, 1680.0));
    tds->addAttribute( new TUniformDistribution("kw", 9855.0, 12045.0));
      
    // \param Size of a sampling.
    Int_t nS = 4000;

    TSampling *sampling = new TSampling(tds, "lhs", nS);
    if (seed > 0)
        sampling->setSeed(seed);
    sampling->generateSample();	
      

    TLauncherFunction * tlf = new TLauncherFunction(tds, "flowrateModel");
    tlf->setDrawProgressBar(kFALSE);
    tlf->run();
      

    TRegression * treg = new TRegression(tds, "rw:r:tu:tl:hu:hl:l:kw","flowrateModel", "SRC");
    treg->computeIndexes();
    treg->getResultTuple()->SetScanField(60);

    treg->getResultTuple()->Scan("Out:Inp:Method:Algo:Value:CILower:CIUpper","Order==\"First\"", "colsize=5 col=6:8::9:8:8:8");
      

    TCanvas  *can = new TCanvas("c1", "Graph for the Macro sensitivityRegressionFunctionFlowrate",5,64,1270,667);
    treg->drawIndexes("Flowrate", "", "hist,first,nonewcanv");

    if (!style.empty())
        gROOT->ProcessLine("set_date()");

    can->SaveAs(figure.data()); 

    if (!filename.empty() && !treename.empty())
    {
        ROOT::RDataFrame treg_rdt(*treg->getResultTuple());
        treg_rdt.Snapshot(treename, filename, {"Out", "Inp", "Method", "Value", "CILower", "CIUpper"});
    }
}
