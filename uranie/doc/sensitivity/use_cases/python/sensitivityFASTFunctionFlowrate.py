"""
Example of the flowrate function sensitivity analysis
"""
from URANIE import DataServer, Sensitivity
import ROOT
import argparse

parser = argparse.ArgumentParser("Sobol estimation with a brut-force approach")
parser.add_argument("--figure", help="image filename", default="figure.png")
parser.add_argument("--style", help="macro filename", default="")
parser.add_argument("--filename", help="ROOT binary filename", default="")
parser.add_argument("--tree", help="ROOT binary treename", default="")
parser.add_argument("--seed", help="seed number", default=0, type=int)
args = parser.parse_args()

if args.style:
    ROOT.gROOT.LoadMacro(args.style)

ROOT.gROOT.LoadMacro("UserFunctions.C")

# Define the DataServer
tds = DataServer.TDataServer("tdsflowreate", "DataBase flowreate")
tds.addAttribute(DataServer.TUniformDistribution("rw", 0.05, 0.15))
tds.addAttribute(DataServer.TUniformDistribution("r", 100.0, 50000.0))
tds.addAttribute(DataServer.TUniformDistribution("tu", 63070.0, 115600.0))
tds.addAttribute(DataServer.TUniformDistribution("tl", 63.1, 116.0))
tds.addAttribute(DataServer.TUniformDistribution("hu", 990.0, 1110.0))
tds.addAttribute(DataServer.TUniformDistribution("hl", 700.0, 820.0))
tds.addAttribute(DataServer.TUniformDistribution("l", 1120.0, 1680.0))
tds.addAttribute(DataServer.TUniformDistribution("kw", 9855.0, 12045.0))

# \param Size of a sampling.
nS = 4000
# Graph
fast = Sensitivity.TFast(tds, "flowrateModel", nS)
if args.seed > 0:
    fast.setSeed(args.seed)
fast.setDrawProgressBar(False)
fast.computeIndexes("graph")

fast.getResultTuple().Scan("Out:Inp:Order:Method:Value", "Algo==\"--first--\"")

Canvas = ROOT.gROOT.FindObject("__sensitivitycan__0")
Canvas.SetCanvasSize(696, 472)
if args.style:
    ROOT.gROOT.ProcessLine("set_date()")
Canvas.SaveAs(args.figure)

if args.filename and args.tree:
    fast_rdt = ROOT.RDataFrame(fast.getResultTuple())
    fast_rdt.Snapshot(args.tree, args.filename, ["Out","Inp","Order","Method","Value"])
