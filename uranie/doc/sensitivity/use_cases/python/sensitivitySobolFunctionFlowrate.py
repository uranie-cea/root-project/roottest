"""
Example of Sobol estimation for the flowrate function
"""
from URANIE import DataServer, Sensitivity
import ROOT
import argparse

parser = argparse.ArgumentParser("Sobol estimation for the flowrate function")
parser.add_argument("--figure", help="image filename", default="figure.png")
parser.add_argument("--style", help="macro filename", default="")
parser.add_argument("--filename", help="ROOT binary filename", default="")
parser.add_argument("--tree", help="ROOT binary treename", default="")
parser.add_argument("--seed", help="seed number", default=0, type=int)
args = parser.parse_args()

if args.style:
    ROOT.gROOT.LoadMacro(args.style)

ROOT.gROOT.LoadMacro("UserFunctions.C")

# Define the DataServer
tds = DataServer.TDataServer("tdsflowreate", "DataBase flowreate")
tds.addAttribute(DataServer.TUniformDistribution("rw", 0.05, 0.15))
tds.addAttribute(DataServer.TUniformDistribution("r", 100.0, 50000.0))
tds.addAttribute(DataServer.TUniformDistribution("tu", 63070.0, 115600.0))
tds.addAttribute(DataServer.TUniformDistribution("tl", 63.1, 116.0))
tds.addAttribute(DataServer.TUniformDistribution("hu", 990.0, 1110.0))
tds.addAttribute(DataServer.TUniformDistribution("hl", 700.0, 820.0))
tds.addAttribute(DataServer.TUniformDistribution("l", 1120.0, 1680.0))
tds.addAttribute(DataServer.TUniformDistribution("kw", 9855.0, 12045.0))

ns = 100000
tsobol = Sensitivity.TSobol(tds, "flowrateModel", ns, "rw:r:tu:tl:hu:hl:l:kw",
                            "flowrateModel", "DummyPython")
if args.seed > 0:
    tsobol.setSeed(args.seed)
tsobol.setDrawProgressBar(False)
tsobol.computeIndexes()

tsobol.getResultTuple().Scan("*", "Algo==\"--first--\" || Algo==\"--total--\"")

cc = ROOT.TCanvas("c1", "histgramme", 5, 64, 1270, 667)
pad = ROOT.TPad("pad", "pad", 0, 0.03, 1, 1)
pad.Draw()
if args.style:
    ROOT.gROOT.ProcessLine("set_date()")
pad.Divide(2, 1)
pad.cd(1)
tsobol.drawIndexes("Flowrate", "", "nonewcanv, hist, all")

pad.cd(2)
tsobol.drawIndexes("Flowrate", "", "nonewcanv, pie, first")

ROOT.gSystem.Rename("_sobol_launching_.dat", "ref_sobol_launching_.dat")
tds.exportData("_onlyMandN_sobol_launching_.dat",
               "rw:r:tu:tl:hu:hl:l:kw:flowrateModel",
               "sobol__n__iter__tdsflowreate < 100")

cc.SaveAs(args.figure)

if args.filename and args.tree:
    tsobol_rdt = ROOT.RDataFrame(tsobol.getResultTuple())
    tsobol_rdt.Snapshot(args.tree, args.filename, ["Out", "Inp", "Order", "Method", "Value", "CILower", "CIUpper"])
