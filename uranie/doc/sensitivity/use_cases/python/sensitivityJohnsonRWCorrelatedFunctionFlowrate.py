"""
Example of Johson relative weight method applied to flowrate
"""
from math import sqrt
from URANIE import DataServer, Sensitivity
import ROOT
import argparse

parser = argparse.ArgumentParser("Johson relative weight method applied to flowrate")
parser.add_argument("--style", help="macro filename", default="")
parser.add_argument("--basename", help="ROOT binary basename", default="")
parser.add_argument("--tree", help="ROOT binary treename", default="")
parser.add_argument("--seed", help="seed number", default=0, type=int)
parser.add_argument("--no-progressBar", dest="progressBar", help="deactivate progress bar", action="store_false")
args = parser.parse_args()

def gen_corr(_nx=8, correlated=True, seed=0):
    """Generate randomly a good, highly-correlated, input correlation matrix."""
    if seed > 0:
        ROOT.gRandom.SetSeed(seed)
    # Define a randomly filled matrix
    a_mat = ROOT.TMatrixD(_nx, _nx)
    for i in range(_nx):
        for j in range(_nx):
            a_mat[i][j] = ROOT.gRandom.Gaus(0, 1)

    # Compute AA^T and normalise it to get "covariance matrix"
    gamma = ROOT.TMatrixD(a_mat, ROOT.TMatrixD.kMultTranspose, a_mat)
    gamma *= 1. / _nx

    # Inverse of the diagonal matrix to do as if this was 1/sqrt(variance)
    sig = ROOT.TMatrixD(_nx, _nx)
    for i in range(_nx):
        sig[i][i] = 1. / sqrt(gamma[i][i])

    # Compute the input correlation matrix
    in_corr_right = ROOT.TMatrixD(gamma, ROOT.TMatrixD.kMult, sig)
    in_corr = ROOT.TMatrixD(sig, ROOT.TMatrixD.kMult, in_corr_right)
    if not correlated:
        in_corr.UnitMatrix()

    return in_corr

if args.style:
    ROOT.gROOT.LoadMacro(args.style)

ROOT.gROOT.LoadMacro("UserFunctions.C")

# Define the DataServer
tds = DataServer.TDataServer("tdsflowrate", "DataBase flowrate")
tds.addAttribute(DataServer.TUniformDistribution("rw", 0.05, 0.15))
tds.addAttribute(DataServer.TUniformDistribution("r", 100.0, 50000.0))
tds.addAttribute(DataServer.TUniformDistribution("tu", 63070.0, 115600.0))
tds.addAttribute(DataServer.TUniformDistribution("tl", 63.1, 116.0))
tds.addAttribute(DataServer.TUniformDistribution("hu", 990.0, 1110.0))
tds.addAttribute(DataServer.TUniformDistribution("hl", 700.0, 820.0))
tds.addAttribute(DataServer.TUniformDistribution("l", 1120.0, 1680.0))
tds.addAttribute(DataServer.TUniformDistribution("kw", 9855.0, 12045.0))

# \param Size of a sampling.
nS = 1000
FuncName = "flowrateModel"

# Get a correlation matrix for the inputs
inCorr = gen_corr(8, True, args.seed)
inCorr.Print()

tjrw = Sensitivity.TJohnsonRW(tds, FuncName, nS,
                              "rw:r:tu:tl:hu:hl:l:kw", FuncName)
if args.seed > 0:
    tjrw.setSeed(args.seed)
tjrw.setDrawProgressBar(args.progressBar)
# Set the correlation
tjrw.setInputCorrelationMatrix(inCorr)
tjrw.computeIndexes()

# Get the results on screen
tjrw.getResultTuple().Scan("Out:Inp:Method:Value", "Order==\"First\"")

# Get the results as plots
cc = ROOT.TCanvas("canhist", "histgramme")
tjrw.drawIndexes("Flowrate", "", "nonewcanv, hist, first")
if args.style:
    ROOT.gROOT.ProcessLine("set_date()")
cc.Print("appliUranieFlowrateJohnsonRWCorrelated1000Histogram_py.png")

ccc = ROOT.TCanvas("canpie", "TPie")
tjrw.drawIndexes("Flowrate", "", "nonewcanv, pie")
if args.style:
    ROOT.gROOT.ProcessLine("set_date()")
ccc.Print("appliUranieFlowrateJohnsonRWCorrelated1000Pie_py.png")

if args.basename and args.tree:
    inCorr_json = ROOT.TBufferJSON.ToJSON(inCorr)

    with open(args.basename+"_matrix.json", "w") as file:
        file.write(str(inCorr_json))

    tjrw_rdt = ROOT.RDataFrame(tjrw.getResultTuple())
    tjrw_rdt.Snapshot(args.tree, args.basename+".root", ["Out", "Inp", "Method", "Value"])
