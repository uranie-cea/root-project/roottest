"""
Example of sensitivity analysis through linear regression
"""
from URANIE import DataServer, Sensitivity
import ROOT
import argparse

parser = argparse.ArgumentParser("Sensitivity analysis through linear regression")
parser.add_argument("--figure", help="image filename", default="figure.png")
parser.add_argument("--style", help="macro filename", default="")
parser.add_argument("--filename", help="ROOT binary filename", default="")
parser.add_argument("--tree", help="ROOT binary treename", default="")
args = parser.parse_args()

if args.style:
    ROOT.gROOT.LoadMacro(args.style)

# Create a DataServer.TDataServer
tds = DataServer.TDataServer()
# Load a database in an ASCII file
tds.fileDataRead("flowrateUniformDesign.dat")

# Graph
Canvas = ROOT.TCanvas("c2", "Graph for the Macro", 5, 64, 1270, 667)
# Visualisation
tds.Draw("ystar:rw")

# Sensitivity analysis
treg = Sensitivity.TRegression(tds, "rw:r:tu:tl:hu:hl:l:kw", "ystar", "src")
treg.computeIndexes()

treg.drawIndexes("Flowrate", "", "hist, first")
# treg.getResultTuple().Scan()

# Graph
c = ROOT.gROOT.FindObject("__sensitivitycan__0")
can = ROOT.TCanvas("c1", "Graph for the Macro sensitivityDataBaseFlowrate",
                   5, 64, 1270, 667)
pad = ROOT.TPad("pad", "pad", 0, 0.03, 1, 1)
pad.Draw()
if args.style:
    ROOT.gROOT.ProcessLine("set_date()")
pad.Divide(2)
pad.cd(1)
Canvas.DrawClonePad()
pad.cd(2)
c.DrawClonePad()

can.SaveAs(args.figure)

if args.filename and args.tree:
    treg_rdt = ROOT.RDataFrame(treg.getResultTuple())
    treg_rdt.Snapshot(args.tree, args.filename, ["Out", "Inp", "Order", "Method", "Value", "CILower", "CIUpper"])
