"""
Example of Johnson relative weight method applied to flowrate
"""
from URANIE import DataServer, Sensitivity
import ROOT
import argparse

parser = argparse.ArgumentParser("Johnson relative weight method applied to flowrate")
parser.add_argument("--style", help="macro filename", default="")
parser.add_argument("--filename", help="ROOT binary filename", default="")
parser.add_argument("--tree", help="ROOT binary treename", default="")
parser.add_argument("--seed", help="seed number", default=0, type=int)
parser.add_argument("--no-progressBar", dest="progressBar", help="deactivate progress bar", action="store_false")
args = parser.parse_args()

if args.style:
    ROOT.gROOT.LoadMacro(args.style)

ROOT.gROOT.LoadMacro("UserFunctions.C")

# Define the DataServer
tds = DataServer.TDataServer("tdsflowrate", "DataBase flowrate")
tds.addAttribute(DataServer.TUniformDistribution("rw", 0.05, 0.15))
tds.addAttribute(DataServer.TUniformDistribution("r", 100.0, 50000.0))
tds.addAttribute(DataServer.TUniformDistribution("tu", 63070.0, 115600.0))
tds.addAttribute(DataServer.TUniformDistribution("tl", 63.1, 116.0))
tds.addAttribute(DataServer.TUniformDistribution("hu", 990.0, 1110.0))
tds.addAttribute(DataServer.TUniformDistribution("hl", 700.0, 820.0))
tds.addAttribute(DataServer.TUniformDistribution("l", 1120.0, 1680.0))
tds.addAttribute(DataServer.TUniformDistribution("kw", 9855.0, 12045.0))

# param Size of a sampling.
nS = 1000
FuncName = "flowrateModel"

tjrw = Sensitivity.TJohnsonRW(tds, FuncName, nS,
                              "rw:r:tu:tl:hu:hl:l:kw", FuncName)
if args.seed > 0:
    tjrw.setSeed(args.seed)
tjrw.setDrawProgressBar(args.progressBar)
tjrw.computeIndexes()

# Get the results on screen
tjrw.getResultTuple().Scan("Out:Inp:Method:Value", "Order==\"First\"")

# Get the results as plots
cc = ROOT.TCanvas("canhist", "histgramme")
tjrw.drawIndexes("Flowrate", "", "nonewcanv,hist,first")
if args.style:
    ROOT.gROOT.ProcessLine("set_date()")
cc.Print("appliUranieFlowrateJohnsonRW1000Histogram_py.png")

ccc = ROOT.TCanvas("canpie", "TPie")
tjrw.drawIndexes("Flowrate", "", "nonewcanv,pie")
if args.style:
    ROOT.gROOT.ProcessLine("set_date()")
ccc.Print("appliUranieFlowrateJohnsonRW1000Pie_py.png")

if args.filename and args.tree:
    tjrw_rdt = ROOT.RDataFrame(tjrw.getResultTuple())
    tjrw_rdt.Snapshot(args.tree, args.filename, ["Out", "Inp", "Method", "Value"])
