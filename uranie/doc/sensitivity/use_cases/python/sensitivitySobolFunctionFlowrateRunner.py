"""
Example of Sobol estimation for the flowrate function with Relauncher approach
"""
from URANIE import DataServer, Relauncher, Sensitivity
import ROOT
import argparse

parser = argparse.ArgumentParser("Sobol estimation for the flowrate function with Relauncher approach")
parser.add_argument("--figure", help="image filename", default="figure.png")
parser.add_argument("--style", help="macro filename", default="")
parser.add_argument("--filename", help="ROOT binary filename", default="")
parser.add_argument("--tree", help="ROOT binary treename", default="")
parser.add_argument("--seed", help="seed number", default=0, type=int)
args = parser.parse_args()

ROOT.gROOT.LoadMacro("UserFunctions.C")

# Define the DataServer
rw = DataServer.TUniformDistribution("rw", 0.05, 0.15)
r = DataServer.TUniformDistribution("r", 100.0, 50000.0)
tu = DataServer.TUniformDistribution("tu", 63070.0, 115600.0)
tl = DataServer.TUniformDistribution("tl", 63.1, 116.0)
hu = DataServer.TUniformDistribution("hu", 990.0, 1110.0)
hl = DataServer.TUniformDistribution("hl", 700.0, 820.0)
lvar = DataServer.TUniformDistribution("l", 1120.0, 1680.0)
kw = DataServer.TUniformDistribution("kw", 9855.0, 12045.0)

# Create the evaluator
code = Relauncher.TCIntEval("flowrateModel")
# Create output attribute
yout = DataServer.TAttribute("flowrateModel")
# Provide input/output attributes to the assessor
code.addInput(rw)
code.addInput(r)
code.addInput(tu)
code.addInput(tl)
code.addInput(hu)
code.addInput(hl)
code.addInput(lvar)
code.addInput(kw)
code.addOutput(yout)

run = Relauncher.TSequentialRun(code)  # Replace to distribute computation
run.startSlave()
if run.onMaster():
    if args.style:
        ROOT.gROOT.LoadMacro(args.style)
    # Create the dataserver
    tds = DataServer.TDataServer("sobol", "foo bar pouet chocolat")
    code.addAllInputs(tds)

    # Create the sobol object
    ns = 100000
    tsobol = Sensitivity.TSobol(tds, run, ns)
    if args.seed > 0:
        tsobol.setSeed(args.seed)
    tsobol.setDrawProgressBar(ROOT.kFALSE)
    tsobol.computeIndexes()

    cc = ROOT.TCanvas("c1", "histgramme", 5, 64, 1270, 667)
    pad = ROOT.TPad("pad", "pad", 0, 0.03, 1, 1)
    pad.Draw()
    if args.style:
        ROOT.gROOT.ProcessLine("set_date()")
    pad.Divide(2, 1)
    pad.cd(1)
    tsobol.drawIndexes("Flowrate", "", "nonewcanv, hist, all")

    pad.cd(2)
    tsobol.drawIndexes("Flowrate", "", "nonewcanv, pie, first")

    tds.exportData("_sobol_launching_.dat")

    cc.SaveAs(args.figure)

    if args.filename and args.tree:
        tsobol_rdt = ROOT.RDataFrame(tsobol.getResultTuple())
        tsobol_rdt.Snapshot(args.tree, args.filename, ["Inp", "Order", "Value"])
