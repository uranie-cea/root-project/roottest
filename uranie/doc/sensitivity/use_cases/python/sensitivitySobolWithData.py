"""
Example of sobol estimation using provided data
"""
from URANIE import DataServer, Sensitivity
import ROOT
import argparse

parser = argparse.ArgumentParser("Sobol estimation using provided data")
parser.add_argument("--figure", help="image filename", default="figure.png")
parser.add_argument("--style", help="macro filename", default="")
parser.add_argument("--filename", help="ROOT binary filename", default="")
parser.add_argument("--tree", help="ROOT binary treename", default="")
args = parser.parse_args()

if args.style:
    ROOT.gROOT.LoadMacro(args.style)

ROOT.gROOT.LoadMacro("UserFunctions.C")
# Define the DataServer
tds = DataServer.TDataServer("tdsflowreate", "DataBase flowrate")
tds.fileDataRead("_onlyMandN_sobol_launching_.dat")

ns = 10000
tsobol = Sensitivity.TSobol(tds, "flowrateModel", ns, "rw:r:tu:tl:hu:hl:l:kw",
                            "flowrateModel", "WithData")
tsobol.setDrawProgressBar(False)
tsobol.computeIndexes()

cc = ROOT.TCanvas("c1", "histgramme", 5, 64, 1270, 667)
pad = ROOT.TPad("pad", "pad", 0, 0.03, 1, 1)
pad.Draw()
if args.style:
    ROOT.gROOT.ProcessLine("set_date()")
pad.Divide(2, 1)
pad.cd(1)
tsobol.drawIndexes("Flowrate", "", "nonewcanv, hist, all")

pad.cd(2)
tsobol.drawIndexes("Flowrate", "", "nonewcanv, pie, first")

cc.SaveAs(args.figure)

if args.filename and args.tree:
    tsobol_rdt = ROOT.RDataFrame(tsobol.getResultTuple())
    tsobol_rdt.Snapshot(args.tree, args.filename, ["Inp", "Order", "Value"])
