"""
Example of regression approach on the flowrate function
"""
from URANIE import DataServer, Launcher, Sampler, Sensitivity
import ROOT
import argparse

parser = argparse.ArgumentParser("Regression approach on the flowrate function")
parser.add_argument("--figure", help="image filename", default="figure.png")
parser.add_argument("--style", help="macro filename", default="")
parser.add_argument("--filename", help="ROOT binary filename", default="")
parser.add_argument("--tree", help="ROOT binary treename", default="")
parser.add_argument("--seed", help="seed number", default=0, type=int)
args = parser.parse_args()

if args.style:
    ROOT.gROOT.LoadMacro(args.style)

ROOT.gROOT.LoadMacro("UserFunctions.C")

# Define the DataServer
tds = DataServer.TDataServer("tdsflowreate", "DataBase flowreate")
tds.addAttribute(DataServer.TUniformDistribution("rw", 0.05, 0.15))
tds.addAttribute(DataServer.TUniformDistribution("r", 100.0, 50000.0))
tds.addAttribute(DataServer.TUniformDistribution("tu", 63070.0, 115600.0))
tds.addAttribute(DataServer.TUniformDistribution("tl", 63.1, 116.0))
tds.addAttribute(DataServer.TUniformDistribution("hu", 990.0, 1110.0))
tds.addAttribute(DataServer.TUniformDistribution("hl", 700.0, 820.0))
tds.addAttribute(DataServer.TUniformDistribution("l", 1120.0, 1680.0))
tds.addAttribute(DataServer.TUniformDistribution("kw", 9855.0, 12045.0))

# Size of a sampling.
nS = 4000

sampling = Sampler.TSampling(tds, "lhs", nS)
if args.seed > 0:
    sampling.setSeed(args.seed)
sampling.generateSample()

tlf = Launcher.TLauncherFunction(tds, "flowrateModel")
tlf.setDrawProgressBar(False)
tlf.run()

treg = Sensitivity.TRegression(tds, "rw:r:tu:tl:hu:hl:l:kw",
                               "flowrateModel", "SRC")
treg.computeIndexes()
treg.getResultTuple().SetScanField(60)

treg.getResultTuple().Scan("Out:Inp:Method:Algo:Value:CILower:CIUpper",
                           "Order==\"First\"","colsize=5 col=6:8::9:8:8:8")

can = ROOT.TCanvas("c1", "Graph sensitivityRegressionFunctionFlowrate",
                   5, 64, 1270, 667)
treg.drawIndexes("Flowrate", "", "hist, first, nonewcanv")

if args.style:
    ROOT.gROOT.ProcessLine("set_date()")

can.SaveAs(args.figure)

if args.filename and args.tree:
    treg_rdt = ROOT.RDataFrame(treg.getResultTuple())
    treg_rdt.Snapshot(args.tree, args.filename, ["Out", "Inp", "Method", "Value", "CILower", "CIUpper"])
