"""
Example of Morris analysis on flowrate with a Relauncher approach
"""
from URANIE import DataServer, Relauncher, Sensitivity
import ROOT
import argparse

parser = argparse.ArgumentParser("Morris analysis on flowrate with a Relauncher approach")
parser.add_argument("--figure", help="image filename", default="figure.png")
parser.add_argument("--style", help="macro filename", default="")
parser.add_argument("--filename", help="ROOT binary filename", default="")
parser.add_argument("--tree", help="ROOT binary treename", default="")
parser.add_argument("--seed", help="seed number", default=0, type=int)
args = parser.parse_args()

if args.style:
    ROOT.gROOT.LoadMacro(args.style)

ROOT.gROOT.LoadMacro("UserFunctions.C")

# Define the DataServer
rw = DataServer.TUniformDistribution("rw", 0.05, 0.15)
r = DataServer.TUniformDistribution("r", 100.0, 50000.0)
tu = DataServer.TUniformDistribution("tu", 63070.0, 115600.0)
tl = DataServer.TUniformDistribution("tl", 63.1, 116.0)
hu = DataServer.TUniformDistribution("hu", 990.0, 1110.0)
hl = DataServer.TUniformDistribution("hl", 700.0, 820.0)
lvar = DataServer.TUniformDistribution("l", 1120.0, 1680.0)
kw = DataServer.TUniformDistribution("kw", 9855.0, 12045.0)

# Create the evaluator
code = Relauncher.TCIntEval("flowrateModel")
# Create output attribute
yout = DataServer.TAttribute("flowrateModel")
# Provide input/output attributes to the assessor
code.addInput(rw)
code.addInput(r)
code.addInput(tu)
code.addInput(tl)
code.addInput(hu)
code.addInput(hl)
code.addInput(lvar)
code.addInput(kw)
code.addOutput(yout)

run = Relauncher.TSequentialRun(code)  # Replace to distribute computation
run.startSlave()
if run.onMaster():
    # Create the dataserver
    tds = DataServer.TDataServer("sobol", "foo bar pouet chocolat")
    tds.addAttribute(rw)
    tds.addAttribute(r)
    tds.addAttribute(tu)
    tds.addAttribute(tl)
    tds.addAttribute(hu)
    tds.addAttribute(hl)
    tds.addAttribute(lvar)
    tds.addAttribute(kw)

    # Create the Morris object
    nreplique = 3
    nlevel = 10
    scmo = Sensitivity.TMorris(tds, run, nreplique, nlevel)
    if args.seed > 0:
        scmo.setSeed(args.seed)
    scmo.setDrawProgressBar(False)
    scmo.generateSample()

    tds.exportData("_morris_sampling_.dat")
    scmo.computeIndexes()

    tds.exportData("_morris_launching_.dat")

    ntresu = scmo.getMorrisResults()
    ntresu.Scan("*")

    # Graph
    canmoralltraj = ROOT.gROOT.FindObject("canmoralltraj")
    can = ROOT.TCanvas("c1", "Graph sensitivityMorrisFunctionFlowrateRunner",
                       5, 64, 1270, 667)
    pad = ROOT.TPad("pad", "pad", 0, 0.03, 1, 1)
    pad.Draw()
    if args.style:
        ROOT.gROOT.ProcessLine("set_date()")
    pad.Divide(2)
    pad.cd(1)
    scmo.drawSample("", -1, "nonewcanv")
    pad.cd(2)
    scmo.drawIndexes("mustar", "", "nonewcanv")

    can.SaveAs(args.figure)

    if args.filename and args.tree:
        ntresu_rdt = ROOT.RDataFrame(ntresu)
        ntresu_rdt.Snapshot(args.tree, args.filename)
