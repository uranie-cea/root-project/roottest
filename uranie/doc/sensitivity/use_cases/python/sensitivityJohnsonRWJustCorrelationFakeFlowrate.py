"""
Example of Johnson relative weight applied only to a correlation matrix
"""
from math import sqrt
from URANIE import DataServer, Sensitivity
import ROOT
import argparse

parser = argparse.ArgumentParser("Johnson relative weight applied only to a correlation matrix")
parser.add_argument("--style", help="macro filename", default="")
parser.add_argument("--filename", help="ROOT binary filename", default="")
parser.add_argument("--tree", help="ROOT binary treename", default="")
parser.add_argument("--seed", help="seed number", default=0, type=int)
parser.add_argument("--no-progressBar", dest="progressBar", help="deactivate progress bar", action="store_false")
args = parser.parse_args()

def gen_corr(_nx=8, correlated=True, seed=0):
    """Generate randomly a good, highly-correlated, correlation matrix for inputs
    define the proper covariance with the output to do as if this output if a
    perfect linear combination of the inputs."""
    if seed > 0:
        ROOT.gRandom.SetSeed(seed)
    # Define a randomly filled matrix
    a_mat = ROOT.TMatrixD(_nx, _nx)
    for i in range(_nx):
        for j in range(_nx):
            a_mat[i][j] = ROOT.gRandom.Gaus(0, 1)

    # Compute AA^T and normalise it to get "covariance matrix"
    gamma = ROOT.TMatrixD(a_mat, ROOT.TMatrixD.kMultTranspose, a_mat)
    gamma *= 1. / _nx

    # Inverse of the diagonal matrix to do as if this was 1/sqrt(variance)
    sig = ROOT.TMatrixD(_nx, _nx)
    for i in range(_nx):
        sig[i][i] = 1. / sqrt(gamma[i][i])

    # Compute the input correlation matrix
    in_corr_right = ROOT.TMatrixD(gamma, ROOT.TMatrixD.kMult, sig)
    in_corr = ROOT.TMatrixD(sig, ROOT.TMatrixD.kMult, in_corr_right)
    if not correlated:
        in_corr.UnitMatrix()
    var_y = in_corr.Sum()

    # Proper correlation, output included
    corr = ROOT.TMatrixD(_nx+1, _nx+1)
    corr.UnitMatrix()
    # putting already defined input
    corr.SetSub(0, 0, in_corr)
    # Adjust the covariance of the output wrt to all inputs
    for i in range(_nx):
        value = 0
        for j in range(_nx):
            value += in_corr[i][j]

        corr[_nx][i] = corr[i][_nx] = value / sqrt(var_y)

    return corr

if args.style:
    ROOT.gROOT.LoadMacro(args.style)

# Define the DataServer
tds = DataServer.TDataServer("tdsflowrate", "DataBase flowrate")
tds.addAttribute("rw")
tds.addAttribute("r")
tds.addAttribute("tu")
tds.addAttribute("tl")
tds.addAttribute("hu")
tds.addAttribute("hl")
tds.addAttribute("l")
tds.addAttribute("kw")
# outputs
tds.addAttribute("flowrateModel")
tds.getAttribute("flowrateModel").setOutput()


# Get the full correlation matrix
inCorr = gen_corr(8, True, args.seed)

# Johnson definition
tjrw = Sensitivity.TJohnsonRW(tds, "rw:r:tu:tl:hu:hl:l:kw", "flowrateModel")
# Putting the newly defined correlation that states our output
# as a perfect linear combination of inputs
tjrw.setCorrelationMatrix(inCorr)
tjrw.computeIndexes()

# Get the results on screen
tjrw.getResultTuple().Scan("Out:Inp:Method:Value", "Order==\"First\"")

# Get the results as plots
cc = ROOT.TCanvas("canhist", "histgramme")
tjrw.drawIndexes("Flowrate", "", "nonewcanv, hist, first")
if args.style:
    ROOT.gROOT.ProcessLine("set_date()")
cc.Print("appliUranieFakeFlowrateJohnsonRWCorrelationHistogram_py.png")

ccc = ROOT.TCanvas("canpie", "TPie")
tjrw.drawIndexes("Flowrate", "", "nonewcanv, pie")
if args.style:
    ROOT.gROOT.ProcessLine("set_date()")
ccc.Print("appliUranieFakeFlowrateJohnsonRWCorrelationPie_py.png")

if args.filename and args.tree:
    tjrw_rdt = ROOT.RDataFrame(tjrw.getResultTuple())
    tjrw_rdt.Snapshot(args.tree, args.filename, ["Out", "Inp", "Method", "Value"])
