"""
Example of Sobol estimation with a brut-force approach
"""
from URANIE import DataServer, Launcher, Sampler
import ROOT
import argparse, json

parser = argparse.ArgumentParser("Sobol estimation with a brut-force approach")
parser.add_argument("--figure", help="image filename", default="figure.png")
parser.add_argument("--style", help="macro filename", default="")
parser.add_argument("--json", help="JSON filename", default="")
args = parser.parse_args()

def draw_bar_with_tuple(val, name, stitle):
    """Draw the results estimated with brut-force approach."""
    h_div = ROOT.TH1F("hDivdrawBarWithTuple", stitle, 3, 0, 3)
    h_div.SetCanExtend(ROOT.TH1.kXaxis)  # .SetBit(ROOT.TH1.kCanRebin)
    h_div.SetStats(0)

    if h_div != 0:
        h_div.SetBarWidth(0.45)
        h_div.SetBarOffset(0.1)
        h_div.SetMarkerColor(2)
        h_div.SetMarkerSize(2)
        h_div.SetFillColor(49)
        h_div.SetTitle(stitle)
        for ite, value in enumerate(val):
            h_div.Fill(name[ite], value)
        h_div.LabelsDeflate()
        h_div.LabelsOption(">u")
        h_div.SetMinimum(0.0)
        h_div.SetMaximum(1.0)
        ROOT.gStyle.SetPaintTextFormat("5.2f")
        h_div.Draw("bar2, text45")
    return h_div

if args.style:
    ROOT.gROOT.LoadMacro(args.style)

nCond = 50
nbins = 10
# Create a DataServer.TDataServer
tds = DataServer.TDataServer()

print(" ******************************************************")
print(" ** sensitivityBrutForceMethodFlowrate nbins[%i] nCond[%i]" %
      (nbins, nCond))
print(" **")

# Create a DataServer.TDataServer
tds = DataServer.TDataServer()
# Add the eight attributes of the study with uniform law
tds.addAttribute(DataServer.TUniformDistribution("rw", 0.05, 0.15))
tds.addAttribute(DataServer.TUniformDistribution("r", 100.0, 50000.0))
tds.addAttribute(DataServer.TUniformDistribution("tu", 63070.0, 115600.0))
tds.addAttribute(DataServer.TUniformDistribution("tl", 63.1, 116.0))
tds.addAttribute(DataServer.TUniformDistribution("hu", 990.0, 1110.0))
tds.addAttribute(DataServer.TUniformDistribution("hl", 700.0, 820.0))
tds.addAttribute(DataServer.TUniformDistribution("l", 1120.0, 1680.0))
tds.addAttribute(DataServer.TUniformDistribution("kw", 9855.0, 12045.0))

nvar = tds.getNAttributes()
print(" ** nX["+str(nvar)+"]")
nS = nbins*nvar*nCond
print(" ** nS["+str(nS)+"]")


# sam = Sampler.TSampling(tds, "lhs", nS)
sam = Sampler.TQMC(tds, "halton", nS)
sam.generateSample()

# Load the function
ROOT.gROOT.LoadMacro("UserFunctions.C")

# Create a TLauncherFunction from a TDataServer and an analytical function
# Rename the outpout attribute "ymod"
tlf = Launcher.TLauncherFunction(tds, "flowrateModel",
                                 "rw:r:tu:tl:hu:hl:l:kw", "ymod")
# Evaluate the function on all the design of experiments
tlf.setDrawProgressBar(False)
tlf.run()

Canvas = ROOT.TCanvas("c1", "Graph for the Macro modeler", 5, 64, 1270, 667)
pad = ROOT.TPad("pad", "pad", 0, 0.03, 1, 1)
pad.Draw()
if args.style:
    ROOT.gROOT.ProcessLine("set_date()")
pad.Divide(2, 2)
pad.cd(1)
tds.computeStatistic("ymod")
tds.draw("ymod")
dstdy = tds.getAttribute("ymod").getStd()
svary = dstdy * dstdy

print(" ** ymod: std["+str(round(dstdy, 4))+"] vary["+str(round(svary, 1))+"]")

## verification ##
dict_json = {"ymod":{"std":dstdy, "vary":svary}}
##################

tds.getAttribute("ymod").setOutput()


ROOT.gStyle.SetOptStat(1)

valSobolCrt = []
sName = []

c = ROOT.TCanvas()
c.Divide(2)
c.cd(1)
for ivar in range(nvar):

    print(" *****************************")
    print(" *** "+str(tds.getAttribute(ivar).GetName()))

    svar = tds.getAttribute(ivar).GetName()

    if ivar == 0:
        pad.cd(2)
    else:
        c.cd(1)

    tds.drawProfile("ymod:"+svar, "", "nclass="+str(nbins))
    hprofs = ROOT.gPad.GetPrimitive("Profile ymod:%s (Bin = %i )" %
                                    (svar, nbins+2))

    ntd = ROOT.TNtupleD("dd", "sjsjs", "i:x:m")
    ntd.SetMarkerColor(ROOT.kBlue)
    ntd.SetMarkerStyle(8)
    nnbins = hprofs.GetNbinsX()
    for i in range(1, nnbins+1):
        ntd.Fill(i-1, hprofs.GetBinCenter(i), hprofs.GetBinContent(i))

    tds.draw("ymod:"+svar)
    ntd.Draw("m:x", "", "same")

    if ivar == 0:
        pad.cd(3)
    else:
        c.cd(2)

    ntd.Draw("m")
    htemp = ROOT.gPad.GetPrimitive("htemp")

    dvarcond = htemp.GetRMS()

    # Tempory ROOT.TTree for histogram
    valSobolCrt.append(dvarcond*dvarcond / svary)
    sName.append(svar)

    print(" *** S1[ %s] Cond. Var.[%4.6g] -- [%1.6g]" %
          (svar, dvarcond*dvarcond, valSobolCrt[-1]))

    ## Verification ##
    dict_json[svar] = {"cond_var":dvarcond*dvarcond, "valSobolCrt":valSobolCrt[-1]}
    ##################

    c.Modified()
    c.Update()
    c.SaveAs("SAFlowRateVersus"+svar+".png")

pad.cd(4)
hDiv = draw_bar_with_tuple(valSobolCrt, sName,
                           "Sensitivity Indexes: ymod  [Brute-Force Method]")

Canvas.SaveAs(args.figure)

if args.json:
    with open(args.json, "w") as file:
        json.dump(dict_json, file, indent=4)
