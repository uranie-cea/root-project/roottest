list(APPEND SUBSTITUTION_TEST "sensitivityFASTFunctionFlowrate" "sensitivityRBDFunctionFlowrate" 
    "sensitivitySobolFunctionFlowrate" "sensitivitySobolFunctionFlowrateRunner" "sensitivitySobolLoadFile" 
    "sensitivityJohnsonRWFunctionFlowrate" "sensitivityJohnsonRWCorrelatedFunctionFlowrate" 
    "sensitivityJohnsonRWJustCorrelationFakeFlowrate")

foreach(TEST_NAME IN LISTS SUBSTITUTION_TEST)
    ROOTTEST_ADD_TEST(${TEST_NAME}_substitution MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/substitution.py
        OPTS "${TEST_NAME}_clean.log" "regex" "File\\[(.*)souRCE(.*)souRCE(.*)\\] Line" "File\[\$\{SOURCEDIR\}\\2souRCE\\3\] Line"
        DEPENDS ${TEST_NAME})
endforeach()

################## COMPARAISON VALEURS NUMERIQUES SORTIE CONSOLE ##################

find_python_module(jsoncomparison OPTIONAL)
if(PY_JSONCOMPARISON_FOUND)
    ROOTTEST_ADD_TEST(sensitivityBrutForceMethodFlowrate_console MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_json_console.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/sensitivity/use_cases/sensitivityBrutForceMethodFlowrate.json"
        "sensitivityBrutForceMethodFlowrate.json"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/brute_force
        DEPENDS sensitivityBrutForceMethodFlowrate)

    ROOTTEST_ADD_TEST(sensitivityFiniteDifferencesFunctionFlowrate_matrix MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_json_console.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/sensitivity/use_cases/sensitivityFiniteDifferencesFunctionFlowrate.json"
        "sensitivityFiniteDifferencesFunctionFlowrate.json"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/finit_differences
        DEPENDS sensitivityFiniteDifferencesFunctionFlowrate)

    ROOTTEST_ADD_TEST(sensitivityJohnsonRWCorrelatedFunctionFlowrate_matrix MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_json_console.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/sensitivity/use_cases/sensitivityJohnsonRWCorrelatedFunctionFlowrate_matrix.json"
        "sensitivityJohnsonRWCorrelatedFunctionFlowrate_matrix.json"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/johnson_rw_corr
        DEPENDS sensitivityJohnsonRWCorrelatedFunctionFlowrate)
endif()

################## COMPARAISON TABLEAUX SORTIE CONSOLE ##################

find_python_module(pandas OPTIONAL)
if(PY_PANDAS_FOUND)
    ROOTTEST_ADD_TEST(sensitivityDataBaseFlowrate_array MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_arrays.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/sensitivity/use_cases/sensitivityDataBaseFlowrate.json"
        "sensitivityDataBaseFlowrate.json"
    PRECMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/convert_tree_json.py
        "sensitivityDataBaseFlowrate.json" "sensitivityDataBaseFlowrate.root" "data_base" 
        "--tostring" "Out" "Inp" "Order" "Method"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/data_base
        DEPENDS sensitivityDataBaseFlowrate)

    ROOTTEST_ADD_TEST(sensitivityFASTFunctionFlowrate_array MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_arrays.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/sensitivity/use_cases/sensitivityFASTFunctionFlowrate.json"
        "sensitivityFASTFunctionFlowrate.json"
    PRECMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/convert_tree_json.py
        "sensitivityFASTFunctionFlowrate.json" "sensitivityFASTFunctionFlowrate.root" "fast" 
        "--tostring" "Out" "Inp" "Order" "Method"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/fast
        DEPENDS sensitivityFASTFunctionFlowrate)

    ROOTTEST_ADD_TEST(sensitivityRBDFunctionFlowrate_array MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_arrays.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/sensitivity/use_cases/sensitivityRBDFunctionFlowrate.json"
        "sensitivityRBDFunctionFlowrate.json" "--rtol" 1e-3
    PRECMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/convert_tree_json.py
        "sensitivityRBDFunctionFlowrate.json" "sensitivityRBDFunctionFlowrate.root" "rbd"
        "--tostring" "Out" "Inp" "Order" "Method"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/rbd
        DEPENDS sensitivityRBDFunctionFlowrate)

    ROOTTEST_ADD_TEST(sensitivityMorrisFunctionFlowrate_array MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_arrays.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/sensitivity/use_cases/sensitivityMorrisFunctionFlowrate.json"
        "sensitivityMorrisFunctionFlowrate.json"
    PRECMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/convert_tree_json.py
        "sensitivityMorrisFunctionFlowrate.json" "sensitivityMorrisFunctionFlowrate.root" "morris"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/morris
        DEPENDS sensitivityMorrisFunctionFlowrate)

    ROOTTEST_ADD_TEST(sensitivityMorrisFunctionFlowrateRunner_array MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_arrays.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/sensitivity/use_cases/sensitivityMorrisFunctionFlowrateRunner.json"
        "sensitivityMorrisFunctionFlowrateRunner.json"
    PRECMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/convert_tree_json.py
        "sensitivityMorrisFunctionFlowrateRunner.json" "sensitivityMorrisFunctionFlowrateRunner.root" 
        "morris_runner"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/morris_runner
        DEPENDS sensitivityMorrisFunctionFlowrateRunner)

    ROOTTEST_ADD_TEST(sensitivityRegressionFunctionFlowrate_array MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_arrays.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/sensitivity/use_cases/sensitivityRegressionFunctionFlowrate.json"
        "sensitivityRegressionFunctionFlowrate.json"
    PRECMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/convert_tree_json.py
        "sensitivityRegressionFunctionFlowrate.json" "sensitivityRegressionFunctionFlowrate.root" 
        "regression" "--tostring" "Out" "Inp" "Method"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/regression
        DEPENDS sensitivityRegressionFunctionFlowrate)

    ROOTTEST_ADD_TEST(sensitivitySobolFunctionFlowrate_array MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_arrays.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/sensitivity/use_cases/sensitivitySobolFunctionFlowrate.json"
        "sensitivitySobolFunctionFlowrate.json"
    PRECMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/convert_tree_json.py
        "sensitivitySobolFunctionFlowrate.json" "sensitivitySobolFunctionFlowrate.root" "sobol"
        "--tostring" "Out" "Inp" "Order" "Method"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/sobol
        DEPENDS sensitivitySobolFunctionFlowrate)

    ROOTTEST_ADD_TEST(sensitivitySobolFunctionFlowrateRunner_array MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_arrays.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/sensitivity/use_cases/sensitivitySobolFunctionFlowrateRunner.json"
        "sensitivitySobolFunctionFlowrateRunner.json"
    PRECMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/convert_tree_json.py
        "sensitivitySobolFunctionFlowrateRunner.json" "sensitivitySobolFunctionFlowrateRunner.root" "sobol_runner"
        "--tostring" "Inp" "Order"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/sobol_runner
        DEPENDS sensitivitySobolFunctionFlowrateRunner)

    ROOTTEST_ADD_TEST(sensitivitySobolReestimation_array MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_arrays.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/sensitivity/use_cases/sensitivitySobolReestimation.json"
        "sensitivitySobolReestimation.json"
    PRECMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/convert_tree_json.py
        "sensitivitySobolReestimation.json" "sensitivitySobolReestimation.root" "sobol_reestimation"
        "--tostring" "Inp" "Order"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/sobol_reestimation
        DEPENDS sensitivitySobolReestimation)

    ROOTTEST_ADD_TEST(sensitivitySobolWithData_array MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_arrays.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/sensitivity/use_cases/sensitivitySobolWithData.json"
        "sensitivitySobolWithData.json"
    PRECMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/convert_tree_json.py
        "sensitivitySobolWithData.json" "sensitivitySobolWithData.root" "sobol_data"
        "--tostring" "Inp" "Order"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/sobol_data
        DEPENDS sensitivitySobolWithData)

    ROOTTEST_ADD_TEST(sensitivitySobolLoadFile_array MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_arrays.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/sensitivity/use_cases/sensitivitySobolLoadFile.json"
        "sensitivitySobolLoadFile.json"
    PRECMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/convert_tree_json.py
        "sensitivitySobolLoadFile.json" "sensitivitySobolLoadFile.root" "sobol_load_file" 
        "--tostring" "Out" "Inp" "Order" "Method"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/sobol_load_file
        DEPENDS sensitivitySobolLoadFile)

    ROOTTEST_ADD_TEST(sensitivityJohnsonRWFunctionFlowrate_array MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_arrays.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/sensitivity/use_cases/sensitivityJohnsonRWFunctionFlowrate.json"
        "sensitivityJohnsonRWFunctionFlowrate.json"
    PRECMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/convert_tree_json.py
        "sensitivityJohnsonRWFunctionFlowrate.json" "sensitivityJohnsonRWFunctionFlowrate.root" 
        "johnson_rw" "--tostring" "Out" "Inp" "Method"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/johnson_rw
        DEPENDS sensitivityJohnsonRWFunctionFlowrate)

    ROOTTEST_ADD_TEST(sensitivityJohnsonRWCorrelatedFunctionFlowrate_array MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_arrays.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/sensitivity/use_cases/sensitivityJohnsonRWCorrelatedFunctionFlowrate.json"
        "sensitivityJohnsonRWCorrelatedFunctionFlowrate.json"
    PRECMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/convert_tree_json.py
        "sensitivityJohnsonRWCorrelatedFunctionFlowrate.json" 
        "sensitivityJohnsonRWCorrelatedFunctionFlowrate.root" "johnson_rw_corr" 
        "--tostring" "Out" "Inp" "Method"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/johnson_rw_corr
        DEPENDS sensitivityJohnsonRWCorrelatedFunctionFlowrate)

    ROOTTEST_ADD_TEST(sensitivityJohnsonRWJustCorrelationFakeFlowrate_array MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_arrays.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/sensitivity/use_cases/sensitivityJohnsonRWJustCorrelationFakeFlowrate.json"
        "sensitivityJohnsonRWJustCorrelationFakeFlowrate.json"
    PRECMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/convert_tree_json.py
        "sensitivityJohnsonRWJustCorrelationFakeFlowrate.json" 
        "sensitivityJohnsonRWJustCorrelationFakeFlowrate.root" "johnson_rw_corr_fake"
        "--tostring" "Out" "Inp" "Method"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/johnson_rw_corr_fake
        DEPENDS sensitivityJohnsonRWJustCorrelationFakeFlowrate)

    ROOTTEST_ADD_TEST(sensitivityHSICFunctionFlowrate_array MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_arrays.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/sensitivity/use_cases/sensitivityHSICFunctionFlowrate.json"
        "sensitivityHSICFunctionFlowrate.json"
    PRECMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/convert_tree_json.py
        "sensitivityHSICFunctionFlowrate.json" "sensitivityHSICFunctionFlowrate.root" "hsic"
        "--tostring" "Out" "Method" "Order"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/hsic
        DEPENDS sensitivityHSICFunctionFlowrate)

    ROOTTEST_ADD_TEST(sensitivitySobolRankFunctionFlowrate_array MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_arrays.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/sensitivity/use_cases/sensitivitySobolRankFunctionFlowrate.json"
        "sensitivitySobolRankFunctionFlowrate.json"
    PRECMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/convert_tree_json.py
        "sensitivitySobolRankFunctionFlowrate.json" "sensitivitySobolRankFunctionFlowrate.root" 
        "sobol_rank" "--tostring" "Out" "Inp" "Method" "Order"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/sobol_rank
        DEPENDS sensitivitySobolRankFunctionFlowrate)
endif()

################## COMPARAISON IMAGES ##################

find_python_module(skimage OPTIONAL)
if(PY_SKIMAGE_FOUND)

    list(APPEND SENSITIV_LIST_TESTS "sensitivityJohnsonRWFunctionFlowrate" 
        "sensitivityJohnsonRWCorrelatedFunctionFlowrate" 
        "sensitivityJohnsonRWJustCorrelationFakeFlowrate")

    list(APPEND SENSITIV_LIST_FIG "appliUranieFlowrateJohnsonRW1000" "appliUranieFlowrateJohnsonRWCorrelated1000" 
        "appliUranieFakeFlowrateJohnsonRWCorrelation")

    list(APPEND SENSITIV_LIST_FOLDER "johnson_rw" "johnson_rw_corr" "johnson_rw_corr_fake")

    set(SIMILARITY 0.99)

    foreach(FOLDER_NAME FIG_BASE TEST_NAME IN ZIP_LISTS SENSITIV_LIST_FOLDER SENSITIV_LIST_FIG SENSITIV_LIST_TESTS)
        foreach(SUFFIX_NAME IN LISTS SUFFIX_LIST)
            set(FIG_NAME ${FIG_BASE}${SUFFIX_NAME})

            ROOTTEST_ADD_TEST(${TEST_NAME}_${SUFFIX_NAME}_figure MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_images.py
                OPTS "${CMAKE_SOURCE_DIR}/uranie/references/sensitivity/use_cases/${FIG_NAME}.png"
                    "${FIG_NAME}.png"
                    "${FIG_NAME}_diff.png"
                    "--ssim" ${SIMILARITY}
                WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/${FOLDER_NAME}
                DEPENDS ${TEST_NAME})
        endforeach()
    endforeach()

    set(SENSITIV_LIST_FIG)
    set(SENSITIV_LIST_FOLDER)

    list(APPEND SENSITIV_LIST_FIG "sensitivityBrutForceMethodFlowrate" "sensitivityDataBaseFlowrate" 
        "sensitivityFASTFunctionFlowrate" "sensitivityRBDFunctionFlowrate" "sensitivityMorrisFunctionFlowrate" 
        "sensitivityMorrisFunctionFlowrateRunner" "sensitivityRegressionFunctionFlowrate" 
        "sensitivitySobolFunctionFlowrate" "sensitivitySobolFunctionFlowrateRunner" "sensitivityRegressionLeveLE"
        "sensitivitySobolLeveLE" "sensitivitySobolReestimation" "sensitivitySobolWithData" "sensitivitySobolLoadFile" 
        "sensitivityHSICFunctionFlowrate" "sensitivitySobolRankFunctionFlowrate")

    list(APPEND SENSITIV_LIST_FOLDER "brute_force" "data_base" "fast" "rbd" "morris" "morris_runner"
        "regression" "sobol" "sobol_runner" "regression_levelE" "sobol_levelE" "sobol_reestimation"
        "sobol_data" "sobol_load_file" "hsic" "sobol_rank")

    foreach(FOLDER_NAME FIG_NAME IN ZIP_LISTS SENSITIV_LIST_FOLDER SENSITIV_LIST_FIG)
        set(SIMILARITY 0.99)
        if(${FIG_NAME} STREQUAL "sensitivityRBDFunctionFlowrate")
            set(SIMILARITY 0.98)
            if(WIN32)
                set(SIMILARITY 0.97)
            endif()
        endif()

        if (NOT LEVEL_E_CMD AND ${FOLDER_NAME} MATCHES "levelE")
            continue()
        endif()

        ROOTTEST_ADD_TEST(${FIG_NAME}_figure MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_images.py
            OPTS "${CMAKE_SOURCE_DIR}/uranie/references/sensitivity/use_cases/${FIG_NAME}.png"
                "${FIG_NAME}.png"
                "${FIG_NAME}_diff.png"
                "--ssim" ${SIMILARITY}
            WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/${FOLDER_NAME}
            DEPENDS ${FIG_NAME})
    endforeach()
endif()
