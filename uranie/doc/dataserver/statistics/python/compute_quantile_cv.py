from URANIE import DataServer, Launcher, Sampler, Modeler
import ROOT
import argparse

parser = argparse.ArgumentParser('Statistical estimation on vector attributes')
parser.add_argument('input', help='input data')
args = parser.parse_args()

tds = DataServer.TDataServer()
tds2 = DataServer.TDataServer("tdsFlowrate","Ex. flowrate")
tdsza = DataServer.TDataServer( "%s_zalpha" %(tds2.GetName()), "Ex. flowrate")

tds.fileDataRead(args.input)

tds2.addAttribute(DataServer.TUniformDistribution("rw", 0.05, 0.15))
tds2.addAttribute(DataServer.TUniformDistribution("r", 100.0, 50000.0))
tds2.addAttribute(DataServer.TUniformDistribution("tu", 63070.0, 115600.0))
tds2.addAttribute(DataServer.TUniformDistribution("tl", 63.1, 116.0))
tds2.addAttribute(DataServer.TUniformDistribution("hu", 990.0, 1110.0))
tds2.addAttribute(DataServer.TUniformDistribution("hl", 700.0, 820.0))
tds2.addAttribute(DataServer.TUniformDistribution("l", 1120.0, 1680.0))
tds2.addAttribute(DataServer.TUniformDistribution("kw", 9855.0, 12045.0))
sY = "yhat"
sinput = "rw:r:tu:tl:hu:hl:l:kw"
nattinput = 8
dAlpha = 0.5
alpha = 0.5
nS = 500

# Build the SR ( Linear regression + ANN)
tlin = Modeler.TLinearRegression(tds, "rw:r:tu:tl:hu:hl:l:kw", sY, "Dummy")
tlin.estimate()
tlin.exportFunction("c++", "_SR_rl_", "SRrl")

tann = Modeler.TANNModeler(tds, "%s,8,%s" % (sinput,sY) )
tann.train(3, 2, "test")
tann.setDrawProgressBar(False)
tann.exportFunction("c++", "_SR_ann_", "SRann")

# build Z
ROOT.gROOT.LoadMacro("_SR_rl_.C")
tlfz = Launcher.TLauncherFunction(tds, "SRrl", sinput, "Z")
tlfz.setDrawProgressBar(False)
tlfz.run()

for i in range(nattinput):
    tdsza.addAttribute( tds2.getAttribute(i))

fsza = Sampler.TSampling(tdsza, "lhs", 6000)
fsza.generateSample()

tlfrlza = Launcher.TLauncherFunction(tdsza, "SRrl", sinput, "Zrl")
tlfrlza.setDrawProgressBar(False)
tlfrlza.run()

from ctypes import c_double
dAlpha = c_double(0.5)
dZrla = c_double(0)
tdsza.computeQuantile("Zrl", dAlpha, dZrla)
print(dZrla)

dY = c_double(0)
rho = c_double(0)
tds.computeQuantileCV("yhat", alpha, "Z", dZrla, dY, rho)
