from URANIE import DataServer, Launcher, Sampler
import ROOT

tds2 = DataServer.TDataServer()
tds2.addAttribute(DataServer.TUniformDistribution("rw", 0.05, 0.15))
tds2.addAttribute(DataServer.TUniformDistribution("r", 100.0, 50000.0))
tds2.addAttribute(DataServer.TUniformDistribution("tu", 63070.0, 115600.0))
tds2.addAttribute(DataServer.TUniformDistribution("tl", 63.1, 116.0))
tds2.addAttribute(DataServer.TUniformDistribution("hu", 990.0, 1110.0))
tds2.addAttribute(DataServer.TUniformDistribution("hl", 700.0, 820.0))
tds2.addAttribute(DataServer.TUniformDistribution("l", 1120.0, 1680.0))
tds2.addAttribute(DataServer.TUniformDistribution("kw", 9855.0, 12045.0))
sY = "yhat"
sinput = "rw:r:tu:tl:hu:hl:l:kw"
nattinput = 8
nS = 500
seuil = 160.0
ROOT.gROOT.LoadMacro("UserFunctions.C")

tis = Sampler.TImportanceSampling(tds2, "rw", DataServer.TNormalDistribution("rw_IS", 0.10, 0.015), nS)

tdsis = tis.getTDS()

sampis = Sampler.TSampling(tdsis,"lhs",nS)
sampis.generateSample()
tlfis = Launcher.TLauncherFunction(tdsis, "flowrateModel", "*", "Y_IS")
tlfis.setDrawProgressBar(False)
tlfis.run()

ISproba = tis.computeThreshold("Y_IS",seuil)

ISmean = tis.computeMean("Y_IS")
ISstd = tis.computeStd("Y_IS")
