from URANIE import DataServer
from ctypes import c_double
import numpy as np
import argparse

parser = argparse.ArgumentParser('Geyser : compute quantile')
parser.add_argument('filename', help='data file name')
args = parser.parse_args()

tdsGeyser = DataServer.TDataServer("geyser", "g")
tdsGeyser.fileDataRead(args.filename)

aproba = c_double(0.5)
aquant = c_double(0)
tdsGeyser.computeQuantile("x2", aproba, aquant) # (1)

Proba = np.array([0.05,0.95], dtype=np.float64)
Quant = np.array([0.0,0.0], dtype=np.float64) # (2)
tdsGeyser.computeQuantile("x2", 2, Proba, Quant)

print("Quant[0]=%f; Quant[1]=%f" % (Quant[0],Quant[1])) # (3)
