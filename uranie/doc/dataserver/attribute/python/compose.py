from URANIE import DataServer, Sampler

tds = DataServer.TDataServer("tdssampler", "Sampler Uranie demo")
comp = DataServer.TComposedDistribution("compo")
comp.addDistribution(DataServer.TNormalDistribution("n1", -1.5, 0.2), 1.2)
comp.addDistribution(DataServer.TNormalDistribution("n2", 0, 0.5), 1.0)
comp.addDistribution(DataServer.TNormalDistribution("n3", 1.5, 0.2), 0.8)
tds.addAttribute(comp)

fsamp = Sampler.TSampling(tds, "lhs", 300)
fsamp.generateSample()  # Create a representative sample

tds.Draw("compo")

tds.getAttribute("compo").setBounds(-1.6,1.8) # truncate the law
