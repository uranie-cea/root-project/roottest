from URANIE import DataServer, Sampler

tds = DataServer.TDataServer("tdssampler", "Sampler Uranie demo")
tds.addAttribute(DataServer.TGumbelMaxDistribution("gm", 0.5, 2.0))

fsamp = Sampler.TSampling(tds, "lhs", 300)
fsamp.generateSample()  # Create a representative sample

tds.Draw("gm")

tds.getAttribute("gm").setBounds(-1.0,12.0)  #truncate the law
