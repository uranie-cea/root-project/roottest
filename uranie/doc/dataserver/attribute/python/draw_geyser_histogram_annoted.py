from URANIE import DataServer
import ROOT
import argparse

parser = argparse.ArgumentParser("Annoted Geyser Histogram")
parser.add_argument("input", help="input data")
parser.add_argument("--figure", help="image filename", default="figure.png")
parser.add_argument("--style", help="macro filename")
args = parser.parse_args()

ROOT.gStyle.SetMarkerSize(0.01)
tdsGeyser = DataServer.TDataServer()
tdsGeyser.fileDataRead(args.input)

canvas = ROOT.TCanvas()
tdsGeyser.addAttribute("sdp", "x2", "#sigma_{#Delta P}", "M^{2}")
tdsGeyser.draw("sdp")

arr = ROOT.TArrow()
arr.SetLineColor(2)
arr.SetLineWidth(2)
arr.SetNDC()
arr.SetArrowSize(0.02)

lat = ROOT.TLatex()
lat.SetTextColor(2)
lat.SetTextSize(0.045)

arr.DrawArrow(69, 12, 69, 16.5)
lat.DrawLatex(66.5, 11, "Name")

arr.DrawArrow(96, 3.5, 96,-0.9)
lat.DrawLatex(93.5, 3.8, "Title")

arr.DrawArrow(99,5, 99, -0.9)
lat.DrawLatex(96.5, 5.3, "Unit")

if args.style:
    ROOT.gROOT.LoadMacro(args.style)

canvas.SaveAs(args.figure)
