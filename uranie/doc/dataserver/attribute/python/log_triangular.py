from URANIE import DataServer, Sampler

tds = DataServer.TDataServer("tdssampler", "Sampler Uranie demo")
tds.addAttribute(DataServer.TLogTriangularDistribution("lt", .001, 10., 2.5))

fsamp = Sampler.TSampling(tds, "lhs", 300)
fsamp.generateSample()  # Create a representative sample

tds.Draw("lt")
tds.Draw("log(lt)")  # Check that ln(lt) follows a triangular law 
