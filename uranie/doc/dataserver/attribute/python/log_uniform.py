from URANIE import DataServer, Sampler

tds = DataServer.TDataServer("tdssampler", "Sampler Uranie demo")
tds.addAttribute(DataServer.TLogUniformDistribution("lu", .001, 10.))

fsamp = Sampler.TSampling(tds, "lhs", 300)
fsamp.generateSample()  # Create a representative sample 

tds.Draw("lu")
tds.Draw("log(lu)")  # Check that ln(x) follows a uniform law
