from URANIE import DataServer

def unbound_variable():
    px = DataServer.TAttribute("x")

def bound_variable_zero_one():
    px = DataServer.TAttribute("x", 0., 1.)

def bound_variable_minTwo_four():
    px = DataServer.TAttribute("x", -2.0, 4.0)
    px.setTitle("#Delta P_{e}^{F_{iso}}")

def string_variable():
    px = DataServer.TAttribute("x", DataServer.TAttribute.kString)

unbound_variable()
bound_variable_zero_one()
bound_variable_minTwo_four()
string_variable()
