from URANIE import DataServer, Sampler

tds = DataServer.TDataServer("tdssampler", "Sampler Uranie demo")
# using M, Ef and xmin
tds.addAttribute(DataServer.TLogNormalDistribution("ln", 1.2, 1.5, -0.5))  
# to use ln(x) properties:
# mu = 0.5
# sigma = 1
# tds.setUnderlyingNormalParameters(mu,sigma)  

fsamp = Sampler.TSampling(tds, "lhs", 300)
fsamp.generateSample()  # Create a representative sample

tds.Draw("ln")
tds.Draw("log(ln)")  # Check that ln(ln) follows a normal law

tds.getAttribute("ln").setBounds(0.6,3.1)  # truncate the law
