from URANIE import DataServer, Sampler

tds = DataServer.TDataServer("tdssampler", "Sampler Uranie demo")
tds.addAttribute(DataServer.TWeibullDistribution("wei", 0.5, 2.0, -0.01) )

fsamp = Sampler.TSampling(tds, "lhs", 300)
fsamp.generateSample()  # Create a representative sample

tds.Draw("wei")

tds.getAttribute("wei").setBounds(0.2,1.8)  # truncate the law
