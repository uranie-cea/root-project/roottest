# Chargement des modules utiles
from array import array
import subprocess
from LawCatalog import AllTheLaws
import os 
import ROOT
import argparse

parser = argparse.ArgumentParser("Draw uniform distribution")
parser.add_argument("--style", help="macro filename", default="")
args = parser.parse_args()

ROOT.gROOT.SetBatch(True)

## Colors for Root to print curves
Cols=[1,2,3,4,6,7, ROOT.kOrange-2, ROOT.kYellow-5]

## To print text on TCanvas
latT=ROOT.TLatex(); latT.SetNDC(); latT.SetTextSize(0.045)
latF=ROOT.TLatex(); latF.SetNDC(); latF.SetTextSize(0.04)
lats=[]
for col in Cols :
    lats.append( ROOT.TLatex() ); 
    lats[-1].SetNDC(); 
    lats[-1].SetTextSize(0.038)
    lats[-1].SetTextColor(col)
    pass

# Initialisation des diverses variables
nbSteps = 1000

dosingle=False
if args.style:
    dosingle=True

# construction de la figure
fig1 = ROOT.TCanvas("canvas", "canvas",10,32,1268,568)
# construction de 4 sous-figures (deux lignes fois deux colonnes)

if not dosingle : 
    Name="ParamAllDistribution.pdf"
    fig1.Print(Name+"[")
    pass

testformathmore = os.popen("root-config --has-mathmore").read()[:-1]

for law_key in AllTheLaws :

    Law=AllTheLaws[law_key]
    Plots=[]

    if "Student" in Law["Legend"] and testformathmore=="no" :
        continue;
    elif "Student" in Law["Legend"] and testformathmore=="yes" :
        print("youpi")
    
    if dosingle :  Name=Law["Legend"]+"Distribution.png"

    # Construction de l'attribut
    attN = Law["Type"]("att_"+Law["Legend"])

    xMin = Law["Windows"][0]; xMax = Law["Windows"][1]

    xStep   = (xMax-xMin)/nbSteps  

    fig1.Clear();
    pad=ROOT.TPad("pad","pad",0, 0.30, 1, 1); pad.Draw();
    pad.Draw();
    pad.Clear()
    pad.Divide(3,1)

    mPDF=ROOT.TMultiGraph()
    mCDF=ROOT.TMultiGraph()
    mInvCDF=ROOT.TMultiGraph()

    variable = len( Law["Parameters"] )
    variation = len( Law["ParamVar"] )
  
    for param in range(variation) :

        if variable==1   : attN.setParameters(Law["ParamVar"][param][0]);
        elif variable==2 : attN.setParameters(Law["ParamVar"][param][0],Law["ParamVar"][param][1]);
        elif variable==3 : attN.setParameters(Law["ParamVar"][param][0],Law["ParamVar"][param][1],Law["ParamVar"][param][2]);
        elif variable==4 : attN.setParameters(Law["ParamVar"][param][0],Law["ParamVar"][param][1],Law["ParamVar"][param][2],Law["ParamVar"][param][3]);

        if "Gamma" in Law["Legend"]: xMin = Law["Windows"][0] + Law["ParamVar"][param][2];
    
        # construction de liste "typees", necessaire pour ROOT. 
        # Ces objets "array" ne peuvent contenir qu'un seul type de valeurs. 
        # Ici, l'option "d" indique que ces tableaux contiennent des reels.
        x      = array("d",[])
        p      = array("d",[])
        pdf    = array("d",[])
        cdf    = array("d",[])
        invcdf = array("d",[])

    
        # Calcul et stockage des valeurs interessantes
        for i in range(nbSteps):
            x.append(xMin + i * xStep)     # valeurs de x
            pdf.append(attN.getPDF(x[-1])) # valeurs de la PDF
            cdf.append(attN.getCDF(x[-1])) # valeurs de la CDF
            if  i > 0:
                p.append(float(i)/nbSteps)           # valeurs de probabilites
                invcdf.append(attN.getInvCDF(p[-1])) # valeurs de la CDF inverse
                #print str(x[-1])+"  "+str(pdf[-1])+"  "+str(cdf[-1])+"  "+str(invcdf[-1])
                pass
            pass
    
        pad.cd(1); ROOT.gPad.SetGrid();
        gPDF = ROOT.TGraph(nbSteps, x, pdf); 
        gPDF.SetLineColor(Cols[param])
        mPDF.Add(gPDF)
        if param==(variation-1) : 
            mPDF.Draw("AL") # l'option A construit les axes, l'option L trace une limne entre les points
            mPDF.SetTitle("PDF")  
            mPDF.GetYaxis().SetTitle("f(x)")
            mPDF.SetMinimum(0)
            mPDF.GetXaxis().SetTitle("x")
            pass

        pad.cd(2); ROOT.gPad.SetGrid();
        gCDF = ROOT.TGraph(nbSteps, x, cdf)
        gCDF.SetLineColor(Cols[param])
        mCDF.Add(gCDF)
        if param==(variation-1) : 
            mCDF.Draw("AL")
            mCDF.SetTitle("CDF")  
            mCDF.GetYaxis().SetTitle("Probability")
            mCDF.GetXaxis().SetTitle("x")  
            pass

        pad.cd(3); ROOT.gPad.SetGrid();
        gInvCDF = ROOT.TGraph(nbSteps-1, p, invcdf)
        gInvCDF.SetLineColor(Cols[param])
        mInvCDF.Add(gInvCDF)
        if param==(variation-1) : 
            mInvCDF.Draw("AL") # l'option A construit les axes, l'option L trace une limne entre les points
            mInvCDF.SetTitle("Inverse CDF")
            mInvCDF.GetYaxis().SetTitle("x")
            mInvCDF.GetXaxis().SetTitle("Probability")  
            pass

        Plots.append( (gPDF, gCDF, gInvCDF) )
    
        fig1.cd(0)
        latT.DrawLatex(0.45,0.28,Law["Legend"])
        latF.DrawLatex(0.13,0.11,Law["Formula"])
        line=""
        for ipar in range(len(Law["ParamNames"])) :
            line+=Law["ParamNames"][ipar]+"="+str(Law["ParamVar"][param][ipar])+";  "
            pass    
        lats[param].DrawLatex(0.6,0.24-0.039*param,line)
        pass
    if dosingle :
        ROOT.gROOT.LoadMacro(args.style)
        fig1.SaveAs(Name)
        pass
    else:  fig1.Print(Name)

pass

if not dosingle : fig1.Print(Name+"]")
