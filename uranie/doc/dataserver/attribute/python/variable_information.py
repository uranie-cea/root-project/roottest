from URANIE import DataServer
import ROOT

def name():
    px = DataServer.TAttribute("x")

def name_title():
    psdp = DataServer.TAttribute("sdp", "#sigma_{#Delta P}")
    psdp.setUnity("M^{2}")

def name_boundary():
    x = DataServer.TAttribute("x", -2.0, 4.0)

def name_etype():
    xvec = DataServer.TAttribute("x", DataServer.TAttribute.kVector)

def kreal():
    real = DataServer.TAttribute("real")
    real_value=1.23456789
    real.setDefaultValue(real_value)  # Default with double value
    real.setDefault("1.23456789")  # Default with generic method

def kvector():
    import numpy as np

    vector = DataServer.TAttribute("vector", DataServer.TAttribute.kVector)
    v_value = ROOT.std.vector("double")([1.2, 2.3, 3.4])
    vector.setDefaultVector(v_value)  # Default with double value
    vector.setDefault("1.2,2.3,3.4")  # Default with generic method

def kstring():
    string = DataServer.TAttribute("string", DataServer.TAttribute.kString)
    str_value = "chocolat"
    string.setDefaultString(str_value)  # Default with double value
    string.setDefault(str_value)  # Default with generic method

name()
name_title()
name_boundary()
name_etype()
kreal()
kvector()
kstring()
