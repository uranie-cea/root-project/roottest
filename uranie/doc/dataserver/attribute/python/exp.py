from URANIE import DataServer, Sampler

tds = DataServer.TDataServer("tdssampler", "Sampler Uranie demo")
tds.addAttribute(DataServer.TExponentialDistribution("exp", 0.5))

fsamp = Sampler.TSampling(tds, "lhs", 300)
fsamp.generateSample()  # Create a representative sample

tds.Draw("exp")

tds.getAttribute("exp").setBounds(0.4,6.0)  # truncate the law
