from URANIE import DataServer, Sampler

tds = DataServer.TDataServer("tdssampler", "Sampler Uranie demo")
tds.addAttribute(DataServer.TCauchyDistribution("cau", 0.3, 1.0))

fsamp = Sampler.TSampling(tds, "lhs", 300)
fsamp.generateSample()  # Create a representative sample

tds.Draw("cau")

tds.getAttribute("cau").setBounds(-1.0,2.0)  # truncate the law
