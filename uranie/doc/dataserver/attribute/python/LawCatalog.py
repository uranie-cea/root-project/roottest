import ROOT
import os

DataServer = ROOT.URANIE.DataServer

AllTheLaws = dict(

    Uniform=dict(
        Type=DataServer.TUniformDistribution, Legend="TUniform",
        Formula="f(x) = #frac{1}{(x_{max}-x_{min})} for x#in [x_{min},x_{max}]",
        Windows=[-1., 2.], Bounds=[(0.2, 0.9)], Parameters=[0., 1.],
        ParamNames=["x_{min}", "x_{max}"], ParamVar=[(0., 1.)]
    ),

    LogUniform=dict(
        Type=DataServer.TLogUniformDistribution, Legend="TLogUniform",
        Formula="f(x) = #frac{1}{(x #times ln(x_{max}/x_{min}))} for x#in [x_{min},x_{max}]",
        Windows=[-1., 11], Bounds=[(0.3, 6.)], Parameters=[0.1, 8.],
        ParamNames=["x_{min}", "x_{max}"], ParamVar=[(0.1, 5.), (0.1, 8.)]
    ),

    Normal=dict(
        Type=DataServer.TNormalDistribution, Legend="TNormal",
        Formula="f(x) =  e^{ #frac{-( x - #mu)^{2}}{2#sigma^{2} }} #times #frac{1}{ #sqrt{2#pi#sigma^{2}}}",
        Windows=[-4., 4.], Bounds=[(-1.4, 2.), (-1.1, 1.8)],
        Parameters=[0.5, 1.], ParamNames=["#mu", "#sigma"],
        ParamVar=[(0., 1.), (0.5, 1.), (0, 1.5)]
    ),

    LogNormal=dict(
        Type=DataServer.TLogNormalDistribution, Legend="TLogNormal",
        Formula="f(x) = #frac{1}{((x-x_{min})#sigma#sqrt{2#pi})} #times e^{#frac{-(ln(x-x_{min})-#mu)^{2}}{2#sigma^{2}}} for x > x_{min}",
        Windows=[-0.5, 5.], Bounds=[(0.6, 3.1), (0.3, 2.1)],
        Parameters=[1.5, 1.8, -0.5], ParamNames=["#mu", "#sigma", "x_{min}"],
        ParamVar=[(1.5, 1.5, -0.5), (1.5, 1.8, -0.5), (1.2, 1.5, -0.5),
                  (1.5, 1.5, -0.2)]
    ),

    Trapezium=dict(
        Type=DataServer.TTrapeziumDistribution, Legend="TTrapezium",
        Formula="#splitline{f(x)=#frac{2}{(x_{up}-x_{low}) + (x_{max}-x_{min})} #times Y, Y=1 x#in [x_{low},x_{up}] }{Y=#frac{(x - x_{min})}{(x_{low} - x_{min})} (x#in [x_{min},x_{low}]) and #frac{(x_{max} - x)}{(x_{max} - x_{up}} (x#in [x_{up},x_{max}])}",
        Windows=[-1., 2.], Bounds=[(0., 0.7)],
        Parameters=[-0.3, 1., 0.25, 0.75],
        ParamNames=["x_{min}", "x_{max}", "x_{low}", "x_{up}"],
        ParamVar=[(0., 1., 0.25, 0.75), (-0.3, 1., 0.25, 0.75),
                  (0., 1.5, 0.25, 0.75), (0., 1., 0.15, 0.75),
                  (0., 1., 0.25, 0.95)]
    ),

    Triangular=dict(
        Type=DataServer.TTriangularDistribution, Legend="TTriangular",
        Formula="f(x) = #splitline{ #frac{2 #times (x-x_{min})}{ (x_{max}-x_{min}) #times (x_{mode}-x_{min}) } for x#in [x_{min},x_{mode}]}{#frac{2 #times (x_{max}-x)}{ (x_{max}-x_{min}) #times (x_{max}-x_{mode}) } for x#in  [x_{mode},x_{max}] }",
        Windows=[-0.5, 1.5], Bounds=[(-0.2, 0.7)], Parameters=[-0.3, 1., 0.],
        ParamNames=["x_{min}", "x_{max}", "x_{mod}"],
        ParamVar=[(0., 1., 0.), (-0.3, 1., 0.), (0., 1.5, 0.),
                  (0., 1., 0.5)]
    ),

    LogTriangular=dict(
        Type=DataServer.TLogTriangularDistribution, Legend="TLogTriangular",
        Formula="f(x) = #splitline{ #frac{2 #times ln(x/x_{min})}{ x #times ln(x_{max}/x_{min}) #times ln(x_{mode}/x_{min}) } for x#in [x_{min},x_{mode}]}{#frac{2 #times ln(x_{max}/x)}{ x #times ln(x_{max}/x_{min}) #times ln(x_{max}/x_{mode}) } for x#in  [x_{mode},x_{max}] }",
        Windows=[0., 2.], Bounds=[(0.5, 1.1)], Parameters=[0.3, 1.2, 0.8],
        ParamNames=["x_{low}", "x_{ip}", "x_{mod}"],
        ParamVar=[(0.5, 1.2, 0.8), (0.3, 1.2, 0.8), (0.5, 1.5, 0.8),
                  (0.5, 1.2, 1.)]
    ),

    Exponential=dict(
        Type=DataServer.TExponentialDistribution, Legend="TExponential",
        Formula="f(x) = #lambda #times e^{ - #lambda #times (x-x_{min})}, for x #geq x_{min}",
        Windows=[-2., 14.], Bounds=[(0.4, 6.), (2., 8.)], Parameters=[1.],
        ParamNames=["#lambda"], ParamVar=[[0.5], [1.]]
    ),

    Beta=dict(
        Type=DataServer.TBetaDistribution, Legend="TBeta",
        Formula="#splitline{f(x) = #frac{Y^{#alpha - 1} #times ( 1 - Y )^{#beta - 1}}{B(#alpha,#beta)} for  x#in [x_{min}, x_{max}]}{where Y = #frac{(x-x_{min})}{(x_{max}-x_{min})} and B(#alpha,#beta) is beta function}",
        Windows=[0., 2.], Bounds=[(0.5, 1.2)], Parameters=[6., 6., 0., 2.],
        ParamNames=["#alpha", "#beta", "x_{min}", "x_{max}"],
        ParamVar=[(2., 1., 0., 2.), (1., 1., 0., 2.), (2., 2., 0., 2.),
                  (6., 6., 0., 2.), (0.5, 0.5, -0.1, 2.1), (2., 0.5, 0., 2.2)]
    ),

    Cauchy=dict(
        Type=DataServer.TCauchyDistribution,
        Formula="f(x) = #frac{#gamma}{#pi #times (#gamma^{2}+(x-x_{0})^{2})}",
        Legend="TCauchy", Windows=[-2., 6.], Bounds=[(-1., 2.), (-0.2, 4.)],
        Parameters=[0.6, 1.], ParamNames=["#gamma", "x_{0}"],
        ParamVar=[(0.3, 1.), (0.6, 1.), (0.3, 0.6)]
    ),

    Weibull=dict(
        Type=DataServer.TWeibullDistribution,
        Formula="f(x) = #frac{k}{#lambda} #times (#frac{x-x_{min}}{#lambda})^{k-1} #times e^{-(#frac{x-x_{min}}{#lambda})^{k}} for x > x_{min}",
        Legend="TWeibull", Windows=[-0.5, 5.], Bounds=[(0.2, 1.8), (0.3, 2.)],
        Parameters=[1., 2., -0.1], ParamNames=["#lambda", "k", "x_{min}"],
        ParamVar=[(0.5, 2., -0.1), (1., 2., -0.1), (1.5, 3., -0.1),
                  (2.5, 3., -0.1)]
    ),

    GumbelMax=dict(
        Type=DataServer.TGumbelMaxDistribution, Legend="TGumbelMax",
        Formula="f(x) =  z #times #frac{e^{-z}}{#beta}, where z = e^{#frac{-(x - #mu)}{#beta}}",
        Windows=[-5., 20.], Bounds=[(-1., 12.), (-2., 10.)],
        Parameters=[2.5, 3.], ParamNames=["#mu", "#beta"],
        ParamVar=[(0.5, 2.), (1., 2.), (1.5, 3.), (2.5, 3.)]
    ),

    GenPareto=dict(
        Type=DataServer.TGenParetoDistribution, Legend="TGenPareto",
        Formula="f(x) = #frac{1}{#sigma} #times (1 + #xi(#frac{x-#mu}{#sigma}))^{-(1/#xi +1)}",
        Windows=[0., 10.], Bounds=[(1.4, 4.), (2.4, 8.)],
        Parameters=[1., 1., 0.15], ParamNames=["#mu", "#sigma", "#xi"],
        ParamVar=[(1., 1., 0.15), (0.5, 1., 0.15), (1.5, 1., 0.15), (1., 0.7, 0.15), (1., 1.2, 0.15), (1., 1., 0.3)]
    ),

    UniformByParts=dict(
        Type=DataServer.TUniformByPartsDistribution, Legend="TUniformByParts",
        Formula="f(x) = #splitline{#frac{0.5}{(x_{median}-x_{min})} for x#in [x_{min},x_{median}]}{#frac{0.5}{(x_{max}-x_{median})} for x#in [x_{median},x_{max}]}",
        Windows=[0., 2.], Bounds=[], Parameters=[0., 1.5, 0.4], ParamNames=["x_{min}", "x_{max}", "x_{median}"],
        ParamVar=[(0., 1., 0.5), (0.2, 1., 0.6), (0., 1.5, 0.4), (0., 1., 0.2)]
    ),

    Gamma=dict(
        Type=DataServer.TGammaDistribution, Legend="TGamma",
        Formula="f(x) = #frac{(x-#xi)^{#alpha-1} e^{-(x-#xi)/#beta}}{#Gamma(#alpha)#beta^{#alpha}}",
        Windows=[0.00001, 20.], Bounds=[(1.5, 6.), (0.8, 7.)], Parameters=[3., 1., 0.], ParamNames=["#alpha", "#beta", "#xi"],
        ParamVar=[(1., 2., 0.), (2., 1., 0.), (3., 1., 0.), (3., 2., 0.), (3., 3., 0.), (3., 2., 1.)]
    ),

    InvGamma=dict(
        Type=DataServer.TInvGammaDistribution, Legend="TInvGamma",
        Formula="f(x) = #frac{#beta^{#alpha}(x-#xi)^{-#alpha-1} e^{-#beta/(x-#xi)}}{#Gamma(#alpha)}",
        Windows=[0.00001, 3.], Bounds=[(0.1, 1.6), (0.3, 2.6)], Parameters=[2.5, 0.5, 0.], ParamNames=["#alpha", "#beta", "#xi"],
        ParamVar=[(2., 0.5, 0.), (3., 0.5, 0.), (3., 0.4, 0.), (3., 0.5, 0.5)]
    ),

    GeneralizedNormal=dict(
        Type=DataServer.TGeneralizedNormalDistribution, Legend="TGeneralizedNormal",
        Formula="f(x) = #frac{#beta}{2#alpha#Gamma(1/#beta)} e^{-(#frac{|x-#mu|}{#alpha})^{#beta}}",
        Windows=[-3., 3.], Bounds=[(-0.8, 1.6), (0., 1.)], Parameters=[0., 1., 3.], ParamNames=["#mu", "#alpha", "#beta"],
        ParamVar=[(0., 1., 1.), (0., 1., 4.), (0., 2., 2.), (0., 0.5, 2.)]
    )
)

if (os.popen("root-config --has-mathmore").read()[:-1] == "yes"):
    AllTheLaws["Student"] = dict(
        Type=DataServer.TStudentDistribution, Legend="TStudent",
        Formula="f(x) = #frac{1}{#sqrt{k#pi}} #frac{#Gamma#left(#frac{k+1}{2}#right)}{#Gamma#left(#frac{k}{2}#right)}#left(1+#frac{t^{2}}{k}#right)^{-#frac{k+1}{2}}",
        Windows=[-5., 5.], Bounds=[(-3., 8.), (-1., 3.)], Parameters=[3.],
        ParamNames=["k"],
        ParamVar=[[1.], [3.], [8.], [20.]]
    )
