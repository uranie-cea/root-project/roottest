from URANIE import DataServer, Sampler

tds = DataServer.TDataServer("tdssampler", "Sampler Uranie demo")
tds.addAttribute(DataServer.TNormalDistribution("n", 0.0, 1.0))

fsamp = Sampler.TSampling(tds, "lhs", 300)
fsamp.generateSample()  # Create a representative sample

tds.Draw("n")

tds.getAttribute("n").setBounds(-1.4,2.0)  # truncate the law
