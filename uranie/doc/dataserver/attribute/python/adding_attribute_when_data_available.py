from URANIE import DataServer

def add_new_attribute():
    tds = DataServer.TDataServer("foo","pouet")
    tds.fileDataRead("geyser.dat")
    # Adding a new attribute
    x3 = DataServer.TAttribute("x3","0.5*x2+sin(x1)")

def add_new_attribute_by_address():
    import numpy as np

    tds = DataServer.TDataServer("foo","tru")
    tds.fileDataRead("myData.dat")
    # Defining a vector with 11 elements
    x2 = np.array([-10,-8,-6,-4,-2,0,2,4,6,8,10], dtype=np.float64)
    # Call the method using the address of first element and the size of it
    tds.addAttributeUsingData("x2", x2, len(x2))

add_new_attribute()
add_new_attribute_by_address()
