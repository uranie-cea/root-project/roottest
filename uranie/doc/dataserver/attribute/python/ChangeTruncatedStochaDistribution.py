# Chargement des modules utiles
from array import array
import subprocess
from LawCatalog import AllTheLaws
import os 
import ROOT
import argparse
import inspect

parser = argparse.ArgumentParser("Draw uniform distribution")
parser.add_argument("--style", help="macro filename", default="")
args = parser.parse_args()

ROOT.gROOT.SetBatch(True)

# Initialisation des diverses variables
nbSteps = 1000

## Colors for Root to print curves
Cols=[1,2,3,4,6,7, ROOT.kOrange-2, ROOT.kYellow-5]

## To print text on TCanvas
latT=ROOT.TLatex(); latT.SetNDC(); latT.SetTextSize(0.055)
latF=ROOT.TLatex(); latF.SetNDC(); latF.SetTextSize(0.05)
lats=[]
for col in Cols :
    lats.append( ROOT.TLatex() ); 
    lats[-1].SetNDC(); 
    lats[-1].SetTextSize(0.05)
    lats[-1].SetTextColor(col)
    pass

# construction de la figure
fig1 = ROOT.TCanvas("canvas", "canvas",1500,500)

testformathmore = os.popen("root-config --has-mathmore").read()[:-1]

# construction de 4 sous-figures (deux lignes fois deux colonnes)
dosingle=False
if args.style:
    dosingle=True

if not dosingle : 
    Name="TruncatedParamAllDistribution.pdf"
    fig1.Print(Name+"[")
    pass

for law_key in AllTheLaws :
  
    Law=AllTheLaws[law_key]
    Plots=[]
  
    if "Student" in Law["Legend"] and testformathmore=="no" :
        continue;
    elif "Student" in Law["Legend"] and testformathmore=="yes" :
        print("youpi")

    isInf=False
    inherit=inspect.getmro(Law["Type"])
    for f in inherit :
        if "TInfinite" in str(f) : isInf=True
        pass

    if isInf==False : continue

    if dosingle : Name=Law["Legend"]+"TruncatedDistribution.png"
  
    attN = Law["Type"]("att_"+Law["Legend"])
  
    xMin = Law["Windows"][0]; xMax = Law["Windows"][1]
  
    xStep = (xMax-xMin)/nbSteps
  
    varia = len( Law["Parameters"] )
    if varia==1   : attN.setParameters(Law["Parameters"][0]);
    elif varia==2 : attN.setParameters(Law["Parameters"][0],Law["Parameters"][1]);
    elif varia==3 : attN.setParameters(Law["Parameters"][0],Law["Parameters"][1],Law["Parameters"][2]);
    elif varia==4 : attN.setParameters(Law["Parameters"][0],Law["Parameters"][1],Law["Parameters"][2],Law["Parameters"][3]);
  
    fig1.Clear(); #pad=ROOT.TPad("pad","pad",0, 0.30, 1, 1); pad.Draw(); pad.Draw(); pad.Clear(); pad.Divide(3,1);
    pad=ROOT.TPad("pad","pad",0, 0.30, 1, 1); pad.Draw(); pad.Clear(); pad.Divide(3,1);
    mPDF=ROOT.TMultiGraph(); mCDF=ROOT.TMultiGraph(); mInvCDF=ROOT.TMultiGraph();

    config = 1
    if len( Law["Bounds"] ) != 0 : config = len( Law["Bounds"] )+1

    for icol in range(config) :

        if icol!=0: attN.setBounds(Law["Bounds"][icol-1][0],Law["Bounds"][icol-1][1])
    
        x      = array("d",[])
        p      = array("d",[])
        pdf    = array("d",[])
        cdf    = array("d",[])
        invcdf = array("d",[])
    
        # Calcul et stockage des valeurs interessantes
        for i in range(nbSteps):
            x.append(xMin + i * xStep) # valeurs de x
            pdf.append(attN.getPDF(x[-1])) # valeurs de la PDF
            cdf.append(attN.getCDF(x[-1])) # valeurs de la CDF
            if cdf[-1] > 1 :
                print(Law["Legend"]+"  cdf value is "+str(cdf[-1])+" : ca pue !")
            if i > 0:
                p.append(float(i)/nbSteps) # valeurs de probabilites
                invcdf.append(attN.getInvCDF(p[-1])) # valeurs de la CDF inverse
                pass
            pass
    
        pad.cd(1); ROOT.gPad.SetGrid();
        gPDF = ROOT.TGraph(nbSteps, x, pdf); gPDF.SetLineColor(Cols[icol]); gPDF.SetLineWidth(2);
        mPDF.Add(gPDF)
        if icol == (config-1) :
            mPDF.Draw("AL"); mPDF.SetTitle(Law["Legend"]+" PDF"); mPDF.GetYaxis().SetTitle("f(x)"); mPDF.SetMinimum(0); mPDF.GetXaxis().SetTitle("x")
            mPDF.GetYaxis().SetTitleSize(0.065); mPDF.GetXaxis().SetTitleSize(0.065); mPDF.GetYaxis().SetTitleOffset(0.75); mPDF.GetXaxis().SetTitleOffset(0.75)
            mPDF.GetYaxis().SetLabelSize(0.055); mPDF.GetXaxis().SetLabelSize(0.055); 
            pass
    
        pad.cd(2); ROOT.gPad.SetGrid();
        gCDF = ROOT.TGraph(nbSteps, x, cdf); gCDF.SetLineColor(Cols[icol]); gCDF.SetLineWidth(2);
        mCDF.Add(gCDF)
        if icol == (config-1) :
            mCDF.Draw("AL"); mCDF.SetTitle(Law["Legend"]+" CDF"); mCDF.GetYaxis().SetTitle("Probability"); mCDF.GetXaxis().SetTitle("x");
            mCDF.GetYaxis().SetTitleSize(0.065); mCDF.GetXaxis().SetTitleSize(0.065); mCDF.GetYaxis().SetTitleOffset(0.75); mCDF.GetXaxis().SetTitleOffset(0.75)
            mCDF.GetYaxis().SetLabelSize(0.055); mCDF.GetXaxis().SetLabelSize(0.055); 
            pass
    
        pad.cd(3); ROOT.gPad.SetGrid();
        gInvCDF = ROOT.TGraph(nbSteps-1, p, invcdf); gInvCDF.SetLineColor(Cols[icol]); gInvCDF.SetLineWidth(2);
        mInvCDF.Add(gInvCDF);
        if icol == (config-1) :
            mInvCDF.Draw("AL"); mInvCDF.SetTitle(Law["Legend"]+" Inverse CDF"); mInvCDF.GetYaxis().SetTitle("x"); mInvCDF.GetXaxis().SetTitle("Probability");
            mInvCDF.GetYaxis().SetTitleSize(0.065); mInvCDF.GetXaxis().SetTitleSize(0.065); mInvCDF.GetYaxis().SetTitleOffset(0.75); mInvCDF.GetXaxis().SetTitleOffset(0.75)
            mInvCDF.GetYaxis().SetLabelSize(0.055); mInvCDF.GetXaxis().SetLabelSize(0.055); 
            pass
    
        Plots.append( (gPDF, gCDF, gInvCDF) )

        fig1.cd(0);
        if icol==0 :
            latT.DrawLatex(0.45,0.28,Law["Legend"])
            latF.DrawLatex(0.13,0.14,Law["Formula"])
            line=""
            for ipar in range(len(Law["ParamNames"])) :
                line+=Law["ParamNames"][ipar]+"="+str(Law["Parameters"][ipar])+";  "
                pass
            line+=" no boundaries;"
            lats[icol].DrawLatex(0.6,0.2-0.065*icol,line)
            pass
        else :
            line="Bounds = ["+str(Law["Bounds"][icol-1][0])+" , "+str(Law["Bounds"][icol-1][1])+"];"
            lats[icol].DrawLatex(0.6,0.2-0.065*icol,line)
            pass
        pass
  
    fig1.Print(Name)
    if dosingle :
        ROOT.gROOT.LoadMacro(args.style)
        fig1.SaveAs(Name)
        pass    
    pass

if not dosingle : fig1.Print(Name+"]")
