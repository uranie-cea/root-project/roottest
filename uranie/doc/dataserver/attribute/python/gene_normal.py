from URANIE import DataServer, Sampler

tds = DataServer.TDataServer("tdssampler", "Sampler Uranie demo")
tds.addAttribute(DataServer.TGeneralizedNormalDistribution("gennor", 0.0, 1.0, 3.0) )

fsamp = Sampler.TSampling(tds, "lhs", 300)
fsamp.generateSample()  # Create a representative sample

tds.Draw("gennor")

tds.getAttribute("gennor").setBounds(-0.8,1.6) # truncate the law
