from URANIE import DataServer

# Uniform law             
pxu = DataServer.TUniformDistribution("x1", -1.0 , 1.0) # (1)
# Gaussian Law                                                                                      
pxn = DataServer.TNormalDistribution("x2", -1.0 , 1.0) # (2)
