from URANIE import DataServer, Sampler

tds = DataServer.TDataServer("tdssampler", "Sampler Uranie demo")
tds.addAttribute(DataServer.TGammaDistribution("gam", 1.0, 2.0, 0.0))

fsamp = Sampler.TSampling(tds, "lhs", 300)
fsamp.generateSample()  # Create a representative sample

tds.Draw("gam")

tds.getAttribute("gam").setBounds(0.1,1.6)  # truncate the law
