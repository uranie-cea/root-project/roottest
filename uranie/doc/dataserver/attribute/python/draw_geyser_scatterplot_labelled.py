from URANIE import DataServer
import ROOT
import argparse

parser = argparse.ArgumentParser("Draw Geyser Scatterplot Labelled")
parser.add_argument("input", help="input data")
parser.add_argument("--figure", help="image filename", default="figure.png")
parser.add_argument("--style", help="macro filename")
args = parser.parse_args()

ROOT.gStyle.SetMarkerSize(0.01)
tdsGeyser = DataServer.TDataServer()
tdsGeyser.fileDataRead(args.input)

canvas = ROOT.TCanvas()

px1 = tdsGeyser.getAttribute("x1")
px1.setTitle("#Delta P^{#sigma}")  # Change the title
px1.setUnity("#frac{mm^{2}}{s}")  # Change the unit

px2 = tdsGeyser.getAttribute("x2")
px2.setTitle("#delta P_{#sigma}[bar]")  # Change the title

tdsGeyser.draw("x2:x1") # Draw the plot

if args.style:
    ROOT.gROOT.LoadMacro(args.style)

canvas.SaveAs(args.figure)
