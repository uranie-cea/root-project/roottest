from URANIE import DataServer, Sampler

tds = DataServer.TDataServer("tdssampler", "Sampler Uranie demo")
tds.addAttribute(DataServer.TInvGammaDistribution("ing", 2.0, 0.5, 0.0))

fsamp = Sampler.TSampling(tds, "lhs", 300)
fsamp.generateSample()  # Create a representative sample

tds.Draw("ing")

tds.getAttribute("ing").setBounds(-3.0,8.0)  # truncate the law
