from URANIE import DataServer, Sampler

tds = DataServer.TDataServer("tdssampler", "Sampler Uranie demo")
tds.addAttribute(DataServer.TGenParetoDistribution("gpa", 1.0, 1.0, 0.3))

fsamp = Sampler.TSampling(tds, "lhs", 300)
fsamp.generateSample()  # Create a representative sample

tds.Draw("gpa")

tds.getAttribute("gpa").setBounds(1.4,4.0)  # truncate the law
