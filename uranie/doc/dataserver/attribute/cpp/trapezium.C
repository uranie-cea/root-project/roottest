using namespace URANIE::DataServer;
using namespace URANIE::Sampler;

void trapezium() {
    TDataServer *tds = new TDataServer("tdssampler", "Sampler Uranie demo");
    tds->addAttribute( new TTrapeziumDistribution("tr", 0.0, 1.0, 0.25, 0.75) );
    
    TSampling *fsamp = new TSampling(tds, "lhs", 300);
    fsamp->generateSample(); // Create a representative sample
    
    tds->Draw("tr");
}
