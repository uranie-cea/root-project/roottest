"""
Example of attribute handling
"""
from URANIE import DataServer
import ROOT

tdsop = DataServer.TDataServer("foo", "poet")
tdsop.fileDataRead("tdstest.dat")

tdsop.addAttribute("x*y", "x*y")  # Multiply two vectors
tdsop.addAttribute("xovy", "x/y")  # Divide two vectors
tdsop.addAttribute("x-y", "x-y")  # Subtract two vectors
tdsop.addAttribute("x+y", "x+y")  # Add two vectors

tdsop.addAttribute("x*a", "x*a")  # Multiply a vector and a double
tdsop.addAttribute("xova", "x/a")  # Divide a vector and a double
tdsop.addAttribute("x-a", "x-a")  # Subtract a vector and a double
tdsop.addAttribute("x+a", "x+a")  # Add a vector and a double

tdsop.scan("x:y:a:x*y:xovy:x+y:x-y:x*a:xova:x+a:x-a", "",
           "colsize=3 col=1:1:1::4::::4::")

tdsop.saveTuple("update")
