from URANIE import DataServer
import ROOT
import argparse

parser = argparse.ArgumentParser("Annoted Geyser Histogram")
parser.add_argument("--figure", help="image filename", default="figure.png")
parser.add_argument("--style", help="macro filename")
args = parser.parse_args()

ROOT.gStyle.SetMarkerSize(0.01)

tdsGeyser = DataServer.TDataServer("tdsgeyser", "Geyser DataSet")
tdsGeyser.fileDataRead("geyser.dat")

canvas = ROOT.TCanvas()

tdsGeyser.addAttribute("cd1", "sqrt(x2) * x1") # (1)
tdsGeyser.addAttribute("cd2", "sqrt(x2* x1)", "#Delta p_{#sigma}", "sec^{-1}") # (2)

tdsGeyser.draw("cd2:cd1") # (3)

if args.style:
    ROOT.gROOT.LoadMacro(args.style)

canvas.SaveAs(args.figure)
