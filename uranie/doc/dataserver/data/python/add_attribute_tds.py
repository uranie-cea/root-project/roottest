from URANIE import DataServer

tds = DataServer.TDataServer("tds", "new TDataServer")
tds.addAttribute(DataServer.TAttribute("x1"))          # (1)
tds.addAttribute(DataServer.TAttribute("x2", 2.5, 5.)) # (2)
tds.addAttribute( "x3" )                               # (3)
tds.addAttribute( "x4", 2.5, 5.)                       # (4)
