"""
Example of data merging (merging two files)
"""
from URANIE import DataServer
import ROOT

tds1 = DataServer.TDataServer()
tds2 = DataServer.TDataServer()

tds1.fileDataRead("tds1.dat")
tds2.fileDataRead("tds2.dat")

tds1.merge(tds2)

tds1.getTuple().Scan("*","","colsize=2 col=3::::5:::3:")

tds1.saveTuple("update")
