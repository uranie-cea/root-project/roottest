from URANIE import DataServer
import ROOT
import argparse

parser = argparse.ArgumentParser("Annoted Geyser Histogram")
parser.add_argument("input", help="input data")
parser.add_argument("--figure", help="image filename", default="figure.png")
parser.add_argument("--style", help="macro filename")
args = parser.parse_args()

ROOT.gStyle.SetMarkerSize(0.01)

tdsGeyser = DataServer.TDataServer()
tdsGeyser.fileDataRead(args.input)

canvas = ROOT.TCanvas()

tdsGeyser.setCut("x1 >= 3.")
tdsGeyser.draw("x2:x1")

if args.style:
    ROOT.gROOT.LoadMacro(args.style)

canvas.SaveAs(args.figure)
