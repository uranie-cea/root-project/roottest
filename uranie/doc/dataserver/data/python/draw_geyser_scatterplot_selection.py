from URANIE import DataServer
import ROOT
import argparse

parser = argparse.ArgumentParser("Pattern selection")
parser.add_argument("input", help="input data")
parser.add_argument("--figure", help="image filename", default="figure.png")
parser.add_argument("--style", help="macro filename", default="")
args = parser.parse_args()

ROOT.gStyle.SetMarkerSize(0.01)

tdsGeyser = DataServer.TDataServer()
tdsGeyser.fileDataRead(args.input)

canvas = ROOT.TCanvas()

tdsGeyser.setSelect("( x1<3.0 ) && ( x2<55.)")
tdsGeyser.draw("x2:x1")

if args.style:
    ROOT.gROOT.LoadMacro(args.style)

canvas.SaveAs(args.figure)

tdsGeyser.draw("x2:x1", "( x1<3.0 ) && ( x2<55.)")
# tdsGeyser.scan("*", "( x1<3.0 ) && ( x2<55.)")
