from URANIE import DataServer # (1)
import ROOT
import argparse

parser = argparse.ArgumentParser("Annoted Geyser Histogram")
parser.add_argument("--figure", help="image filename", default="figure.png")
parser.add_argument("--style", help="macro filename")
args = parser.parse_args()

ROOT.gStyle.SetMarkerSize(0.01)

tds = DataServer.TDataServer() # (2)
tds.ntupleDataRead("hsimple.root","ntuple","px*px:*:py*py", "px*px+py*py < 2.0") # (3)

canvas = ROOT.TCanvas()

tds.draw("py:px") # (4)

if args.style:
    ROOT.gROOT.LoadMacro(args.style)

canvas.SaveAs(args.figure)
