from URANIE import DataServer # (1)
import ROOT
import argparse

parser = argparse.ArgumentParser("Draw simple scatterplot")
parser.add_argument("--figure", help="image filename", default="figure.png")
parser.add_argument("--style", help="macro filename")
args = parser.parse_args()

ROOT.gStyle.SetMarkerSize(0.01)

tdsGeyser = DataServer.TDataServer("tdsgeyser", "Geyser database") # (2)
tdsGeyser.fileDataRead("geyser.dat") # (3)

canvas = ROOT.TCanvas()

tdsGeyser.draw("x2:x1") # (4)

if args.style:
    ROOT.gROOT.LoadMacro(args.style)

canvas.SaveAs(args.figure)
