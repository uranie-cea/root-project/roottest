from URANIE import DataServer

tdsGeyser = DataServer.TDataServer("tdsgeyser", "geyser database")
tdsGeyser.fileDataRead("geyser.dat")
tdsGeyser.addAttribute("y", "sqrt(x2) * x1")

tdsGeyser.exportData("newfile.dat") # (1)
tdsGeyser.exportDataHeader("newfile.C", "x1:x2:y") # (2)
tdsGeyser.exportDataNeMo("newfile.nemo", "x1:x2", "y", "x2<75.0") # (3)
tdsGeyser.exportDataJSon("newfile.json") # (4)
