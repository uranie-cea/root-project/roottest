"""
Example of young modulus data loading
"""
from URANIE import DataServer
import ROOT
import argparse

parser = argparse.ArgumentParser("Young modulus data loading")
parser.add_argument("--figure", help="image filename", default="figure.png")
parser.add_argument("--style", help="macro filename")
parser.add_argument("--json", help="JSON filename", default="")
args = parser.parse_args()

tds = DataServer.TDataServer()
tds.fileDataRead("youngsmodulus.dat")
# gEnv.SetValue("Hist.Binning.1D.x", 10)
# tds.getTuple().Draw("E>>Attribute E(6, 25000, 34000)", "", "text")
# tds.getTuple().Draw("E>>Attribute E(16, 25000, 34000)")

tds.computeStatistic("E")
tds.getAttribute("E").printLog()

tds.exportDataHeader("youngsmodulus.h")

Canvas = ROOT.TCanvas("c1", "Graph for the Macro loadASCIIFile",
                      5, 64, 1270, 667)
pad = ROOT.TPad("pad", "pad", 0, 0.03, 1, 1)
pad.Draw()
if args.style:
    ROOT.gROOT.LoadMacro(args.style)
pad.Divide(2, 2)

pad.cd(1)
tds.draw("E")
pad.cd(2)
tds.draw("E", "", "nclass=sturges")
pad.cd(3)
tds.draw("E", "", "nclass=scott")
pad.cd(4)
tds.draw("E", "", "nclass=fd")

Canvas.SaveAs(args.figure)

if args.json:
    attr_json = ROOT.TBufferJSON.ToJSON(tds.getAttribute("E"))
    with open(args.json, "w") as file:
        file.write(str(attr_json))
