"""
Example of PCA usage (standalone case)
"""
from URANIE import DataServer
import ROOT
import argparse

parser = argparse.ArgumentParser("PCA usage")
parser.add_argument("--figure1", help="image filename 1", default="figure1.png")
parser.add_argument("--figure2", help="image filename 2", default="figure2.png")
parser.add_argument("--style", help="macro filename")
args = parser.parse_args()

# Read the database
tdsPCA = DataServer.TDataServer("tdsPCA", "my TDS")
tdsPCA.fileDataRead("Notes.dat")

# Create the PCA object precising the variables of interest
tpca = DataServer.TPCA(tdsPCA, "Maths:Physics:French:Latin:Music")
tpca.compute()

graphical = True  # do graphs
dumponscreen = True  # or dumping results
showcoordinate = False  # show the points coordinate while dumping results

if graphical:

    # Draw all point in PCA planes
    cPCA = ROOT.TCanvas("cpca", "PCA", 800, 800)
    apad1 = ROOT.TPad("apad1", "apad1", 0, 0.03, 1, 1)
    apad1.Draw()
    apad1.cd()
    apad1.Divide(2, 2)
    apad1.cd(1)
    tpca.drawPCA(1, 2, "Pupil")
    apad1.cd(3)
    tpca.drawPCA(1, 3, "Pupil")
    apad1.cd(4)
    tpca.drawPCA(2, 3, "Pupil")

    # Draw all variable weight in PC definition
    cLoading = ROOT.TCanvas("cLoading", "Loading Plot", 800, 800)
    apad2 = ROOT.TPad("apad2", "apad2", 0, 0.03, 1, 1)
    apad2.Draw()
    apad2.cd()
    apad2.Divide(2, 2)
    apad2.cd(1)
    tpca.drawLoading(1, 2)
    apad2.cd(3)
    tpca.drawLoading(1, 3)
    apad2.cd(4)
    tpca.drawLoading(2, 3)

    # Draw the eigen values in different normalisation
    c = ROOT.TCanvas("cEigenValues", "Eigen Values Plot", 1100, 500)
    apad3 = ROOT.TPad("apad3", "apad3", 0, 0.03, 1, 1)
    apad3.Draw()

    if args.style:
        ROOT.gROOT.LoadMacro(args.style)

    apad3.cd()
    apad3.Divide(3, 1)
    ntd = tpca.getResultNtupleD()
    apad3.cd(1)
    ntd.Draw("eigen:i", "", "lp")
    apad3.cd(2)
    ntd.Draw("eigen_pct:i", "", "lp")
    ROOT.gPad.SetGrid()
    apad3.cd(3)
    ntd.Draw("sum_eigen_pct:i", "", "lp")
    ROOT.gPad.SetGrid()

    cPCA.SaveAs(args.figure2)
    cLoading.SaveAs(args.figure1)
    c.SaveAs("PCA_notes_musique_Eigen.png")

if dumponscreen:

    nPCused = 5  # 3 to see only the meaningful ones
    PCname = ""
    Cosname = ""
    Contrname = ""
    Variable = "Pupil"
    for iatt in range(1, nPCused+1):
        ##list of PC: PC_1:PC_2:PC_3:PC_4:PC_5
        PCname += "PC_"+str(iatt)+":"
        ##list of quality coeff: cosca_1:cosca_2:cosca_3:cosca_4:cosca_5
        Cosname += "cosca_"+str(iatt)+":"
        ##list of contribution: contr_1:contr_2:contr_3:contr_4:contr_5
        Contrname += "contr_"+str(iatt)+":"

    PCname = PCname[:-1]
    Cosname = Cosname[:-1]
    Contrname = Contrname[:-1]

    print("\n====== EigenValues ======")
    tpca.getResultNtupleD().Scan("*")

    if showcoordinate:
        print("\n====== EigenVectors ======")
        tpca._matEigenVectors.Print()

        print("\n====== New Coordinates ======")
        tdsPCA.scan((Variable+":"+PCname))

    varRes = tpca.getVariableResultNtupleD()
    print("\n====== Looking at variables: Quality of representation ======")
    varRes.Scan(("Variable:"+Cosname))

    print("\n====== Looking at variables: Contribution to axis ======")
    varRes.Scan(("Variable:"+Contrname))

    print("\n====== Looking at events:  Quality of representation ======")
    tdsPCA.scan((Variable+":"+Cosname))

    print("\n====== Looking at events: Contribution to axis ========")
    tdsPCA.scan((Variable+":"+Contrname))

    df_tpca_eigen = ROOT.RDataFrame(tpca.getResultNtupleD())
    df_tpca_eigen.Snapshot("ntdPCAeigen__data__tree__", "Notes_tuple_eigen.root")

    df_tpca_var = ROOT.RDataFrame(tpca.getVariableResultNtupleD())
    df_tpca_var.Snapshot("ntdPCAvar__data__tree__", "Notes_tuple_var.root")

    tdsPCA.saveTuple("update")
