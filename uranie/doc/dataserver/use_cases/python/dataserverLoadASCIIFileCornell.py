"""
Example of data loading and stat analysis
"""
from URANIE import DataServer
import ROOT
import argparse, json

parser = argparse.ArgumentParser("Data loading and stat analysis")
parser.add_argument("--json", help="JSON filename", default="")
args = parser.parse_args()

tds = DataServer.TDataServer()
tds.fileDataRead("cornell.dat")

matCorr = tds.computeCorrelationMatrix("")
matCorr.Print()

if args.json:
    mat_json = ROOT.TBufferJSON.ToJSON(matCorr)
    with open(args.json, "w") as file:
        file.write(str(mat_json))
