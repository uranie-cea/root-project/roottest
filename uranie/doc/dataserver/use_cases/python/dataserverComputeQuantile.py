"""
Example of quantile estimation (Wilks and not) for illustration purpose
"""
from ctypes import c_double  # For ROOT version greater or equal to 6.20
import numpy as np
from URANIE import DataServer
import ROOT
import argparse

parser = argparse.ArgumentParser("Quantile estimation for illustration purpose")
parser.add_argument("--figure", help="image filename", default="figure.png")
parser.add_argument("--style", help="macro filename")
args = parser.parse_args()

# Create a DataServer
tds = DataServer.TDataServer("foo", "pouet")
tds.addAttribute("x")  # With one attribute

# Create Histogram to store the quantile values
Q200 = ROOT.TH1F("quantile200", "", 60, 1, 4)
Q200.SetLineColor(1)
Q200.SetLineWidth(2)
Q400 = ROOT.TH1F("quantile400", "", 60, 1, 4)
Q400.SetLineColor(4)
Q400.SetLineWidth(2)
QW95 = ROOT.TH1F("quantileWilks95", "", 60, 1, 4)
QW95.SetLineColor(2)
QW95.SetLineWidth(2)
QW95400 = ROOT.TH1F("quantileWilks95400", "", 60, 1, 4)
QW95400.SetLineColor(8)
QW95400.SetLineWidth(2)
QW99 = ROOT.TH1F("quantileWilks99", "", 60, 1, 4)
QW99.SetLineColor(6)
QW99.SetLineWidth(2)

# Defining the sample size
nb = np.array([200, 400, 59, 90])
proba = 0.95  # Quantile value
CL = np.array([0.95, 0.99])  # Confidence level value for the two Wilks computation
# Loop over the number of estimation
for iq in range(4):

    # Produce 10000 drawing to get smooth distribution
    for itest in range(10000):

        tds.createTuple()  # Create the tuple to store value
        # Fill it with random drawing of centered gaussian
        for ival in range(nb[iq]):
            tds.getTuple().Fill(ival+1, ROOT.gRandom.Gaus(0, 1))

        # Estimate the quantile...
        quant = c_double(0)  # ROOT.Double for ROOT version lower than 6.20
        if iq < 2:

            # ... with usual methods...
            tds.computeQuantile("x", proba, quant)
            if iq == 0:
                Q200.Fill(quant.value)  # ... on a 200-points sample
            else:
                Q400.Fill(quant.value)  # ... on a 400-points sample
                tds.estimateQuantile("x", proba, quant, CL[iq-1])
                QW95400.Fill(quant.value)  # compute the quantile at 95% CL

        else:

            # ... with the wilks optimised sample
            tds.estimateQuantile("x", proba, quant, CL[iq-2])
            if iq == 2:
                QW95.Fill(quant.value)  # compute the quantile at 95% CL
            else:
                QW99.Fill(quant.value)  # compute the quantile at 99% CL

        # Delete the tuple
        tds.deleteTuple()

# Produce the plot with requested style
ROOT.gStyle.SetOptStat(0)
can = ROOT.TCanvas("Can", "Can", 10, 10, 1000, 1000)
Q400.GetXaxis().SetTitle("Quant_{95%}(Gaus_{(0, 1)})")
Q400.GetXaxis().SetTitleOffset(1.2)
Q400.Draw()
Q200.Draw("same")
QW95.Draw("same")
QW95400.Draw("same")
QW99.Draw("same")

# Add the theoretical estimation
lin = ROOT.TLine()
lin.SetLineStyle(3)
lin.DrawLine(1.645, 0, 1.645, Q400.GetMaximum())

# Add a block of legend
leg = ROOT.TLegend(0.4, 0.6, 0.8, 0.85)
leg.AddEntry(lin, "Theoretical quantile", "l")
leg.AddEntry(Q200, "Usual quantile (200 pts)", "l")
leg.AddEntry(Q400, "Usual quantile (400 pts)", "l")
leg.AddEntry(QW95, "Wilks quantile CL=95% (59 pts)", "l")
leg.AddEntry(QW95400, "Wilks quantile CL=95% (400 pts)", "l")
leg.AddEntry(QW99, "Wilks quantile CL=99% (90 pts)", "l")
leg.SetBorderSize(0)
leg.Draw()

if args.style:
    ROOT.gROOT.LoadMacro(args.style)

can.SaveAs(args.figure)
