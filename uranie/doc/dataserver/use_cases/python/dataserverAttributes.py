"""
Example of attribute management
"""
from URANIE import DataServer
import ROOT
import argparse, json

parser = argparse.ArgumentParser('Attribute management')
parser.add_argument("--json", help="JSON filename", default="")
args = parser.parse_args()

# Define the attribute "x"
px = DataServer.TAttribute("x", -2.0, 4.0)
px.setTitle("#Delta P^{#sigma}")
px.setUnity("#frac{mm^{2}}{s}")

# Define the attribute "y"
py = DataServer.TAttribute("y", 0.0, 1.0)

# Define the DataServer of the study
tds = DataServer.TDataServer("tds", "my first TDS")

# Add the attributes in the DataServer.TDataServer
tds.addAttribute(px)
tds.addAttribute(py)
tds.addAttribute(DataServer.TAttribute("z", 0.25, 0.50))
tds.printLog()

if args.json:
    tds_json = ROOT.TBufferJSON.ToJSON(tds)
    with open(args.json, "w") as file:
        file.write(str(tds_json))
