"""
Example of data loading with pasture file
"""
from URANIE import DataServer, Sampler, Launcher
import ROOT
import argparse

parser = argparse.ArgumentParser("Data loading with pasture file")
parser.add_argument("--style", help="macro filename")
parser.add_argument("--no-progressBar", dest="progressBar", help="deactivate progress bar", action="store_false")
args = parser.parse_args()

C = ROOT.TCanvas("mycanvas", "mycanvas", 1)
ROOT.gROOT.LoadMacro("UserFunctions.C")

tds = DataServer.TDataServer()
tds.fileDataRead("pasture.dat")

tds.getTuple().SetMarkerStyle(8)
tds.getTuple().SetMarkerSize(1.5)
tds.draw("yield:time")

tlf = Launcher.TLauncherFunction(tds, "ModelPasture", "time", "yhat")
tlf.setDrawProgressBar(args.progressBar)
tlf.run()

tds.getTuple().SetMarkerColor(ROOT.kBlue)
tds.getTuple().SetLineColor(ROOT.kBlue)

tds.draw("yhat:time", "", "lpsame")

tds2 = DataServer.TDataServer()
tds2.addAttribute(DataServer.TUniformDistribution("time2", 9, 80))

tsamp = Sampler.TSampling(tds2, "lhs", 1000)
tsamp.generateSample()

tds2.getTuple().SetMarkerColor(ROOT.kGreen)
tds2.getTuple().SetLineColor(ROOT.kGreen)
tlf = Launcher.TLauncherFunction(tds2, "ModelPasture", "", "yhat2")
tlf.setDrawProgressBar(args.progressBar)
tlf.run()

tds2.draw("yhat2:time2", "", "psame")
tds.draw("yhat:time", "", "lpsame")

if args.style:
    ROOT.gROOT.LoadMacro(args.style)

ROOT.gPad.SaveAs("pasture.png")
