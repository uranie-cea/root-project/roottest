"""
Example of data loading for Ionosphere dataset
"""
from URANIE import DataServer
import ROOT
import argparse, json

parser = argparse.ArgumentParser("Loading Ionosphere dataset")
parser.add_argument("--figure", help="image filename", default="figure.png")
parser.add_argument("--style", help="macro filename")
args = parser.parse_args()

tds = DataServer.TDataServer()
tds.fileDataRead("ionosphere.dat")

tds.getAttribute("x28").SetTitle("#Delta P_{e}^{F_{iso}}")

# Graph
Canvas = ROOT.TCanvas("c1", "Graph for the Macro loadASCIIFileIonosphere",
                      5, 64, 1270, 667)
tds.draw("x28")

if args.style:
    ROOT.gROOT.LoadMacro(args.style)

Canvas.SaveAs(args.figure)
