"""
Example of rank usage for illustration purpose
"""
from URANIE import DataServer
import ROOT
import argparse, json

parser = argparse.ArgumentParser('Rank usage')
parser.add_argument('--json', help='json filename', default="")
args = parser.parse_args()

tdsGeyser = DataServer.TDataServer("geyser", "poet")
tdsGeyser.fileDataRead("geyser.dat")
tdsGeyser.computeRank("x1")
tdsGeyser.computeStatistic("Rk_x1")

print("NPatterns="+str(tdsGeyser.getNPatterns())+";  min(Rk_x1)= " +
      str(tdsGeyser.getAttribute("Rk_x1").getMinimum())+";  max(Rk_x1)= " +
      str(tdsGeyser.getAttribute("Rk_x1").getMaximum()))

if args.json:
    d = {"min_Rk_x1":tdsGeyser.getAttribute("Rk_x1").getMinimum(),
        "max_Rk_x1":tdsGeyser.getAttribute("Rk_x1").getMaximum(),
        "NPatterns":tdsGeyser.getNPatterns()}
    with open(args.json, "w") as file:
        json.dump(d, file, indent=4)
