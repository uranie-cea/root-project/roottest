"""
Example of correlation matrix computation for vector
"""
from URANIE import DataServer
import ROOT
import argparse

parser = argparse.ArgumentParser('Correlation matrix computation for vector')
parser.add_argument("--basename", help="base name of json file", default="")
args = parser.parse_args()

tdsop = DataServer.TDataServer("foo", "poet")
tdsop.fileDataRead("tdstest.dat")

# Consider a and x attributes (every element of the vector)
globalOne = tdsop.computeCorrelationMatrix("x:a")
globalOne.Print()

# Consider a and x attributes (cherry-picking a single element of the vector)
focusedOne = tdsop.computeCorrelationMatrix("x[1]:a")
focusedOne.Print()

if args.basename:
    matG_json = ROOT.TBufferJSON.ToJSON(globalOne)
    matF_json = ROOT.TBufferJSON.ToJSON(focusedOne)

    with open(args.basename+"_global.json", "w") as file:
        file.write(str(matG_json))
    with open(args.basename+"_focused.json", "w") as file:
        file.write(str(matF_json))
