"""
Example of statistical estimation on vector attributes
"""
from URANIE import DataServer
import ROOT
import argparse, json

parser = argparse.ArgumentParser('Statistical estimation on vector attributes')
parser.add_argument('--json', help='base name of the json file', default="")
args = parser.parse_args()

tdsop = DataServer.TDataServer("foo", "poet")
tdsop.fileDataRead("tdstest.dat")

# Considering every element of a vector independent from the others
tdsop.computeStatistic("x")
px = tdsop.getAttribute("x")

print("min(x[0])= "+str(px.getMinimum(0))+";  max(x[0])= "+str(px.getMaximum(0))
      + ";  mean(x[0])= "+str(px.getMean(0))+";  std(x[0])= "+str(px.getStd(0)))
print("min(x[1])= "+str(px.getMinimum(1))+";  max(x[1])= "+str(px.getMaximum(1))
      + ";  mean(x[1])= "+str(px.getMean(1))+";  std(x[1])= "+str(px.getStd(1)))
print("min(x[2])= "+str(px.getMinimum(2))+";  max(x[2])= "+str(px.getMaximum(2))
      + ";  mean(x[2])= "+str(px.getMean(2))+";  std(x[2])= "+str(px.getStd(2)))
print("min(xtot)= "+str(px.getMinimum(3))+";  max(xtot)= "+str(px.getMaximum(3))
      + ";  mean(xtot)= "+str(px.getMean(3))+";  std(xtot)= "+str(px.getStd(3)))

# Statistic for a single realisation of a vector, not considering other events
tdsop.addAttribute("Min_x", "Min$(x)")
tdsop.addAttribute("Max_x", "Max$(x)")
tdsop.addAttribute("Mean_x", "Sum$(x)/Length$(x)")

tdsop.scan("x:Min_x:Max_x:Mean_x", "", "colsize=5 col=2:::6")

tdsop.saveTuple("update")

if args.json:
    d = {names: {"min":px.getMinimum(n_i),
                "max":px.getMaximum(n_i),
                "mean":px.getMean(n_i),
                "std":px.getStd(n_i)} for n_i, names in enumerate(["x0","x1","x2","xtot"])}
    with open(args.json, "w") as file:
        json.dump(d, file, indent=4)
