"""
Example of data merging
"""
from URANIE import DataServer
import ROOT
import argparse

parser = argparse.ArgumentParser("DataMerging")
parser.add_argument("--basename", help="ROOT binary basename", default="")
parser.add_argument("--tree1", help="ROOT binary treename 1", default="")
parser.add_argument("--tree2", help="ROOT binary treename 2", default="")
args = parser.parse_args()

tds1 = DataServer.TDataServer()
tds2 = DataServer.TDataServer()

tds1.fileDataRead("tds1.dat")
print("Dumping tds1")
tds1.Scan("*")

if (args.basename and args.tree1):
    df1 = ROOT.RDataFrame(tds1.getTuple())
    df1.Snapshot(args.tree1, args.basename+"Init.root")

tds2.fileDataRead("tds2.dat")
print("Dumping tds2")
tds2.Scan("*")

tds2.saveTuple("update")

tds1.merge(tds2)
print("Dumping merged tds1 and tds2")
tds1.Scan("*", "", "colsize=3 col=9::::::::")

if (args.basename and args.tree2):
    dfMerge = ROOT.RDataFrame(tds1.getTuple())
    dfMerge.Snapshot(args.tree2, args.basename+"Merge.root")
