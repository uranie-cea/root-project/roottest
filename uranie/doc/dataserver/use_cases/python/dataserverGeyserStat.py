"""
Example of statistical usage
"""
from URANIE import DataServer
import ROOT
import argparse, json

parser = argparse.ArgumentParser('Statistical usage')
parser.add_argument('--json', help='json filename', default="")
args = parser.parse_args()

tdsGeyser = DataServer.TDataServer("geyser", "poet")
tdsGeyser.fileDataRead("geyser.dat")
tdsGeyser.computeStatistic("x1")

print("min(x1)= "+str(tdsGeyser.getAttribute("x1").getMinimum())+";  max(x1)= "
      + str(tdsGeyser.getAttribute("x1").getMaximum())+";  mean(x1)= " +
      str(tdsGeyser.getAttribute("x1").getMean())+";  std(x1)= " +
      str(tdsGeyser.getAttribute("x1").getStd()))

if args.json:
    d = {"min_x1":tdsGeyser.getAttribute("x1").getMinimum(),
        "max_x1":tdsGeyser.getAttribute("x1").getMaximum(),
        "mean_x1":tdsGeyser.getAttribute("x1").getMean(),
        "std_x1":tdsGeyser.getAttribute("x1").getStd()}
    with open(args.json, "w") as file:
        json.dump(d, file, indent=4)
