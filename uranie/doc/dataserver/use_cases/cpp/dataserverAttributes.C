    using namespace URANIE::DataServer;

    // Define the DataServer of the study
    TDataServer *tds = new TDataServer("tds", "my first TDS");

void dataserverAttributes(const string& file_name=""){
    // Define the attribute "x"
    TAttribute *px = new TAttribute("x", -2.0, 4.0);
    px->setTitle("#Delta P^{#sigma}");
    px->setUnity("#frac{mm^{2}}{s}");

    // Define the attribute "y"
    TAttribute *py = new TAttribute("y", 0.0, 1.0);

    // Add the attributes in the TDataServer
    tds->addAttribute(px);
    tds->addAttribute(py);

    tds->addAttribute(new TAttribute("z", 0.25, 0.50));

    tds->printLog();
    
    if (!file_name.empty())
    {
        auto tds_json = TBufferJSON::ToJSON(tds);
        ofstream out (file_name.data());
        out << tds_json << endl;
        out.close();
    }
}
