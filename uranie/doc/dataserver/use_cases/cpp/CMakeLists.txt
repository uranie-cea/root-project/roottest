ROOTTEST_ADD_TESTDIRS()

include(UranieModules)
include(CMakePrintHelpers)
include("${CMAKE_SOURCE_DIR}/uranie/doc/dataserver/use_cases/regex_table.cmake")

################## CREATION SOUS-DOSSIERS POUR TESTS C++ ##################

file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/tdstest_1)
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/tdstest_2)
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/tdstest_3)
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/geyser_1)
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/geyser_2)

################## COPIES FICHIERS POUR TESTS C++ ##################

configure_file(${CMAKE_SOURCE_DIR}/uranie/references/dataserver/tds1.dat
    ${CMAKE_CURRENT_BINARY_DIR} COPYONLY)
configure_file(${CMAKE_SOURCE_DIR}/uranie/references/dataserver/tds2.dat
    ${CMAKE_CURRENT_BINARY_DIR} COPYONLY)
configure_file(${CMAKE_SOURCE_DIR}/uranie/references/flowrateUniformDesign.dat
    ${CMAKE_CURRENT_BINARY_DIR} COPYONLY)
configure_file(${CMAKE_SOURCE_DIR}/uranie/references/dataserver/pasture.dat
    ${CMAKE_CURRENT_BINARY_DIR} COPYONLY)
configure_file(${CMAKE_SOURCE_DIR}/uranie/references/youngsmodulus.dat
    ${CMAKE_CURRENT_BINARY_DIR} COPYONLY)
configure_file(${CMAKE_SOURCE_DIR}/uranie/references/cornell.dat ${CMAKE_CURRENT_BINARY_DIR} COPYONLY)
configure_file(${CMAKE_SOURCE_DIR}/uranie/references/dataserver/ionosphere.dat
    ${CMAKE_CURRENT_BINARY_DIR} COPYONLY)
configure_file(${CMAKE_SOURCE_DIR}/uranie/references/dataserver/tdstest.dat
    ${CMAKE_CURRENT_BINARY_DIR}/tdstest_1 COPYONLY)
configure_file(${CMAKE_SOURCE_DIR}/uranie/references/dataserver/tdstest.dat
    ${CMAKE_CURRENT_BINARY_DIR}/tdstest_2 COPYONLY)
configure_file(${CMAKE_SOURCE_DIR}/uranie/references/dataserver/tdstest.dat
    ${CMAKE_CURRENT_BINARY_DIR}/tdstest_3 COPYONLY)
configure_file(${CMAKE_SOURCE_DIR}/uranie/references/geyser.dat
    ${CMAKE_CURRENT_BINARY_DIR}/geyser_1 COPYONLY)
configure_file(${CMAKE_SOURCE_DIR}/uranie/references/geyser.dat
    ${CMAKE_CURRENT_BINARY_DIR}/geyser_2 COPYONLY)
configure_file(${CMAKE_SOURCE_DIR}/uranie/references/dataserver/aTDSWithVectors.dat
    ${CMAKE_CURRENT_BINARY_DIR} COPYONLY)
configure_file(${CMAKE_SOURCE_DIR}/uranie/references/dataserver/Notes.dat
    ${CMAKE_CURRENT_BINARY_DIR} COPYONLY)

################## TESTS GENERATION IMAGE C++ ##################

ROOTTEST_ADD_TEST(dataserverComputeQuantile MACRO dataserverComputeQuantile.C
    MACROARG "\"dataserverComputeQuantile.png\", \"${CMAKE_SOURCE_DIR}/uranie/doc/test_style_OLD.C\"")

ROOTTEST_ADD_TEST(dataserverDrawQQPlot MACRO dataserverDrawQQPlot.C
    MACROARG "\"dataserverDrawQQPlot.png\", 12345, \"${CMAKE_SOURCE_DIR}/uranie/doc/test_style_OLD.C\"")

ROOTTEST_ADD_TEST(dataserverDrawPPPlot MACRO dataserverDrawPPPlot.C
    MACROARG "\"dataserverDrawPPPlot.png\", 12345, \"${CMAKE_SOURCE_DIR}/uranie/doc/test_style_OLD.C\"")

################## TESTS SORTIE CONSOLE ET GENERATION IMAGE C++ ##################

ROOTTEST_ADD_TEST(dataserverLoadASCIIFilePasture MACRO dataserverLoadASCIIFilePasture.C
    MACROARG "\"${CMAKE_SOURCE_DIR}/uranie/doc/test_style_OLD.C\", false"
    POSTCMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/delete_ansi.py
        dataserverLoadASCIIFilePasture_clean.log dataserverLoadASCIIFilePasture.log
    COPY_TO_BUILDDIR ${CMAKE_SOURCE_DIR}/uranie/doc/UserFunctions.C)

ROOTTEST_ADD_TEST(dataserverLoadASCIIFile MACRO dataserverLoadASCIIFile.C
    MACROARG "\"dataserverLoadASCIIFile.png\", \"${CMAKE_SOURCE_DIR}/uranie/doc/test_style_OLD.C\"")

ROOTTEST_ADD_TEST(dataserverLoadASCIIFileIonosphere MACRO dataserverLoadASCIIFileIonosphere.C
    MACROARG "\"dataserverLoadASCIIFileIonosphere.png\",
        \"${CMAKE_SOURCE_DIR}/uranie/doc/test_style_OLD.C\"")

ROOTTEST_ADD_TEST(dataserverLoadASCIIFileYoungsModulus MACRO dataserverLoadASCIIFileYoungsModulus.C
    MACROARG "\"dataserverLoadASCIIFileYoungsModulus.png\",
        \"${CMAKE_SOURCE_DIR}/uranie/doc/test_style_OLD.C\",
        \"dataserverLoadASCIIFileYoungsModulus.json\""
    POSTCMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/delete_ansi.py
        dataserverLoadASCIIFileYoungsModulus_clean.log dataserverLoadASCIIFileYoungsModulus.log)

ROOTTEST_ADD_TEST(dataserverPCAExample MACRO dataserverPCAExample.C
    MACROARG "\"PCA_notes_musique_VariableCircle.png\", \"PCA_notes_musique_ontoPC.png\",
        \"${CMAKE_SOURCE_DIR}/uranie/doc/test_style_OLD.C\"")

set(PYROOT_EXTRAFLAGS_SAVED ${PYROOT_EXTRAFLAGS})
set(PYROOT_EXTRAFLAGS)
cmake_print_variables(CMAKE_CURRENT_LIST_FILE PYROOT_EXTRAFLAGS)

ROOTTEST_ADD_TEST(dataserverPCAExample_console MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/test_regex_key.py
    OPTS "${CMAKE_CURRENT_BINARY_DIR}/dataserverPCAExample.log"
        "${CMAKE_SOURCE_DIR}/uranie/doc/dataserver/use_cases/regex_table_pca.json"
    DEPENDS dataserverPCAExample)

ROOTTEST_ADD_TEST(dataserverMerge_console MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/test_regex_key.py
    OPTS "${CMAKE_CURRENT_BINARY_DIR}/dataserverMerge.log"
        "${CMAKE_SOURCE_DIR}/uranie/doc/dataserver/use_cases/regex_table_merge.json"
    DEPENDS dataserverMerge)

################## TESTS SORTIE CONSOLE C++ ##################

ROOTTEST_ADD_TEST(dataserverAttributes MACRO dataserverAttributes.C
    MACROARG "\"dataserverAttributes.json\""
    POSTCMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/delete_ansi.py
        dataserverAttributes_clean.log dataserverAttributes.log)

ROOTTEST_ADD_TEST(dataserverMerge MACRO dataserverMerge.C
    MACROARG "\"tds\", \"tdsInit__data__tree__\", \"tdsMerge__data__tree__\""
    POSTCMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/delete_ansi.py
        dataserverMerge_clean.log dataserverMerge.log)

ROOTTEST_ADD_TEST(dataserverLoadASCIIFileCornell MACRO dataserverLoadASCIIFileCornell.C
    MACROARG "\"dataserverLoadASCIIFileCornell.json\""
    POSTCMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/delete_ansi.py
        dataserverLoadASCIIFileCornell_clean.log dataserverLoadASCIIFileCornell.log)

ROOTTEST_ADD_TEST(dataserverGeyserRank MACRO dataserverGeyserRank.C
    MACROARG "\"dataserverGeyserRank.json\""
    WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/geyser_1)

ROOTTEST_ADD_TEST(dataserverGeyserStat MACRO dataserverGeyserStat.C
    MACROARG "\"dataserverGeyserStat.json\""
    WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/geyser_2)

ROOTTEST_ADD_TEST(dataserverComputeQuantileVec MACRO dataserverComputeQuantileVec.C
    MACROARG "\"dataserverComputeQuantileVec.json\"")

ROOTTEST_ADD_TEST(dataserverNormaliseVector MACRO dataserverNormaliseVector.C
    WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/tdstest_1
    PASSREGEX ${REGEX_CONSOLE_NORMALISE})

ROOTTEST_ADD_TEST(dataserverComputeStatVector MACRO dataserverComputeStatVector.C
    MACROARG "\"dataserverComputeStatVector.json\""
    WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/tdstest_2
    PASSREGEX ${REGEX_CONSOLE_STAT})

ROOTTEST_ADD_TEST(dataserverComputeCorrelationMatrixVector
    MACRO dataserverComputeCorrelationMatrixVector.C
    MACROARG "\"dataserverComputeCorrelationMatrixVector\""
    WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/tdstest_3)

################## COMPARAISON TABLEAUX SORTIE CONSOLE ##################

find_python_module(pandas OPTIONAL)
if(PY_PANDAS_FOUND)
    ROOTTEST_ADD_TEST(dataserverMerge_array MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_arrays.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/dataserver/use_cases/dataserverMerge.json"
            "dataserverMerge.json"
        PRECMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/convert_tree_json.py
            "dataserverMerge.json" "tdsMerge.root" "tdsMerge__data__tree__"
        DEPENDS dataserverMerge)

    ROOTTEST_ADD_TEST(dataserverMerge_tds1_array MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_arrays.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/dataserver/use_cases/dataserverMerge_tds1.json"
            "dataserverMerge_tds1.json"
        PRECMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/convert_tree_json.py
            "dataserverMerge_tds1.json" "tdsInit.root" "tdsInit__data__tree__"
        DEPENDS dataserverMerge)

    ROOTTEST_ADD_TEST(dataserverMerge_tds2_array MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_arrays.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/dataserver/use_cases/dataserverMerge_tds2.json"
            "dataserverMerge_tds2.json"
        PRECMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/convert_tree_json.py
            "dataserverMerge_tds2.json" "tds2.root" "tds2__data__tree__"
        DEPENDS dataserverMerge)

    ROOTTEST_ADD_TEST(dataserverNormaliseVector_array
        MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_arrays.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/dataserver/use_cases/dataserverNormaliseVector.json"
        "dataserverNormaliseVector.json"
        PRECMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/convert_tree_json.py
        "dataserverNormaliseVector.json" "tdstest.root" "foo__data__tree__"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/tdstest_1
        DEPENDS dataserverNormaliseVector)

    ROOTTEST_ADD_TEST(dataserverComputeStatVector_array
        MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_arrays.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/dataserver/use_cases/dataserverComputeStatVector_table.json"
        "dataserverComputeStatVector_table.json"
        PRECMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/convert_tree_json.py
        "dataserverComputeStatVector_table.json" "tdstest.root" "foo__data__tree__"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/tdstest_2
        DEPENDS dataserverComputeStatVector)

    ROOTTEST_ADD_TEST(dataserverPCAExample_tds_array
        MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_arrays.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/dataserver/use_cases/dataserverPCAExample_tds.json"
        "dataserverPCAExample_tds.json"
        PRECMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/convert_tree_json.py
        "dataserverPCAExample_tds.json" "Notes.root" "tdsPCA__data__tree__"
        DEPENDS dataserverPCAExample)

    ROOTTEST_ADD_TEST(dataserverPCAExample_tuple_eigen_array
        MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_arrays.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/dataserver/use_cases/dataserverPCAExample_tuple_eigen.json"
        "dataserverPCAExample_tuple_eigen.json"
        PRECMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/convert_tree_json.py
        "dataserverPCAExample_tuple_eigen.json" "Notes_tuple_eigen.root" "ntdPCAeigen__data__tree__"
        DEPENDS dataserverPCAExample)

    ROOTTEST_ADD_TEST(dataserverPCAExample_tuple_var_array
        MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_arrays.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/dataserver/use_cases/dataserverPCAExample_tuple_var.json"
        "dataserverPCAExample_tuple_var.json"
        PRECMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/convert_tree_json.py
        "dataserverPCAExample_tuple_var.json" "Notes_tuple_var.root" "ntdPCAvar__data__tree__"
        DEPENDS dataserverPCAExample)
endif()

################## COMPARAISON VALEURS NUMERIQUES SORTIE CONSOLE C++ ##################

find_python_module(jsoncomparison OPTIONAL)
if(PY_JSONCOMPARISON_FOUND)
    ROOTTEST_ADD_TEST(dataserverAttributes_console MACRO
        ${CMAKE_SOURCE_DIR}/uranie/doc/compare_json_console.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/dataserver/use_cases/dataserverAttributes.json"
        "dataserverAttributes.json"
        DEPENDS dataserverAttributes)

    ROOTTEST_ADD_TEST(dataserverLoadASCIIFileCornell_matrix MACRO
        ${CMAKE_SOURCE_DIR}/uranie/doc/compare_json_console.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/dataserver/use_cases/dataserverLoadASCIIFileCornell.json"
        "dataserverLoadASCIIFileCornell.json"
        DEPENDS dataserverLoadASCIIFileCornell)

    ROOTTEST_ADD_TEST(dataserverLoadASCIIFileYoungsModulus_console MACRO
        ${CMAKE_SOURCE_DIR}/uranie/doc/compare_json_console.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/dataserver/use_cases/dataserverLoadASCIIFileYoungsModulus.json"
        "dataserverLoadASCIIFileYoungsModulus.json"
        DEPENDS dataserverLoadASCIIFileYoungsModulus)

    ROOTTEST_ADD_TEST(dataserverComputeQuantileVec_console MACRO
        ${CMAKE_SOURCE_DIR}/uranie/doc/compare_json_console.py
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/dataserver/use_cases/dataserverComputeQuantileVec.json"
        "dataserverComputeQuantileVec.json"
        DEPENDS dataserverComputeQuantileVec)

    ROOTTEST_ADD_TEST(dataserverGeyserRank_console MACRO
        ${CMAKE_SOURCE_DIR}/uranie/doc/compare_json_console.py
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/geyser_1
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/dataserver/use_cases/dataserverGeyserRank.json"
        "dataserverGeyserRank.json"
        DEPENDS dataserverGeyserRank)

    ROOTTEST_ADD_TEST(dataserverGeyserStat_console MACRO
        ${CMAKE_SOURCE_DIR}/uranie/doc/compare_json_console.py
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/geyser_2
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/dataserver/use_cases/dataserverGeyserStat.json"
        "dataserverGeyserStat.json"
        DEPENDS dataserverGeyserStat)

    ROOTTEST_ADD_TEST(dataserverComputeStatVector_console MACRO
        ${CMAKE_SOURCE_DIR}/uranie/doc/compare_json_console.py
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/tdstest_2
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/dataserver/use_cases/dataserverComputeStatVector.json"
        "dataserverComputeStatVector.json"
        DEPENDS dataserverComputeStatVector)

    ROOTTEST_ADD_TEST(dataserverComputeCorrelationMatrixVector_global_matrix MACRO
        ${CMAKE_SOURCE_DIR}/uranie/doc/compare_json_console.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/dataserver/use_cases/dataserverComputeCorrelationMatrixVector_global.json"
        "dataserverComputeCorrelationMatrixVector_global.json"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/tdstest_3
        DEPENDS dataserverComputeCorrelationMatrixVector)

    ROOTTEST_ADD_TEST(dataserverComputeCorrelationMatrixVector_focused_matrix MACRO
        ${CMAKE_SOURCE_DIR}/uranie/doc/compare_json_console.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/dataserver/use_cases/dataserverComputeCorrelationMatrixVector_focused.json"
        "dataserverComputeCorrelationMatrixVector_focused.json"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/tdstest_3
        DEPENDS dataserverComputeCorrelationMatrixVector)
endif()

################## COMPARAISON IMAGES ##################

find_python_module(skimage OPTIONAL)
if(PY_SKIMAGE_FOUND)
    ROOTTEST_ADD_TEST(dataserverLoadASCIIFilePasture_figure MACRO
        ${CMAKE_SOURCE_DIR}/uranie/doc/compare_images.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/dataserver/use_cases/pasture.png"
            "pasture.png"
            "pasture_diff.png"
        DEPENDS dataserverLoadASCIIFilePasture)

    ROOTTEST_ADD_TEST(dataserverLoadASCIIFile_figure MACRO
        ${CMAKE_SOURCE_DIR}/uranie/doc/compare_images.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/dataserver/use_cases/dataserverLoadASCIIFile.png"
            "dataserverLoadASCIIFile.png"
            "dataserverLoadASCIIFile_diff.png"
        DEPENDS dataserverLoadASCIIFile)

    ROOTTEST_ADD_TEST(dataserverLoadASCIIFileYoungsModulus_figure MACRO
        ${CMAKE_SOURCE_DIR}/uranie/doc/compare_images.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/dataserver/use_cases/dataserverLoadASCIIFileYoungsModulus.png"
            "dataserverLoadASCIIFileYoungsModulus.png"
            "dataserverLoadASCIIFileYoungsModulus_diff.png"
        DEPENDS dataserverLoadASCIIFileYoungsModulus)

    ROOTTEST_ADD_TEST(dataserverLoadASCIIFileIonosphere_figure MACRO
        ${CMAKE_SOURCE_DIR}/uranie/doc/compare_images.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/dataserver/use_cases/dataserverLoadASCIIFileIonosphere.png"
            "dataserverLoadASCIIFileIonosphere.png"
            "dataserverLoadASCIIFileIonosphere_diff.png"
        DEPENDS dataserverLoadASCIIFileIonosphere)

    ROOTTEST_ADD_TEST(dataserverComputeQuantile_figure MACRO
        ${CMAKE_SOURCE_DIR}/uranie/doc/compare_images.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/dataserver/use_cases/dataserverComputeQuantile.png"
            "dataserverComputeQuantile.png"
            "dataserverComputeQuantile_diff.png"
        DEPENDS dataserverComputeQuantile)

    ROOTTEST_ADD_TEST(dataserverDrawQQPlot_figure MACRO
        ${CMAKE_SOURCE_DIR}/uranie/doc/compare_images.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/dataserver/use_cases/dataserverDrawQQPlot.png"
            "dataserverDrawQQPlot.png"
            "dataserverDrawQQPlot_diff.png"
        DEPENDS dataserverDrawQQPlot
        ${WILLFAIL_ON_WIN32})

    ROOTTEST_ADD_TEST(dataserverDrawPPPlot_figure MACRO
        ${CMAKE_SOURCE_DIR}/uranie/doc/compare_images.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/dataserver/use_cases/dataserverDrawPPPlot.png"
            "dataserverDrawPPPlot.png"
            "dataserverDrawPPPlot_diff.png"
        DEPENDS dataserverDrawPPPlot
        ${WILLFAIL_ON_WIN32})

    ROOTTEST_ADD_TEST(dataserverPCAExample_eigen_figure
        MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_images.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/dataserver/pca/PCA_notes_musique_Eigen.png"
            "PCA_notes_musique_Eigen.png"
            "PCA_notes_musique_Eigen_diff.png"
        DEPENDS dataserverPCAExample)

    ROOTTEST_ADD_TEST(dataserverPCAExample_circle_figure
        MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_images.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/dataserver/pca/PCA_notes_musique_VariableCircle.png"
            "PCA_notes_musique_VariableCircle.png"
            "PCA_notes_musique_VariableCircle_diff.png"
        DEPENDS dataserverPCAExample)

    ROOTTEST_ADD_TEST(dataserverPCAExample_ontoPC_figure
        MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_images.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/dataserver/pca/PCA_notes_musique_ontoPC.png"
            "PCA_notes_musique_ontoPC.png"
            "PCA_notes_musique_ontoPC_diff.png"
        DEPENDS dataserverPCAExample)
endif()

set(PYROOT_EXTRAFLAGS ${PYROOT_EXTRAFLAGS_SAVED})
