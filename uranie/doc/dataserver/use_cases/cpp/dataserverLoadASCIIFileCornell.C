    using namespace URANIE::DataServer;

    TDataServer * tds = new TDataServer();

void dataserverLoadASCIIFileCornell(const string& file_name=""){
    tds->fileDataRead("cornell.dat");

    TMatrixD matCorr = tds->computeCorrelationMatrix("");
    matCorr.Print();

    if (!file_name.empty())
    {
        auto mat_json = TBufferJSON::ToJSON(&matCorr);
        ofstream out (file_name.data());
        out << mat_json << endl;
        out.close();
    }
}
