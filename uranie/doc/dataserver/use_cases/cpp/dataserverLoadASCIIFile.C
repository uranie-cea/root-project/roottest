    using namespace URANIE::DataServer;

    // Create a TDataServer
    TDataServer * tds = new TDataServer();

    TCanvas  *Canvas = new TCanvas("c1", "Graph for the Macro loadASCIIFile",5,64,1270,667);
    TPad *pad = new TPad("pad","pad",0, 0.03, 1, 1);

void dataserverLoadASCIIFile(const string& figure="figure.png", const string& style=""){
    // Load the data base in the DataServer
    tds->fileDataRead("flowrateUniformDesign.dat");

    // Graph
    pad->Draw();
    if (!style.empty())
        gROOT->LoadMacro(style.data());
    pad->Divide(2,2);

    pad->cd(1); tds->draw("ystar");
    pad->cd(2); tds->draw("ystar:rw");
    pad->cd(3); tds->drawTufte("ystar:rw");
    pad->cd(4); tds->drawProfile("ystar:rw");

    tds->startViewer();

    Canvas->SaveAs(figure.data());
}
