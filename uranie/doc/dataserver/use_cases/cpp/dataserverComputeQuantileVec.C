    #include <nlohmann/json.hpp>
    using namespace URANIE::DataServer;

    TDataServer *tdsvec = new TDataServer("foo", "bar");

void dataserverComputeQuantileVec(const string& file_name="") {
    tdsvec->fileDataRead("aTDSWithVectors.dat");

    double probas[3]={0.2, 0.6, 0.8}; double quants[3];
    tdsvec->computeQuantile("rank", 3, probas, quants);

    TAttribute *prank = tdsvec->getAttribute("rank");
    int nbquant;
    prank->getQuantilesSize(nbquant); // (1)
    cout << "nbquant = " << nbquant << endl;

    double aproba=0.8; double aquant;
    prank->getQuantile(aproba, aquant); // (2)
    cout << "aproba = " << aproba << ", aquant = " <<
    aquant << endl;

    double theproba[3], thequant[3];
    prank->getQuantiles(theproba, thequant); // (3)
    for(int i_quant=0; i_quant<nbquant; ++i_quant) {
        cout << "(theproba, thequant)[" << i_quant << "] = "
        << "(" << theproba[i_quant] << ", " <<
        thequant[i_quant] << ")" << endl;
    }

    vector<double> allquant;
    prank->getQuantileVector(aproba, allquant); // (4)
    cout << "aproba = " << aproba << ", allquant = ";
    for(double quant_i: allquant)
        cout << quant_i << " ";
    cout << endl;

    if (!file_name.empty())
    {
        nlohmann::json jfile;
        vector<double> theproba_vec(begin(theproba), end(theproba));
        vector<double> thequant_vec(begin(thequant), end(thequant));

        ofstream output (file_name.data());
        jfile["nbquant"] = nbquant;
        jfile["aproba"] = aproba;
        jfile["aquant"] = aquant;
        jfile["thequant"] = thequant_vec;
        jfile["theproba"] = theproba_vec;
        jfile["allquant"] = allquant;
        output << jfile.dump(4) << endl;
        output.close();
    }
}
