    #include <nlohmann/json.hpp>
    using namespace URANIE::DataServer;

    TDataServer *tdsop =new TDataServer("foo","poet");

void dataserverComputeStatVector(const string& file_name="") {
    tdsop->fileDataRead("tdstest.dat");

    //Considering every element of a vector independent from the others
    tdsop->computeStatistic("x");
    TAttribute *px = tdsop->getAttribute("x");

    cout<<"min(x[0])= "<<px->getMinimum(0)<<";  max(x[0])= "<<px->getMaximum(0)
        <<";  mean(x[0])= "<<px->getMean(0)<<";  std(x[0])= "<<px->getStd(0)<<endl;
    cout<<"min(x[1])= "<<px->getMinimum(1)<<";  max(x[1])= "<<px->getMaximum(1)
        <<";  mean(x[1])= "<<px->getMean(1)<<";  std(x[1])= "<<px->getStd(1)<<endl;
    cout<<"min(x[2])= "<<px->getMinimum(2)<<";  max(x[2])= "<<px->getMaximum(2)
        <<";  mean(x[2])= "<<px->getMean(2)<<";  std(x[2])= "<<px->getStd(2)<<endl;
    cout<<"min(xtot)= "<<px->getMinimum(3)<<";  max(xtot)= "<<px->getMaximum(3)
        <<";  mean(xtot)= "<<px->getMean(3)<<";  std(xtot)= "<<px->getStd(3)<<endl;

    //Statistic for a single realisation of a vector, not considering other events
    tdsop->addAttribute("Min_x","Min$(x)");
    tdsop->addAttribute("Max_x","Max$(x)");
    tdsop->addAttribute("Mean_x","Sum$(x)/Length$(x)");

    tdsop->scan("x:Min_x:Max_x:Mean_x","","colsize=5 col=2:::6");

    tdsop->saveTuple("update");

    if (!file_name.empty())
    {
        nlohmann::json jfile;
        ofstream output (file_name.data());
        vector<string> names{"x0","x1","x2","xtot"};
        for (size_t n_i{0}; n_i<names.size(); n_i++) {
            jfile[names[n_i]]["min"] = px->getMinimum(n_i);
            jfile[names[n_i]]["max"] = px->getMaximum(n_i);
            jfile[names[n_i]]["mean"] = px->getMean(n_i);
            jfile[names[n_i]]["std"] = px->getStd(n_i);
        }
        output << jfile.dump(4) << endl;
        output.close();
    }
}
