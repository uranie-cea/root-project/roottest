import ROOT
import pytest
import argparse, json

parser = argparse.ArgumentParser('Uniform Distribution Checker')
parser.add_argument('references', help='JSON reference')
parser.add_argument('treename', help='ROOT tree name')
parser.add_argument('filename', help='ROOT filename')
args = parser.parse_args()

with open(args.references) as json_file:
    reference = json.load(json_file)['uniform']

print('reference:', reference)
df = ROOT.RDataFrame(args.treename, args.filename)
tree = {'mean': df.Mean("x").GetValue(), 'stddev': df.StdDev("x").GetValue()}
print('tree:', tree)
assert tree['mean'] == pytest.approx(reference['mean'])
assert tree['stddev'] == pytest.approx(reference['stddev'])
