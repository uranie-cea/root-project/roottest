from URANIE import DataServer
import ROOT
import argparse

parser = argparse.ArgumentParser("CobWeb plot")
parser.add_argument("--figure", help="image filename", default="figure.png")
parser.add_argument("--style", help="macro filename")
args = parser.parse_args()

ROOT.gStyle.SetMarkerSize(0.01)
tdsCobweb = DataServer.TDataServer("tdscobweb", "Database of the cobweb")
tdsCobweb.fileDataRead("cobwebdata.dat") # read data file

Can = ROOT.TCanvas()
# Draw the cobweb plot
tdsCobweb.drawCobWeb("x0:x1:x2:x3:x4:x5:x6:x7:out") # Draw the cobweb

# Get the parallel coordination part to perform modification
para = ROOT.gPad.GetListOfPrimitives().FindObject("__tdspara__0")
# Get the output axis
axis = para.GetVarList().FindObject("out")

# Create a range for 0.97 < out < 1.0 and display it in blue
Range  = ROOT.TParallelCoordRange(axis, 0.97, 1.0)
axis.AddRange(Range)
para.AddSelection("blue")
Range.Draw()

if args.style:
    ROOT.gROOT.LoadMacro(args.style)

Can.SaveAs(args.figure)
