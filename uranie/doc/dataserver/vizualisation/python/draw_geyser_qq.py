from URANIE import DataServer
import ROOT
import argparse

parser = argparse.ArgumentParser("Draw QQ plot")
parser.add_argument("--figure", help="image filename", default="figure.png")
parser.add_argument("--style", help="macro filename")
args = parser.parse_args()

ROOT.gStyle.SetMarkerSize(0.01)
tdsQQ = DataServer.TDataServer("tdsQQ", "Database of the QQ")
tdsQQ.fileDataRead("geyser.dat") # read data file

canvas = ROOT.TCanvas()
tdsQQ.computeStatistic() # to estimate mu and sigma 
tdsQQ.drawQQPlot("x2", "normal(%g,%g)" % (tdsQQ.getAttribute("x2").getMean(), tdsQQ.getAttribute("x2").getStd()), 400)

if args.style:
    ROOT.gROOT.LoadMacro(args.style)

canvas.SaveAs(args.figure)
