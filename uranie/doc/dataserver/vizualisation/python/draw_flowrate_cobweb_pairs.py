from URANIE import DataServer
import ROOT
import argparse

parser = argparse.ArgumentParser("CobWeb drawPairs Plot")
parser.add_argument("--figure", help="image filename", default="figure.png")
parser.add_argument("--style", help="macro filename")
args = parser.parse_args()

ROOT.gStyle.SetMarkerSize(0.01)
tdsCobweb = DataServer.TDataServer("tdscobweb", "Database of the cobweb")
tdsCobweb.fileDataRead("cobwebdata.dat") # read data file

Can = ROOT.TCanvas()

apad = ROOT.TPad("apad","apad",0, 0.03, 1, 1)

if args.style:
    ROOT.gROOT.LoadMacro(args.style)

apad.Draw()
apad.cd()

tdsCobweb.getTuple().SetMarkerStyle(20)
tdsCobweb.getTuple().SetMarkerSize(0.25)
tdsCobweb.drawPairs() # do the drawPairs graph

Can.SaveAs(args.figure)
