from URANIE import DataServer
import ROOT
import argparse

parser = argparse.ArgumentParser("Draw PP plot")
parser.add_argument("--figure", help="image filename", default="figure.png")
parser.add_argument("--style", help="macro filename")
args = parser.parse_args()

ROOT.gStyle.SetMarkerSize(0.01)
tdsPP = DataServer.TDataServer("tdsPP", "Database of the PP")
tdsPP.fileDataRead("geyser.dat") # read data file

Can = ROOT.TCanvas()
tdsPP.computeStatistic() # to estimate mu and sigma
tdsPP.drawPPPlot("x2", "normal(%g,%g)" %(tdsPP.getAttribute("x2").getMean(), tdsPP.getAttribute("x2").getStd()), 400)

if args.style:
    ROOT.gROOT.LoadMacro(args.style)

Can.SaveAs(args.figure)
