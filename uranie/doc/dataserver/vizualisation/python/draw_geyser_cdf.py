from URANIE import DataServer
import ROOT
import argparse

parser = argparse.ArgumentParser("Draw graph of CDF")
parser.add_argument("input", help="input data")
parser.add_argument("--figure", help="image filename", default="figure.png")
parser.add_argument("--style", help="macro filename")
args = parser.parse_args()

ROOT.gStyle.SetMarkerSize(0.01)
tdsGeyser = DataServer.TDataServer("tdsgeyser", "Database of the geyser")
tdsGeyser.fileDataRead(args.input)

Can = ROOT.TCanvas("can","can",1)
tdsGeyser.drawCDF("x2")

if args.style:
    ROOT.gROOT.LoadMacro(args.style)

Can.SaveAs(args.figure)
