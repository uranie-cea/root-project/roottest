from URANIE import DataServer
import ROOT
import argparse

parser = argparse.ArgumentParser("Draw contour of scatterplot")
parser.add_argument("--figure", help="image filename", default="figure.png")
parser.add_argument("--style", help="macro filename")
args = parser.parse_args()

ROOT.gStyle.SetMarkerSize(0.01)
tdsGeyser = DataServer.TDataServer("tdsgeyser", "Geyser database")
tdsGeyser.fileDataRead("geyser.dat")

Can = ROOT.TCanvas("can","can",1)
tdsGeyser.drawScatterplot("x2:x1")

if args.style:
    ROOT.gROOT.LoadMacro(args.style)

Can.SaveAs(args.figure)
