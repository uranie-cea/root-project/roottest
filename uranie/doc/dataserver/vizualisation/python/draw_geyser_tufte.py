from URANIE import DataServer
import ROOT
import argparse

parser = argparse.ArgumentParser("Draw tufte plot")
parser.add_argument("--figure", help="image filename", default="figure.png")
parser.add_argument("--style", help="macro filename")
args = parser.parse_args()

ROOT.gStyle.SetMarkerSize(0.01)
tdsGeyser = DataServer.TDataServer("tdsgeyser", "Database of the geyser")
tdsGeyser.fileDataRead("geyser.dat")

canvas = ROOT.TCanvas("can","can",1)

if args.style:
    ROOT.gROOT.LoadMacro(args.style)

tdsGeyser.drawTufte("x2:x1")

canvas.SaveAs(args.figure)
