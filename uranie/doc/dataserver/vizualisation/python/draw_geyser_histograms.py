from URANIE import DataServer
import ROOT
import argparse

parser = argparse.ArgumentParser("Draw geyser histograms")
parser.add_argument("--figure", help="image filename", default="figure.png")
parser.add_argument("--style", help="macro filename")
args = parser.parse_args()

ROOT.gStyle.SetMarkerSize(0.01)
c2 = ROOT.TCanvas()
apad = ROOT.TPad("apad","apad",0, 0.03, 1, 1)

tdsGeyser = DataServer.TDataServer("tdsgeyser", "Database of the geyser")
tdsGeyser.fileDataRead("geyser.dat")
tdsGeyser.addAttribute("xnorm", "sqrt(x1*x1+x2*x2)")

apad.Draw()

if args.style:
    ROOT.gROOT.LoadMacro(args.style)

apad.Divide(2,2)

apad.cd(1)
tdsGeyser.draw("xnorm","","nclass=root")
apad.cd(2)
tdsGeyser.draw("xnorm","","nclass=sturges")
apad.cd(3)
tdsGeyser.draw("xnorm","","nclass=fd")
apad.cd(4)
tdsGeyser.draw("xnorm","","nclass=scott")

c2.SaveAs(args.figure)
