using namespace URANIE::DataServer;

void code_geyser_pairs() {
    TDataServer * tdsGeyser = new TDataServer("tdsgeyser", "Database of the geyser");
    tdsGeyser->fileDataRead("flowrateUniformDesign.dat");
    tdsGeyser->drawPairs();
}
