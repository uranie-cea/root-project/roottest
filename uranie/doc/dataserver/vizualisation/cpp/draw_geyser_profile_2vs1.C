    using namespace URANIE::DataServer;

    gStyle->SetMarkerSize(0.01);
    TDataServer * tdsGeyser = new TDataServer("tdsgeyser", "Geyser database");

    TCanvas *Can = new TCanvas("can","can",1);

void draw_geyser_profile_2vs1(const string& figure="figure.png", const string& style="") {
    tdsGeyser->fileDataRead("geyser.dat");

    tdsGeyser->drawProfile("x2:x1","","same");

    if (!style.empty())
        gROOT->LoadMacro(style.data());

    Can->SaveAs(figure.data());
}
