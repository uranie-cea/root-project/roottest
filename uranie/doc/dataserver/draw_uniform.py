from URANIE import DataServer, Sampler
import ROOT
import argparse

parser = argparse.ArgumentParser('Uniform Distribution Demo')
parser.add_argument('filename', help='image filename', default='figure.png')
parser.add_argument('--set_seed', help='set sampler seed', action='store_true')
args = parser.parse_args()

ds = DataServer.TDataServer()
ds.addAttribute(DataServer.TUniformDistribution('x'))

sampling = Sampler.TSampling(ds)
if args.set_seed:
    sampling.setSeed(1)
sampling.generateSample()
ds.saveTuple("RECREATE")

canvas = ROOT.TCanvas()
ds.draw('x')
canvas.SaveAs(args.filename)
