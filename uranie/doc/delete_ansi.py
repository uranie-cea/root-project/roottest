import argparse, re

parser = argparse.ArgumentParser("Delete ANSI characters")
parser.add_argument("output", help="Text output filename")
parser.add_argument("input", help="Text input filename")
args = parser.parse_args()

ansi_escape = re.compile(r'\x1B(?:[@-Z\\-_]|\[[0-?]*[ -/]*[@-~])')

with open(args.input, "r") as inputF:
    inputF_data = inputF.read()

outputF_data = ansi_escape.sub('', inputF_data)

with open(args.output, "w") as outputF:
    outputF.write(outputF_data)
