import re
import json
import argparse

parser = argparse.ArgumentParser("Check columns headers of URANIE tables with regex")
parser.add_argument("input", help="Text file name")
parser.add_argument("json_regex", help="JSON file name that contains regex to compare")
args = parser.parse_args()

def title_found(data, title):
    for i, line in enumerate(data):
        if re.search(title, line):
            return i
    return None

with open(args.json_regex, "r") as jsonFile:
    json_regex = json.load(jsonFile)

with open(args.input, "r") as file:
    file_data = file.readlines()
    assert not (len(file_data) == 1 and file_data[0] == "\n"), "ERROR: input file empty"
    for title in json_regex.keys():
        line = title_found(file_data, title)
        assert line != None, "ERROR: key not found in the file"
        assert re.search(json_regex[title], file_data[line+2]), "Regex expression \"{}\" not found in: {}".format(json_regex[title], file_data[line+2])
