from matplotlib import pyplot
import skimage, skimage.io, skimage.transform, skimage.metrics
import numpy
import argparse

parser = argparse.ArgumentParser('Images Comparison')
parser.add_argument('reference', help='figure filename')
parser.add_argument('test', help='figure filename')
parser.add_argument('comparison', help='figure filename')
parser.add_argument('--ssim', type=float, help='similarity tolerance', default=0.82)
parser.add_argument('--mse', type=float, help='mse tolerance', default=0.05)
args = parser.parse_args()

reference = skimage.io.imread(args.reference)
print('reference:', reference.shape)
test = skimage.io.imread(args.test)
print('test:', test.shape)
ratios = numpy.array(test.shape) / numpy.array(reference.shape)
print('ratios:', ratios)
test_resized = skimage.transform.resize(test, reference.shape)

comparison = skimage.util.compare_images(reference, test_resized, method='blend')
pyplot.imshow(comparison)
pyplot.savefig(args.comparison)

reference_gray = skimage.color.rgb2gray(reference)
test_gray = skimage.color.rgb2gray(test_resized)
mse = skimage.metrics.mean_squared_error(reference_gray, test_gray)
print('MSE:', mse)
ssim = skimage.metrics.structural_similarity(reference_gray, test_gray, data_range=test_gray.max() - test_gray.min())
print('SSIM:', ssim)

assert numpy.all(1 <= ratios) and numpy.all(ratios < 1.01)
assert mse < args.mse
assert ssim > args.ssim
