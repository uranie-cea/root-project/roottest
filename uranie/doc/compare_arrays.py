import pandas as pd
from pandas.testing import assert_frame_equal
import numpy as np
import argparse

parser = argparse.ArgumentParser('Compare two arrays')
parser.add_argument('jsonName_1', help='First json file name')
parser.add_argument('jsonName_2', help='Second json file name')
parser.add_argument('--rtol', type=float, help='Relative tolerence value', default=1e-6)
args = parser.parse_args()

def compare_float(df1, df2, rtol, listcols, nofloat_idx):
    # Récupération du contenu des colonnes et conversion en numpy.ndarray
    df1_np = df1[listcols].to_numpy()
    df2_np = df2[listcols].to_numpy()

    # Tableau booléen de comparaison 
    isCloseDF = np.isclose(df1_np, df2_np, rtol=rtol)

    # Sélection des colonnes et lignes qui contiennent un False
    valid_cols_mask = np.invert(np.all(isCloseDF, axis=0))
    valid_rows_mask = np.invert(np.all(isCloseDF, axis=1))

    # Création de la sous-matrice booléenne sub_isCloseDF
    dims = np.ix_(valid_rows_mask, valid_cols_mask)
    sub_isCloseDF = isCloseDF[dims]

    # Extraction des lignes et colonnes qui contiennent des différences entre df1 et df2
    sub_df1 = df1.loc[valid_rows_mask, [listcols[i] for i in range(valid_cols_mask.size) if valid_cols_mask[i]]]
    sub_df2 = df2.loc[valid_rows_mask, [listcols[i] for i in range(valid_cols_mask.size) if valid_cols_mask[i]]]

    # Toutes les valeurs communes entre df1 et df2 deviennent des NaN
    df1_compare = sub_df1.mask(sub_isCloseDF)
    df2_compare = sub_df2.mask(sub_isCloseDF)

    return df1_compare, df2_compare

###### Création du premier DataFrame f1
f1 = pd.read_json(args.jsonName_1)

# Récuparation du nom des colonnes et de leur type
f1_cols = f1.columns
f1_cols_types = f1.dtypes

###### Création du second DataFrame f2
f2 = pd.read_json(args.jsonName_2)

print("\n############## DataFrame 1 ############\n")
print(f1)
print("\n############## DataFrame 2 ############\n")
print(f2)

# Classification des colonnes en fonction de leur type
isFloatIndex = [] # si type(columns) == float
noFloatIndex = [] # si type(columns) != float

for index in f1_cols_types.index:
    if f1_cols_types[index] == float:
        isFloatIndex.append(index)
    else:
        noFloatIndex.append(index)

# Création des DataFrame de comparaison
noFloatCompare = pd.DataFrame()
isFloatCompare = pd.DataFrame()
totalCompare = pd.DataFrame()

# Si type(columns) != float
if not len(noFloatIndex) == 0:
    noFloatCompare = f1[noFloatIndex].compare(f2[noFloatIndex]) # fonction compare appliquée au type(columns) != float

# Si type(columns) == float
if not len(isFloatIndex) == 0:
    f1_compare, f2_compare = compare_float(f1, f2, args.rtol, isFloatIndex, noFloatIndex) # fonction compare_float appliquée au type(columns) == float

# Recréation du rendu final de la fonction DataFrame.compare()
    col_name = ['self', 'other']

    for col in f1_compare.columns:
        multiIdx = pd.MultiIndex.from_arrays([[col,col], col_name])
        one_col_compare = pd.concat([f1_compare[col], f2_compare[col]], axis=1, keys=multiIdx)
        isFloatCompare = pd.concat([isFloatCompare, one_col_compare], axis=1)

# Si type(columns) != float and type(columns) == float
if not noFloatCompare.empty and not isFloatCompare.empty:
    total_compare = pd.concat([noFloatCompare, isFloatCompare], axis=1)
# Si type(columns) != float
elif not noFloatCompare.empty:
    total_compare = noFloatCompare
# Si type(columns) == float
else:
    total_compare = isFloatCompare

print("\n################ DataFrame Comparaison ###############\n")
print(total_compare.to_string())
print("")

assert_frame_equal(f1,f2, check_exact=False, rtol=args.rtol, atol=0.)
