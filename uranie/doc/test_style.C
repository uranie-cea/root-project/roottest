using namespace URANIE;

namespace TestStyle {
    TDatime datetime;
    TString text = TString(datetime.AsSQLString(), 10) + TString(" - ") +
        DataServer::TDataServer::getUranieVersion();
    TLatex date;
}

gROOT->SetBatch(true);
gStyle->SetPalette(1);

//Legend
gStyle->SetLegendBorderSize(0);
gStyle->SetFillStyle(0);

void set_date() {
    gPad->GetCanvas()->cd();
    TestStyle::date.SetTextSize(gStyle->GetAttDate()->GetTextSize());
    TestStyle::date.SetNDC();
    TestStyle::date.DrawText(0.01, 0.01, TestStyle::text.Data());
}

void test_style(){}
