import re
import argparse

parser = argparse.ArgumentParser("Check if a string/regex is contained in an input file")
parser.add_argument("input", help="Text file name")
parser.add_argument("choice", help="type of pattern : regex or string", choices=["string", "regex"])
parser.add_argument("pattern", help="String/Regex target to locate in \"input\"")
args = parser.parse_args()

with open(args.input, "r") as file:
    file_data = file.read()

if args.choice == "string":
    pattern = re.escape(args.pattern)
elif args.choice == "regex":
    pattern = args.pattern
    
assert re.search(pattern, file_data), "ERROR: {} is not contained in {}".format(args.pattern, args.input)
