# Load the ROOT module
import ROOT
import argparse

parser = argparse.ArgumentParser('Normal Distribution PyROOT Histogram')
parser.add_argument('--figure', help='image filename', default='figure.png')
parser.add_argument('--style', help='macro filename')
args = parser.parse_args()

ROOT.gStyle.SetMarkerSize(0.01)
# Create a new data server object
tds = ROOT.URANIE.DataServer.TDataServer("myTDS", "DataServer for python example")

# Add an attribute to the data server
tds.addAttribute(ROOT.URANIE.DataServer.TNormalDistribution("x", 0.0, 1.0))

# Create a sampler object
sampler = ROOT.URANIE.Sampler.TSampling(tds, "lhs", 1000)

# Generate data
sampler.generateSample()

canvas = ROOT.TCanvas()
# Display the histogram of attribute x
tds.draw("x")

if args.style:
    ROOT.gROOT.LoadMacro(args.style)

canvas.SaveAs(args.figure)
