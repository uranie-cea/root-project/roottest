import ROOT, pandas
import argparse

parser = argparse.ArgumentParser("Convert to JSON")
parser.add_argument("output", help="JSON output filename")
parser.add_argument("filename", help="ROOT binary filename")
parser.add_argument("treename", help="ROOT binary treename")
parser.add_argument("--sort", help="Sort by column option", default="")
parser.add_argument("--tostring", help="ROOT object columns to convert into string", nargs="+", default=[])
args = parser.parse_args()

ROOT.gInterpreter.Declare(r"""
string convert_to_string(const ROOT::VecOps::RVec<Char_t>& name) {
    string a_string;
    for(char character: name) {
        if(character == '\0')
            break;
        a_string.push_back(character);
    }
    return a_string;
};
""")

rdf = ROOT.RDataFrame(args.treename, args.filename)

if args.tostring:
    for index, column in enumerate(args.tostring):
        rdf = rdf.Redefine(column, f"convert_to_string({column})")

df = pandas.DataFrame(rdf.AsNumpy())
if args.sort:
    df = df.sort_values(by=args.sort, ignore_index=True)

df.to_json(args.output, indent=4, 
    default_handler=lambda vector: vector.decode("utf-8").replace("\x00", "") if hasattr(vector, "decode") else list(vector))
