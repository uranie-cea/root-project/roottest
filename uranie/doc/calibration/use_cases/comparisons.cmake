ROOTTEST_ADD_TEST(calibrationRejectionABCFlowrate1D_substitution MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/substitution.py
    OPTS "calibrationRejectionABCFlowrate1D_clean.log" "regex" "File\\[(.*)souRCE(.*)souRCE(.*)\\] Line" "File\[\$\{SOURCEDIR\}\\2souRCE\\3\] Line"
    DEPENDS calibrationRejectionABCFlowrate1D)

################## COMPARAISON VALEURS NUMERIQUES SORTIE CONSOLE ##################

find_python_module(jsoncomparison OPTIONAL)
if(PY_JSONCOMPARISON_FOUND)
    ROOTTEST_ADD_TEST(calibrationLinBayesFlowrate1D_matrix MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_json_console.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/calibration/use_cases/calibrationLinBayesFlowrate1D.json"
        "calibrationLinBayesFlowrate1D.json"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/linBayes
        DEPENDS calibrationLinBayesFlowrate1D)

    ROOTTEST_ADD_TEST(calibrationRejectionABCFlowrate1D_console MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_json_console.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/calibration/use_cases/calibrationRejectionABCFlowrate1D.json"
        "calibrationRejectionABCFlowrate1D.json"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/rejection_abc
        DEPENDS calibrationRejectionABCFlowrate1D)

    ROOTTEST_ADD_TEST(calibrationMetropHastingFlowrate1D_console MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_json_console.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/calibration/use_cases/calibrationMetropHastingFlowrate1D.json"
        "calibrationMetropHastingFlowrate1D.json"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/metrop_hasting
        DEPENDS calibrationMetropHastingFlowrate1D)

    ROOTTEST_ADD_TEST(calibrationMetropHastingLinReg_console MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_json_console.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/calibration/use_cases/calibrationMetropHastingLinReg.json"
        "calibrationMetropHastingLinReg.json"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/metrop_hasting_linReg
        DEPENDS calibrationMetropHastingLinReg)

    ROOTTEST_ADD_TEST(calibrationMinimisationFlowrate2DVizir_console MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_json_console.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/calibration/use_cases/calibrationMinimisationFlowrate2DVizir_hu_hl.json"
        "calibrationMinimisationFlowrate2DVizir_hu_hl.json"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/minimisation_vizir
        DEPENDS calibrationMinimisationFlowrate2DVizir)

    ROOTTEST_ADD_TEST(calibrationMinimisationFlowrate2DVizir_matrix MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_json_console.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/calibration/use_cases/calibrationMinimisationFlowrate2DVizir_mat.json"
        "calibrationMinimisationFlowrate2DVizir_mat.json"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/minimisation_vizir
        DEPENDS calibrationMinimisationFlowrate2DVizir)
endif()

################## COMPARAISON TABLEAUX SORTIE CONSOLE ##################

find_python_module(pandas OPTIONAL)
if(PY_PANDAS_FOUND)
    ROOTTEST_ADD_TEST(calibrationMinimisationFlowrate1D_array MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_arrays.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/calibration/use_cases/calibrationMinimisationFlowrate1D.json"
        "calibrationMinimisationFlowrate1D.json"
        PRECMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/convert_tree_json.py
        "calibrationMinimisationFlowrate1D.json" "_dataserver_.root" "tdsPar__data__tree__"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/minimisation
        DEPENDS calibrationMinimisationFlowrate1D)

    ROOTTEST_ADD_TEST(calibrationMinimisationFlowrate2DVizir_array MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_arrays.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/calibration/use_cases/calibrationMinimisationFlowrate2DVizir.json"
        "calibrationMinimisationFlowrate2DVizir.json"
        PRECMD ${Python3_EXECUTABLE} ${CMAKE_SOURCE_DIR}/uranie/doc/convert_tree_json.py
        "calibrationMinimisationFlowrate2DVizir.json" "_dataserver_.root" "tdsPar__data__tree__"
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/minimisation_vizir
        DEPENDS calibrationMinimisationFlowrate2DVizir)
endif()

################## COMPARAISON IMAGES ##################
find_python_module(skimage OPTIONAL)
if(PY_SKIMAGE_FOUND)

    list(APPEND CALIB_LIST_FIG "calibrationLinBayesFlowrate1D" "calibrationRejectionABCFlowrate1D" 
        "calibrationMetropHastingFlowrate1D" "calibrationMetropHastingLinReg" 
        "calibrationMinimisationFlowrate2DVizir")

    list(APPEND CALIB_LIST_FOLDER "linBayes" "rejection_abc" "metrop_hasting" "metrop_hasting_linReg" 
        "minimisation_vizir")

    cmake_language(CALL ${SIMILARITY_FUNCTION_NAME} "calibrationMinimisationFlowrate1D")

    ROOTTEST_ADD_TEST(calibrationMinimisationFlowrate1D_figure MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_images.py
        OPTS "${CMAKE_SOURCE_DIR}/uranie/references/calibration/use_cases/calibrationMinimisationFlowrate1D.png"
            "calibrationMinimisationFlowrate1D.png"
            "calibrationMinimisationFlowrate1D_diff.png"
            "--ssim" ${SIMILARITY}
        WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/minimisation
        DEPENDS calibrationMinimisationFlowrate1D)

    foreach(FOLDER_NAME FIG_BASE IN ZIP_LISTS CALIB_LIST_FOLDER CALIB_LIST_FIG)
        set(CALIB_LIST_SUFFIX)

        if(${FIG_BASE} STREQUAL "calibrationMetropHastingFlowrate1D")
            list(APPEND CALIB_LIST_SUFFIX "Par" "Res" "Tr" "Tr_split")
        elseif(${FIG_BASE} STREQUAL "calibrationMetropHastingLinReg")
            list(APPEND CALIB_LIST_SUFFIX "Par" "Res" "Tr" "Acc")
        else()
            list(APPEND CALIB_LIST_SUFFIX "Res" "Par")
        endif()

        foreach(SUFFIX_NAME IN LISTS CALIB_LIST_SUFFIX)
            set(FIG_NAME ${FIG_BASE}_${SUFFIX_NAME})

            cmake_language(CALL ${SIMILARITY_FUNCTION_NAME} ${FIG_NAME})

            ROOTTEST_ADD_TEST(${FIG_NAME}_figure MACRO ${CMAKE_SOURCE_DIR}/uranie/doc/compare_images.py
                OPTS "${CMAKE_SOURCE_DIR}/uranie/references/calibration/use_cases/${FIG_NAME}.png"
                    "${FIG_NAME}.png"
                    "${FIG_NAME}_diff.png"
                    "--ssim" ${SIMILARITY}
                WORKING_DIR ${CMAKE_CURRENT_BINARY_DIR}/${FOLDER_NAME}
                DEPENDS ${FIG_BASE})
        endforeach()
    endforeach()
endif()
