{
    using namespace URANIE::Calibration;
    using namespace URANIE::Relauncher;
    using namespace URANIE::DataServer;

    string figure_base = "figure";
    if (gSystem->Getenv("FIGURE_BASE") != nullptr)
        figure_base = gSystem->Getenv("FIGURE_BASE");

    string style = "";
    if (gSystem->Getenv("STYLE") != nullptr)
        style = gSystem->Getenv("STYLE");

    string file_name = "";
    if (gSystem->Getenv("FILE_NAME") != nullptr)
        file_name = gSystem->Getenv("FILE_NAME");

    string seed_str = "0";
    if (gSystem->Getenv("SEED") != nullptr)
        seed_str = gSystem->Getenv("SEED");
    int seed = stoi(seed_str);

    // Load the function flowrateCalib1D
    gROOT->LoadMacro("UserFunctions.C");

    // Input reference file   
    TString ExpData="Ex2DoE_n100_sd1.75.dat";

    // define the reference
    TDataServer *tdsRef = new TDataServer("tdsRef","doe_exp_Re_Pr");
    tdsRef->fileDataRead(ExpData.Data());

    // define the parameters
    TDataServer *tdsPar = new TDataServer("tdsPar","pouet");
    tdsPar->addAttribute( new TUniformDistribution("hl", 700.0, 760.0) );

    // Create the output attribute
    TAttribute *out = new TAttribute("out");

    // Create interface to assessors
    TCIntEval *Reg = new TCIntEval("flowrateModelnoH");
    Reg->addInput(tdsRef->getAttribute("rw"));
    Reg->addInput(tdsRef->getAttribute("l"));    
    Reg->addOutput(new TAttribute("H") );    
    TSequentialRun *runnoH = new TSequentialRun(Reg);    
    runnoH->startSlave();
    if(runnoH->onMaster())
    {
        TLauncher2 l(tdsRef, runnoH);
        l.solverLoop();
        runnoH->stopSlave();
    }

    // Create interface to assessors
    TCIntEval *Model = new TCIntEval("flowrateCalib1D");
    Model->addInput(tdsPar->getAttribute("hl"));
    Model->addInput(tdsRef->getAttribute("rw"));
    Model->addInput(tdsRef->getAttribute("l"));
    Model->addOutput(out);

    // Set the runner
    TSequentialRun *runner = new TSequentialRun(Model);    

    // Set the covariance matrix of the input reference    
    double sd=tdsRef->getValue("sd_eps",0);
    TMatrixD mat(100,100);
    for(unsigned int ival=0; ival<tdsRef->getNPatterns(); ival++)
        mat(ival,ival)=(sd*sd);

    // Set the calibration object
    TLinearBayesian *cal = new TLinearBayesian(tdsPar,runner,1,"");
    cal->setDistanceAndReference("Mahalanobis",tdsRef,"rw:l","Qexp");
    cal->setObservationCovarianceMatrix(mat);
    cal->setRegressorName("H");
    if (seed > 0)
        cal->setSeed(seed);
    cal->setParameterTransformationFunction(transf);
    cal->estimateParameters();

    // Draw the residuals
    TCanvas *canRes = new TCanvas("CanRes","CanRes",1200,800);
    TPad *padRes = new TPad("padRes","padRes",0, 0.03, 1, 1);  padRes->Draw();  
    if (!style.empty())
        gROOT->LoadMacro(style.data());
    padRes->cd();
    cal->drawResidues("Residual title","*","","nonewcanvas");

    // Draw the parameters
    TCanvas *canPar = new TCanvas("CanPar","CanPar",1200,800);
    TPad *padPar = new TPad("padPar","padPar",0, 0.03, 1, 1);  padPar->Draw();  
    if (!style.empty())
        gROOT->LoadMacro(style.data());
    padPar->cd();
    cal->drawParameters("Parameter title","*","","nonewcanvas,transformed");

    if (!file_name.empty())
    {
        TMatrixD mat_param{cal->getTransfParameterValueMatrix()};
        auto mat_json = TBufferJSON::ToJSON(&mat_param);

        ofstream out_mat (file_name);
        out_mat << mat_json << endl;
        out_mat.close();
    }

    string fig_res{figure_base+"_Res.png"};
    string fig_par{figure_base+"_Par.png"};

    canRes->SaveAs(fig_res.data());
    canPar->SaveAs(fig_par.data());
}
