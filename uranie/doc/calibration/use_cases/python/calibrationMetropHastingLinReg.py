"""
Example of calibration using MH algo on linear model
"""
from URANIE import DataServer, Relauncher, Calibration
import ROOT
import argparse, json

parser = argparse.ArgumentParser(description="Use Metropolis Hasting on simple flowrate 1D model")
parser.add_argument("--figure_base", help="image filename", default="figure")
parser.add_argument("--style", help="macro filename", default="")
parser.add_argument("--json", help="json filename", default="")
parser.add_argument("--seed", help="seed number", default=0, type=int)
args = parser.parse_args()

# Load the function flowrateCalib1D
ROOT.gROOT.LoadMacro("UserFunctions.C")

# Input reference file
ExpData = "linReg_Database.dat"

# define the reference
tdsRef = DataServer.TDataServer("tdsRef", "doe_exp_Re_Pr")
tdsRef.fileDataRead(ExpData)

# Define the uncertainty model wih a guess
sd_exp = 0.2
tdsRef.addAttribute("wei_exp", "1./("+str(sd_exp)+"*"+str(sd_exp)+")")

# Define the parameters
tdsPar = DataServer.TDataServer("tdsPar", "poute")
binf_sea = -2.0
bsup_sea = 2.0
tdsPar.addAttribute(DataServer.TUniformDistribution("t0", binf_sea, bsup_sea))
tdsPar.addAttribute(DataServer.TUniformDistribution("t1", binf_sea, bsup_sea))

# Create the output attribute
out = DataServer.TAttribute("out")

# Create interface to assessors
myeval = Relauncher.TCIntEval("Toy")
myeval.addInput(tdsRef.getAttribute("x"))
myeval.addInput(tdsPar.getAttribute("t0"))
myeval.addInput(tdsPar.getAttribute("t1"))
myeval.addOutput(out)

# Set the runner
run = Relauncher.TSequentialRun(myeval)

# Set the calibration object
# Providing wild guess for value and variation range
inval = ROOT.std.vector['double']([0.8, -0.6])
std = ROOT.std.vector['double']([0.4, 0.5])
ns = 12000
cal = Calibration.TMetropHasting(tdsPar, run, ns, "")
cal.setDistanceAndReference("weightedLS", tdsRef, "x", "yExp", "wei_exp")
cal.setNbDump(4000)
if args.seed > 0:
    cal.setSeed(args.seed)
cal.setInitialisation(inval, std)
cal.estimateParameters()

# Quality assessment :  Draw the trace the MCMC
canTr = ROOT.TCanvas("CanTr", "CanTr", 1200, 800)
padTr = ROOT.TPad("padTr", "padTr", 0, 0.03, 1, 1)
padTr.Draw()
padTr.cd()
cal.drawTrace("Trace title", "*", "", "nonewcanvas")

canTr.SaveAs(args.figure_base+"_Tr.png")

# Quality assessment :  Draw the trace the MCMC
canAcc = ROOT.TCanvas("CanAcc", "CanAcc", 1200, 800)
padAcc = ROOT.TPad("padAcc", "padAcc", 0, 0.03, 1, 1)
padAcc.Draw()
padAcc.cd()
cal.drawAcceptationRatio("AcceptRatio title", "*", "", "nonewcanvas")

canAcc.SaveAs(args.figure_base+"_Acc.png")

burn = 100  # Remove first 100 elements
# Compute the auto-correlation
lag = ROOT.std.vector('int')([1, 3, 6, 10, 20])
autoCorr = ROOT.std.vector['double'](range(lag.size()))
cal.getAutoCorrelation(lag, autoCorr, burn)

print("Autocorrelation are:")
for il in range(lag.size()):
    output = "*** for lag="+str(lag[il])+": "
    for ip in range(cal.getNPar()):
        output += "%1.6g" % (autoCorr[ip*lag.size()+il])
        output += "; "
    print(output)

mylag = 6
# Define a selection based on burn-in and lag
mycut = "(%s > %d) && ((%s %% %d) == 0)" % (tdsPar.getIteratorName(), burn,
                                            tdsPar.getIteratorName(), mylag)
# Draw the parameters
canPar = ROOT.TCanvas("CanPar", "CanPar", 1200, 800)
padPar = ROOT.TPad("padPar", "padPar", 0, 0.03, 1, 1)
padPar.Draw()
padPar.cd()
cal.drawParameters("Parameter title", "*", mycut, "nonewcanvas")

canPar.SaveAs(args.figure_base+"_Par.png")

# Draw the residuals
canRes = ROOT.TCanvas("CanRes", "CanRes", 1200, 800)
padRes = ROOT.TPad("padRes", "padRes", 0, 0.03, 1, 1)
padRes.Draw()
if args.style:
    ROOT.gROOT.LoadMacro(args.style)
padRes.cd()
cal.drawResidues("Residual title", "*", "", "nonewcanvas")

canRes.SaveAs(args.figure_base+"_Res.png")

if args.json:
    json_dict = {}

    for il in range(lag.size()):
        autoCorr_json = []
        for ip in range(cal.getNPar()):
            autoCorr_json.append(autoCorr[ip*lag.size()+il])
        json_dict["lag_"+str(lag[il])] = autoCorr_json

    tdsPar.computeStatistic()

    json_dict["t0_mean"] = tdsPar.getAttribute("t0").getMean()
    json_dict["t1_mean"] = tdsPar.getAttribute("t1").getMean()

    with open(args.json, "w") as file:
        json.dump(json_dict, file, indent=4)
