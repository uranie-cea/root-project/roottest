"""
Example of calibration using Rejection ABC approach on flowrate 1D
"""
from URANIE import DataServer, Calibration, Relauncher
import ROOT
import argparse, json

parser = argparse.ArgumentParser(description="Calibration using rejection ABC approach")
parser.add_argument("--figure_base", help="image filename", default="figure")
parser.add_argument("--style", help="macro filename", default="")
parser.add_argument("--json", help="json filename", default="")
parser.add_argument("--seed", help="seed number", default=0, type=int)
args = parser.parse_args()

# Load the function flowrateCalib1D
ROOT.gROOT.LoadMacro("UserFunctions.C")

# Input reference file
ExpData = "Ex2DoE_n100_sd1.75.dat"

# define the reference
tdsRef = DataServer.TDataServer("tdsRef", "doe_exp_Re_Pr")
tdsRef.fileDataRead(ExpData)

# define the parameters
tdsPar = DataServer.TDataServer("tdsPar", "pouet")
tdsPar.addAttribute(DataServer.TUniformDistribution("hl", 700.0, 760.0))

# Create the output attribute
out = DataServer.TAttribute("out")

# Create interface to assessors
Model = Relauncher.TCIntEval("flowrateCalib1D")
Model.addInput(tdsPar.getAttribute("hl"))
Model.addInput(tdsRef.getAttribute("rw"))
Model.addInput(tdsRef.getAttribute("l"))
Model.addOutput(out)

# Set the runner
runner = Relauncher.TSequentialRun(Model)

# Set the calibration object
nABC = 100
eps = 0.05
cal = Calibration.TRejectionABC(tdsPar, runner, nABC, "")
cal.setDistanceAndReference("LS", tdsRef, "rw:l", "Qexp")
cal.setGaussianNoise("sd_eps")
cal.setPercentile(eps)
if args.seed:
    cal.setSeed(args.seed)
cal.estimateParameters()

# Draw the parameters
canPar = ROOT.TCanvas("CanPar", "CanPar", 1200, 800)
padPar = ROOT.TPad("padPar", "padPar", 0, 0.03, 1, 1)
padPar.Draw()
padPar.cd()
cal.drawParameters("Parameter title", "*", "", "nonewcanvas")

canPar.SaveAs(args.figure_base+"_Par.png")

# Draw the residuals
canRes = ROOT.TCanvas("CanRes", "CanRes", 1200, 800)
padRes = ROOT.TPad("padRes", "padRes", 0, 0.03, 1, 1)
padRes.Draw()
if args.style:
    ROOT.gROOT.LoadMacro(args.style)
padRes.cd()
cal.drawResidues("Residual title", "*", "", "nonewcanvas")

# Compute statistics
tdsPar.computeStatistic()
print("The mean of hl is %3.8g" % (tdsPar.getAttribute("hl").getMean()))
print("The std of hl is %3.8g" % (tdsPar.getAttribute("hl").getStd()))

canRes.SaveAs(args.figure_base+"_Res.png")

if args.json:
    json_dict = {"hl_mean":tdsPar.getAttribute("hl").getMean(), 
        "hl_std":tdsPar.getAttribute("hl").getStd()}

    with open(args.json, "w") as file:
        json.dump(json_dict, file, indent=4)
