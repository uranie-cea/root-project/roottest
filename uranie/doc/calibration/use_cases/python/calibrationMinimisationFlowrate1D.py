"""
Example of calibration using minimisation approach on flowrate 1D
"""
from URANIE import DataServer, Relauncher, Reoptimizer, Calibration
import ROOT
import argparse

parser = argparse.ArgumentParser(description="Calibration using minimisation approach")
parser.add_argument("--figure", help="image filename", default="figure.png")
parser.add_argument("--style", help="macro filename", default="")
parser.add_argument("--seed", help="seed number", default=0, type=int)
args = parser.parse_args()

# Load the function flowrateCalib1D
ROOT.gROOT.LoadMacro("UserFunctions.C")

# Input reference file
ExpData = "Ex2DoE_n100_sd1.75.dat"

# define the reference
tdsRef = DataServer.TDataServer("tdsRef", "doe_exp_Re_Pr")
tdsRef.fileDataRead(ExpData)

# define the parameters
tdsPar = DataServer.TDataServer("tdsPar", "pouet")
tdsPar.addAttribute(DataServer.TAttribute("hl", 700.0, 760.0))
tdsPar.getAttribute("hl").setDefaultValue(728.0)

# Create the output attribute
out = DataServer.TAttribute("out")

# Create interface to assessors
Model = Relauncher.TCIntEval("flowrateCalib1D")
Model.addInput(tdsPar.getAttribute("hl"))
Model.addInput(tdsRef.getAttribute("rw"))
Model.addInput(tdsRef.getAttribute("l"))
Model.addOutput(out)

# Set the runner
runner = Relauncher.TSequentialRun(Model)

# Set the calibration object
cal = Calibration.TMinimisation(tdsPar, runner, 1)
cal.setDistanceAndReference("LS", tdsRef, "rw:l", "Qexp")
solv = Reoptimizer.TNloptSubplexe()
cal.setOptimProperties(solv)
if args.seed > 0:
    cal.setSeed(args.seed)
cal.estimateParameters()

# Draw the residuals
canRes = ROOT.TCanvas("CanRes", "CanRes", 1200, 800)
padRes = ROOT.TPad("padRes", "padRes", 0, 0.03, 1, 1)
padRes.Draw()
if args.style:
    ROOT.gROOT.LoadMacro(args.style)
padRes.cd()
cal.drawResidues("Residual title", "*", "", "nonewcanvas")

tdsPar.saveTuple("update")

canRes.SaveAs(args.figure)
