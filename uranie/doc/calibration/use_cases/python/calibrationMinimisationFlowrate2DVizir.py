"""
Example of calibration through minimisation with Vizir in Flowrate 2D
"""
from URANIE import DataServer, Relauncher, Reoptimizer, Calibration
import ROOT
import argparse, json

parser = argparse.ArgumentParser(description="Calibration through minimisation with Vizir")
parser.add_argument("--figure_base", help="image filename", default="figure")
parser.add_argument("--style", help="macro filename", default="")
parser.add_argument("--basename", help="json basename", default="")
parser.add_argument("--fixed-seed", dest="seed", help="fix the seed", action="store_true")
args = parser.parse_args()

# Load the function flowrateCalib2DVizir
ROOT.gROOT.LoadMacro("UserFunctions.C")

# Input reference file
ExpData = "Ex2DoE_n100_sd1.75.dat"

# define the reference
tdsRef = DataServer.TDataServer("tdsRef", "doe_exp_Re_Pr")
tdsRef.fileDataRead(ExpData)

# define the parameters
tdsPar = DataServer.TDataServer("tdsPar", "pouet")
tdsPar.addAttribute(DataServer.TAttribute("hu", 1020.0, 1080.0))
tdsPar.addAttribute(DataServer.TAttribute("hl", 720.0, 780.0))

# Create the output attribute
out = DataServer.TAttribute("out")

# Create interface to assessors
Model = Relauncher.TCIntEval("flowrateCalib2D")
Model.addInput(tdsPar.getAttribute("hu"))
Model.addInput(tdsPar.getAttribute("hl"))
Model.addInput(tdsRef.getAttribute("rw"))
Model.addInput(tdsRef.getAttribute("l"))
Model.addOutput(out)

# Set the runner
runner = Relauncher.TSequentialRun(Model)

# Set the calibration object
cal = Calibration.TMinimisation(tdsPar, runner, 1)
cal.setDistanceAndReference("relativeLS", tdsRef, "rw:l", "Qexp")
# Set optimisaiton properties
solv = Reoptimizer.TVizirGenetic()
solv.setSize(24, 15000, 100)
cal.setOptimProperties(solv)
if args.seed:
    cal.getOptimMaster().setFixedSeed()
# cal.getOptimMaster().setTolerance(1e-6)
cal.estimateParameters()

# Draw the Residual
canRes = ROOT.TCanvas("CanRes", "CanRes", 1200, 800)
padRes = ROOT.TPad("padRes", "padRes", 0, 0.03, 1, 1)
padRes.Draw()
if args.style:
    ROOT.gROOT.LoadMacro(args.style)
padRes.cd()
cal.drawResidues("Residual title", "*", "", "nonewcanvas")

canRes.SaveAs(args.figure_base+"_Res.png")

# Draw the box plot of parameters
canPar = ROOT.TCanvas("CanPar", "CanPar", 1200, 800)
tdsPar.getTuple().SetMarkerStyle(20)
tdsPar.getTuple().SetMarkerSize(0.8)
tdsPar.Draw("hu:hl")

# Look at the correlation and statistic
tdsPar.computeStatistic("hu:hl")
corr = tdsPar.computeCorrelationMatrix("hu:hl")
corr.Print()

print("hl is %3.8g +- %3.8g " % (tdsPar.getAttribute("hl").getMean(),
                                 tdsPar.getAttribute("hl").getStd()))
print("hu is %3.8g +- %3.8g " % (tdsPar.getAttribute("hu").getMean(),
                                 tdsPar.getAttribute("hu").getStd()))

tdsPar.saveTuple("update")

if args.basename:
    json_dict = {"hl_mean":tdsPar.getAttribute("hl").getMean(), "hl_std":tdsPar.getAttribute("hl").getStd(), 
        "hu_mean":tdsPar.getAttribute("hu").getMean(), "hu_std":tdsPar.getAttribute("hu").getStd()}

    with open(args.basename+"_hu_hl.json", "w") as file:
        json.dump(json_dict, file, indent=4)

    mat_json = ROOT.TBufferJSON.ToJSON(corr)

    with open(args.basename+"_mat.json", "w") as file:
        file.write(str(mat_json))

canPar.SaveAs(args.figure_base+"_Par.png")
