"""
Example of Metropolis Hasting usage on simple flowrate 1D model
"""
from URANIE import DataServer, Relauncher, Calibration
import ROOT
import argparse, json

parser = argparse.ArgumentParser(description="Use Metropolis Hasting on simple flowrate 1D model")
parser.add_argument("--figure_base", help="image filename", default="figure")
parser.add_argument("--style", help="macro filename", default="")
parser.add_argument("--json", help="json filename", default="")
parser.add_argument("--seed", help="seed number", default=0, type=int)
args = parser.parse_args()

# Load the function flowrateCalib1D
ROOT.gROOT.LoadMacro("UserFunctions.C")

# Input reference file$
ExpData = "Ex2DoE_n100_sd1.75.dat"

# define the reference
tdsRef = DataServer.TDataServer("tdsRef", "doe_exp_Re_Pr")
tdsRef.fileDataRead(ExpData)
tdsRef.addAttribute("wei_exp", "1./(sd_eps*sd_eps)")

# define the parameters
tdsPar = DataServer.TDataServer("tdsPar", "pouet")
tdsPar.addAttribute(DataServer.TUniformDistribution("hl", 700.0, 760.0))

# Create the output attribute
out = DataServer.TAttribute("out")

# Create interface to assessors
Model = Relauncher.TCIntEval("flowrateCalib1D")
Model.addInput(tdsPar.getAttribute("hl"))
Model.addInput(tdsRef.getAttribute("rw"))
Model.addInput(tdsRef.getAttribute("l"))
Model.addOutput(out)

# Set the runner
runner = Relauncher.TSequentialRun(Model)

# Set the calibration object
cal = Calibration.TMetropHasting(tdsPar, runner, 2000, "")
cal.setDistanceAndReference("weightedLS", tdsRef, "rw:l", "Qexp", "wei_exp")
cal.setNbDump(400)
if args.seed > 0:
    cal.setSeed(args.seed)
cal.setAcceptationRatioRange(0.4, 0.45)
cal.estimateParameters()

# Quality assessment :  Draw the trace the MCMC
canTr = ROOT.TCanvas("CanTr", "CanTr", 1200, 800)
padTr = ROOT.TPad("padTr", "padTr", 0, 0.03, 1, 1)
padTr.Draw()
padTr.cd()
cal.drawTrace("Trace title", "*", "", "nonewcanvas")

canTr.SaveAs(args.figure_base+"_Tr.png")

# Draw the parameters
canPar = ROOT.TCanvas("CanPar", "CanPar", 1200, 800)
padPar = ROOT.TPad("padPar", "padPar", 0, 0.03, 1, 1)
padPar.Draw()
padPar.cd()
cal.drawParameters("Parameter title", "*", "", "nonewcanvas")

canPar.SaveAs(args.figure_base+"_Par.png")

# Draw the residuals
canRes = ROOT.TCanvas("CanRes", "CanRes", 1200, 800)
padRes = ROOT.TPad("padRes", "padRes", 0, 0.03, 1, 1)
padRes.Draw()
if args.style:
    ROOT.gROOT.LoadMacro(args.style)    
padRes.cd()
cal.drawResidues("Residual title", "*", "", "nonewcanvas")

# Compute the auto-correlation
burn = 20  # Remove first 20 elements
lag = ROOT.std.vector('int')([1, 5, 10, 20])
autoCorr = ROOT.std.vector['double'](range(lag.size()))
cal.getAutoCorrelation(lag, autoCorr, burn)

print("Autocorrelation are " + str(autoCorr.size()) + ":")
for il in range(lag.size()):
    output = "*** for lag=" + str(lag[il]) + ": "
    for ip in range(cal.getNPar()):
        output += "%1.6g" % (autoCorr[ip*lag.size()+il])
    print(output)

if args.json:
    json_dict = {"autoCorr_size":autoCorr.size()}

    for il in range(lag.size()):
        autoCorr_json = []
        for ip in range(cal.getNPar()):
            autoCorr_json.append(autoCorr[ip*lag.size()+il])
        json_dict["lag_"+str(lag[il])] = autoCorr_json

    tdsPar.computeStatistic()
    json_dict["hl_mean"] = tdsPar.getAttribute("hl").getMean()

    with open(args.json, "w") as file:
        json.dump(json_dict, file, indent=4)

canRes.SaveAs(args.figure_base+"_Res.png")

canTr2 = ROOT.TCanvas("CanTr2","CanTr2",1200,800)
adTr = ROOT.TPad("adTr","padTr",0, 0.03, 1, 1)
adTr.Draw()
if args.style:
    ROOT.gROOT.LoadMacro(args.style)    
adTr.cd()
adTr.Divide(1,2)
adTr.cd(1)
cal.drawTrace("Looking for burn-in","*", tdsPar.getIteratorName()+str("< 100"), "nonewcanvas")
adTr.cd(2)
cal.drawTrace("Looking for burn-in","*", tdsPar.getIteratorName()+str("> 100"), "nonewcanvas")

canTr2.SaveAs(args.figure_base+"_Tr_split.png")
