"""
Example of linear bayesian calibration with simple 1D flowrate model
"""
from URANIE import DataServer, Relauncher, Calibration
import ROOT
import argparse, json

parser = argparse.ArgumentParser(description="Linear bayesian calibration")
parser.add_argument("--figure_base", help="image filename", default="figure")
parser.add_argument("--style", help="macro filename", default="")
parser.add_argument("--json", help="json filename", default="")
parser.add_argument("--seed", help="seed number", default=0, type=int)
args = parser.parse_args()

# Load the function flowrateCalib1D
ROOT.gROOT.LoadMacro("UserFunctions.C")

# Input reference file
ExpData = "Ex2DoE_n100_sd1.75.dat"

# define the reference
tdsRef = DataServer.TDataServer("tdsRef", "doe_exp_Re_Pr")
tdsRef.fileDataRead(ExpData)

# define the parameters
tdsPar = DataServer.TDataServer("tdsPar", "pouet")
tdsPar.addAttribute(DataServer.TUniformDistribution("hl", 700.0, 760.0))

# Create the output attribute
out = DataServer.TAttribute("out")

# Create interface to assessors
Reg = Relauncher.TCIntEval("flowrateModelnoH")
Reg.addInput(tdsRef.getAttribute("rw"))
Reg.addInput(tdsRef.getAttribute("l"))
Reg.addOutput(DataServer.TAttribute("H"))
runnoH = Relauncher.TSequentialRun(Reg)
runnoH.startSlave()
if runnoH.onMaster():
    launch = Relauncher.TLauncher2(tdsRef, runnoH)
    launch.solverLoop()
    runnoH.stopSlave()

# Create interface to assessors
Model = Relauncher.TCIntEval("flowrateCalib1D")
Model.addInput(tdsPar.getAttribute("hl"))
Model.addInput(tdsRef.getAttribute("rw"))
Model.addInput(tdsRef.getAttribute("l"))
Model.addOutput(out)

# Set the runner
runner = Relauncher.TSequentialRun(Model)

# Set the covariance matrix of the input reference
sd = tdsRef.getValue("sd_eps", 0)
mat = ROOT.TMatrixD(100, 100)
for ival in range(tdsRef.getNPatterns()):
    mat[ival][ival] = (sd*sd)

# Set the calibration object
cal = Calibration.TLinearBayesian(tdsPar, runner, 1, "")
cal.setDistanceAndReference("Mahalanobis", tdsRef, "rw:l", "Qexp")
cal.setObservationCovarianceMatrix(mat)
cal.setRegressorName("H")
if args.seed > 0:
    cal.setSeed(args.seed)
cal.setParameterTransformationFunction(ROOT.transf)
cal.estimateParameters()

# Draw the parameters
canPar = ROOT.TCanvas("CanPar", "CanPar", 1200, 800)
padPar = ROOT.TPad("padPar", "padPar", 0, 0.03, 1, 1)
padPar.Draw()
if args.style:
    ROOT.gROOT.LoadMacro(args.style)
padPar.cd()
cal.drawParameters("Parameter title", "*", "", "nonewcanvas,transformed")

canPar.SaveAs(args.figure_base+"_Par.png")

# Draw the residuals
canRes = ROOT.TCanvas("CanRes", "CanRes", 1200, 800)
padRes = ROOT.TPad("padRes", "padRes", 0, 0.03, 1, 1)
padRes.Draw()
if args.style:
    ROOT.gROOT.LoadMacro(args.style)
padRes.cd()
cal.drawResidues("Residual title", "*", "", "nonewcanvas")

canRes.SaveAs(args.figure_base+"_Res.png")

if args.json:
    mat_param = cal.getTransfParameterValueMatrix()
    mat_json = ROOT.TBufferJSON.ToJSON(mat_param)

    with open(args.json, "w") as file:
        file.write(str(mat_json))
