set(CMAKE_MODULES_DIR ${CMAKE_CURRENT_LIST_DIR})
function(get_uranie_modules URANIE_MODULES)
    if(Python3_EXECUTABLE)
        execute_process(COMMAND ${Python3_EXECUTABLE}
            ${CMAKE_MODULES_DIR}/get_uranie_modules.py
            OUTPUT_VARIABLE URANIE_MODULES)
        string(STRIP ${URANIE_MODULES} URANIE_MODULES)
        string(REPLACE " " ";" URANIE_MODULES "${URANIE_MODULES}")
        set(URANIE_MODULES ${URANIE_MODULES} PARENT_SCOPE)
    endif()
endfunction()

set(CPP_DIR "cpp")
set(PYTHON_DIR "python")
