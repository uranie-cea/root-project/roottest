import URANIE
import pkgutil, pathlib

uranie_path = pathlib.Path(URANIE.__file__).parent
modules = [name for _, name, _ in pkgutil.iter_modules([str(uranie_path)])]
print(*modules)
